# Dulux Weathershield Warranty Registration Portal - Laravel

Warranty Clime

## Version
1.0.0

## PHP Version
7.2 Above

## Laravel Version
7.0

## Requirements
- Basic knowledge of the [Laravel framework](https://laravel.com).
- Laravel CLI installed on your machine.
- [Valet](https://laravel.com/docs/5.5/valet#installation) installed on your machine.
- A code editor like [Visual Studio Code](https://code.visualstudio.com).
- [SQLite installed](http://www.sqlitetutorial.net/download-install-sqlite/) on your machine.

> Valet is only officially available to Mac users. However, there are ports for both [Linux](https://github.com/cpriego/valet-linux) and [Windows](https://github.com/cretueusebiu/valet-windows) available.

## Getting Started
To get up and running, simply run the following commands, first clone the repository and run the following commands
- `$ composer install`
- `$ valet link http://ewarranty.rewardssolution.com/`
- `$ valet link http://ewarranty.rewardssolution.com/admin`
 

Next, copy the `.env.example` file to `.env` and then add your database keys, and the `APP_BASE_DOMAIN`. Run the command below:
- `$ php artisan migrate --seed`

## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers/Backend` - Contains all the admin controllers
- `app/Http/Controllers` - Contains all the end user controllers
- `public/themes/dashforge` - Contains the admin CSS & JS files
- `public/themes/dashforge` - Contains all the end user CSS & JS files
- `resources/views/admin` - Contains the admin view files
- `resources/views` - Contains the end user view files
- `routes/admin` - Contains the all admin routes
- `routes/user` - Contains the all end user routes