<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false]);
Route::get('/', 'WarrantyController@index')->name('guest.home');
Route::get('/test', 'WarrantyController@indexTest')->name('guest.home.test');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('guest.check.warranty');
Route::post('post-login', 'Auth\LoginController@postLogin')->name('user.login.post'); 
Route::post('/logout/submit', 'Auth\LoginController@logout')->name('user.logout.submit');
Route::get('/terms-conditions', 'WarrantyController@termsConditions')->name('guest.terms');
Route::get('/product-usage-guide', 'WarrantyController@usageGuide')->name('guest.usage.guide');

// Reset password
Route::get('forget-password', 'Auth\ForgotPasswordController@showForgetPasswordForm')->name('forget.password.get');
Route::post('forget-password', 'Auth\ForgotPasswordController@submitForgetPasswordForm')->name('forget.password.post');
Route::get('reset-password/{token}', 'Auth\ForgotPasswordController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'Auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');

Route::post('new-warranty','WarrantyController@store')->name('guest.new.warranty');
Route::post('upload-image-media','WarrantyController@storeMedia')->name('guest.new.store.media');
Route::post('remove-image-media','WarrantyController@deleteMedia')->name('guest.new.remove.media');
Route::post('upload-image',['as'=>'image.upload','uses'=>'WarrantyController@uploadImages']);
Route::post('image/{id}/edit/','WarrantyController@edit')->name('guest.remove.image'); 

Route::get('gshops', 'WarrantyController@autocomplete')->name('guest.autocomplete');
Route::get('checkemail', 'WarrantyController@checkemail')->name('guest.check.email');
Route::get('checkinvoice', 'WarrantyController@checkinvoice')->name('guest.check.invoice');
Route::get('cities/get_by_states', 'WarrantyController@get_by_states')->name('guest.cities.states');
Route::get('postcodes/get_by_cities', 'WarrantyController@get_by_cities')->name('guest.postcode.cities');
//Route::get('/image-upload', 'HomeController@upload')->name('test.upload');
//Rouxte::post('image-submit','HomeController@store')->name('test.upload.post');

Route::group(['prefix' => '/user', 'middleware'=>['auth']], function () {
    Route::get('/', 'HomeController@index')->name('user.home');
    Route::get('/home2', 'HomeController@home2')->name('user.home2');
    Route::get('/show/{id}', 'HomeController@show')->name('user.warranty.show');
    Route::get('/home2', 'HomeController@home2')->name('home2');
    Route::get('/terms-conditions', 'HomeController@termsConditions')->name('user.terms');
    Route::get('cities/get_by_states', 'HomeController@get_by_states')->name('user.cities.states');
    Route::get('postcodes/get_by_cities', 'HomeController@get_by_cities')->name('user.postcode.cities');
    
    //AJAX DropZone
    Route::post('/upload-image-media','UserWarrantyController@storeMedia')->name('user.store.media');
    Route::post('/remove-image-media','UserWarrantyController@deleteMedia')->name('user.remove.media');
    
    Route::get('/autocomplete', 'UserWarrantyController@autocomplete')->name('user.autocomplete');
    Route::get('/new-submission','UserWarrantyController@index')->name('user.new.warranty');
    Route::get('/re-submission/{id}','UserWarrantyController@update')->name('user.re.submission');
    Route::post('/new-warranty','UserWarrantyController@store')->name('user.new.warranty.store');
    Route::post('/upload-image',['as'=>'user.image.upload','uses'=>'UserWarrantyController@uploadImages']);
    Route::post('/re-upload-image',['as'=>'user.image.re.upload','uses'=>'UserWarrantyController@reuploadImages']);
    Route::post('/image/{id}/edit/','UserWarrantyController@edit')->name('user.remove.image');
    Route::post('/resubmit/','UserWarrantyController@updateResubmit')->name('user.resubmit.save');
    Route::get('/download-pdf/{id}','UserWarrantyController@downloadPDF')->name('user.warranty.pdf');
    Route::get('/download-pdf-powerflexx/{id}','UserWarrantyController@downloadPDFPowerflexx')->name('user.warranty.pdf.powerflexx');
    
    // Password Change
    Route::get('/change-password', 'HomeController@changePassword')->name('user.change.password.form');
    Route::post('/change-password', 'HomeController@changPasswordStore')->name('user.change.password');
    
});
/**
 * Admin routes
 */
Route::group(['prefix' => '/admin'], function () {
    // Login Routes
    //Route::get('/', 'Backend\Auth\LoginController@showLoginForm')->name('admin.login'); 
    //Route::get('/elfinder', '\Barryvdh\Elfinder\ElfinderController@showIndex')->name('admin.elfinder.index'); 
    
    Route::get('/', 'Backend\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/login', 'Backend\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', [ 'as' => 'login', 'Backend\Auth\LoginController@showLoginForm']);
    Route::post('/login/submit', 'Backend\Auth\LoginController@login')->name('admin.login.submit');

    // Logout Routes
    Route::post('/logout/submit', 'Backend\Auth\LoginController@logout')->name('admin.logout.submit');

    // Forget Password Routes
    Route::get('/forget-password', 'Backend\Auth\ForgotPasswordController@getEmail')->name('admin.forgot_password');
    Route::post('/forget-password', 'Backend\Auth\ForgotPasswordController@postEmail')->name('admin.password.submit');
    
    Route::get('/reset-password/{token}', 'Backend\Auth\ResetPasswordController@getPassword')->name('admin.password.reset');
    Route::post('/reset-password-update', 'Backend\Auth\ResetPasswordController@updatePassword')->name('admin.password.update');
    
    Route::get('/shop/{token}','Backend\EmailShopsController@index')->name('guest.shop.token');
    Route::post('/shop-update', 'Backend\EmailShopsController@update')->name('guest.shop.update');
    //Route::group(['middleware' => ['auth']], function() { 
    Route::middleware('auth:admin')->group(function () {    
        //Auth::routes(); 
        Route::get('/dashboard', 'Backend\DashboardController@index')->name('admin.dashboard');
        Route::get('/manager', 'Backend\DashboardController@manager')->name('manager.dashboard');
        Route::get('/sale', 'Backend\DashboardController@sale')->name('sales.dashboard');
        //Route::get('/', 'Backend\DashboardController@index')->name('admin.dashboard');
        Route::resource('/settings/roles', 'Backend\RolesController', ['names' => 'admin.roles']);
        Route::get('/settings/roles/{id}/delete/', 'Backend\RolesController@destroy')->name('admin.roles.delete');
        
        Route::resource('users', 'Backend\UsersController', ['names' => 'admin.users']);
        Route::resource('/settings/admins', 'Backend\AdminsController', ['names' => 'admin.admins']);

        // Global Email Template
        Route::resource('/settings/global-email-templates','Backend\GlobalEmailTemplatesController');
        Route::get('/settings/global-email-templates/{id}/edit/','Backend\GlobalEmailTemplatesController@edit');
        Route::get('/settings/global-email-templates/{id}/delete/', 'Backend\GlobalEmailTemplatesController@destroy')->name('global-email-templates.delete');

        // Shops
        Route::resource('/shops','Backend\ShopsController');
        Route::get('/shops/{id}/edit/','Backend\ShopsController@edit');
        Route::get('/shops/{id}/delete/', 'Backend\ShopsController@destroy')->name('shops.delete');

        // Warranty
        Route::resource('/warranties/warranty','Backend\PurchaseController');
        Route::get('/warranties/edit-sales/{id}','Backend\PurchaseController@editsales')->name('warranty.edit.sales');
        Route::get('/warranties/accepted/','Backend\PurchaseController@accepted')->name('warranty.accepted');
        Route::get('/warranties/reviews/','Backend\PurchaseController@reviews')->name('warranty.reviews');
        Route::get('/warranties/approve/','Backend\PurchaseController@approve')->name('warranty.approve');
        Route::get('/warranties/decline/','Backend\PurchaseController@decline')->name('warranty.decline');
        Route::get('/warranties/pdf/{id}','Backend\PurchaseController@createPDF')->name('warranty.pdf');
        Route::get('/warranties/pdfpowerflexx/{id}','Backend\PurchaseController@createPDFPowerflexx')->name('warranty.pdf.powerflexx');
        Route::get('/warranties/send-shop-info/{id}','Backend\PurchaseController@sendEmail')->name('warranty.send.shop.info');
        
        Route::get('/warranties/edit-customer/{id}','Backend\PurchaseController@editCustomer')->name('warranty.edit.customer');
        Route::post('/warranties/edit-customer','Backend\PurchaseController@updateCustomer')->name('warranty.customer.save');
        
        Route::get('/warranties/on-behalf/','Backend\PurchaseController@onBehalf')->name('warranty.onbehalf');
        Route::get('/warranties/cities/', 'Backend\PurchaseController@get_by_states')->name('warranty.cities.states');
        Route::get('/warranties/postcode/', 'Backend\PurchaseController@get_by_cities')->name('warranty.postcode.cities');
        Route::get('/warranties/shops/', 'Backend\PurchaseController@get_by_shops')->name('warranty.sale.shops');
        
        Route::get('/warranties/{id}/edit/','Backend\PurchaseController@edit');
        Route::get('/warranties/{id}/delete/', 'Backend\PurchaseController@destroy')->name('warranty.delete');
        Route::get('/warranties/{id}/test/','Backend\PurchaseController@emailtest');
        
        Route::get('/warranties/autocomplete', 'Backend\PurchaseController@autocomplete')->name('warranty.autocomplete');
        Route::post('/warranties/re-upload-image',['as'=>'warranty.image.re.upload','uses'=>'Backend\PurchaseController@reuploadImages']);
        Route::post('/warranties/upload-image-media','Backend\PurchaseController@storeMedia')->name('warranty.store.media');
        Route::post('/warranties/remove-image-media','Backend\PurchaseController@deleteMedia')->name('warranty.remove.media');
        Route::get('/warranties/checkemail', 'Backend\PurchaseController@checkemail')->name('warranty.check.email');
        
        
        Route::resource('/team','Backend\TeamPurchaseController');
        
        //Users
        Route::resource('/users','Backend\UsersController');
        
        // Sales PIC
        Route::resource('/sales','Backend\SalesManagerController');
        Route::get('/sales/{id}/edit/','Backend\SalesManagerController@edit');
        Route::get('/sales/{id}/delete/', 'Backend\SalesManagerController@destroy')->name('sales.delete');
        
        // Regional Manager
        Route::get('/managers/my-sales-team/','Backend\SalesManagerController@mysales')->name('my.sales.team');
        Route::resource('/managers','Backend\RegionalSalesManagerController');
        Route::get('/managers/{id}/edit/','Backend\RegionalSalesManagerController@edit');
        Route::get('/managers/{id}/delete/', 'Backend\RegionalSalesManagerController@destroy')->name('managers.delete');
        
        

        Route::get('/change-password', 'Backend\DashboardController@changePassword')->name('admin.change.password.form'); 
        Route::post('/change-password', 'Backend\DashboardController@changPasswordStore')->name('admin.change.password');
        
    });
});
