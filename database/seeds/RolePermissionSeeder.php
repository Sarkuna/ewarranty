<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Roles
        $roleSuperAdmin = Role::create(['name' => 'superadmin']);
        //$roleAdmin = Role::create(['name' => 'admin']);
        //$roleEditor = Role::create(['name' => 'editor']);
        //$roleUser = Role::create(['name' => 'user']);


        // Permission List as array
        $permissions = [

            [
                'group_name' => 'dashboard',
                'permissions' => [
                    'dashboard.view',
                    'dashboard.edit',
                ]
            ],
            [
                'group_name' => 'warranties',
                'permissions' => [
                    // Warrantie Permissions
                    'warranties.view',
                    'warranties.accepted',
                    'warranties.reviews',
                    'warranties.approve',
                    'warranties.warranties.decline',
                ]
            ],            
            [
                'group_name' => 'user',
                'permissions' => [
                    // Users Permissions
                    'user.create',
                    'user.view',
                    'user.edit',
                    'user.delete',
                    'user.approve',
                ]
            ],
            [
                'group_name' => 'managers',
                'permissions' => [
                    // Managers Permissions
                    'managers.create',
                    'managers.view',
                    'managers.edit',
                    'managers.delete',
                    'managers.approve',
                ]
            ],
            [
                'group_name' => 'sales',
                'permissions' => [
                    // Sales PIC Permissions
                    'sales.create',
                    'sales.view',
                    'sales.edit',
                    'sales.delete',
                    'sales.approve',
                ]
            ],
            [
                'group_name' => 'admin',
                'permissions' => [
                    // admin Permissions
                    'admin.create',
                    'admin.view',
                    'admin.edit',
                    'admin.delete',
                    'admin.approve',
                ]
            ],
            [
                'group_name' => 'shop',
                'permissions' => [
                    // shop Permissions
                    'shop.create',
                    'shop.view',
                    'shop.edit',
                    'shop.delete',
                    'shop.approve',
                ]
            ],
            [
                'group_name' => 'role',
                'permissions' => [
                    // role Permissions
                    'role.create',
                    'role.view',
                    'role.edit',
                    'role.delete',
                    'role.approve',
                ]
            ],
            [
                'group_name' => 'profile',
                'permissions' => [
                    // profile Permissions
                    'profile.view',
                    'profile.edit',
                ]
            ],
            
        ];


        // Create and Assign Permissions
        for ($i = 0; $i < count($permissions); $i++) {
            $permissionGroup = $permissions[$i]['group_name'];
            for ($j = 0; $j < count($permissions[$i]['permissions']); $j++) {
                // Create Permission
                $permission = Permission::create(['name' => $permissions[$i]['permissions'][$j], 'group_name' => $permissionGroup]);
                $roleSuperAdmin->givePermissionTo($permission);
                $permission->assignRole($roleSuperAdmin);
            }
        }
    }
}
