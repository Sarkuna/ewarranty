<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Admin::where('email', 'shihanagni@gmail.com')->first();
        if (is_null($user)) {
            $user = new Admin();
            $user->name = "Mohamed Shihan";
            $user->email = "shihanagni@gmail.com";
            $user->password = Hash::make('12345678');
            $user->type = "admin";            
            $user->status = 'active';
            $user->save();
        }
    }
}
