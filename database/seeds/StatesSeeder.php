<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            array(
                'state'=>'Johor'
            ),
            array(
                'state'=>'Kedah'
            ),
            array(
                'state'=>'Kelantan'
            ),
            array(
                'state'=>'Kuala Lumpur'
            ),
            array(
                'state'=>'Labuan'
            ), 
            array(
                'state'=>'Melaka'
            ),
            array(
                'state'=>'Negeri Sembilan'
            ),
            array(
                'state'=>'Pahang'
            ),
            array(
                'state'=>'Perak'
            ),
            array(
                'state'=>'Perlis'
            ),
            array(
                'state'=>'Pulau Pinang'
            ),
            array(
                'state'=>'Putrajaya'
            ),
            array(
                'state'=>'Sabah'
            ),
            array(
                'state'=>'Sarawak'
            ),
            array(
                'state'=>'Selangor'
            ),
            array(
                'state'=>'Terengganu'
            ),
        );

        DB::table('states')->insert($data);
    }
}
