<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'shihanagni@gmail.com')->first();
        if (is_null($user)) {
            $user = new User();
            $user->name = "Mohamed Shihan";
            $user->email = "shihanagni@gmail.com";
            $user->mobile = "00601118889597";
            $user->password = Hash::make('12345678');
            $user->status = 'active';
            $user->save();
        }
    }
}
