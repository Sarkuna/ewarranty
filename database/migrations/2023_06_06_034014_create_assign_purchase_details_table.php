<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignPurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_purchase_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('purchase_details_id')->default(0);
            $table->unsignedBigInteger('admins_id')->default(0);
            $table->ipAddress('visitor');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('purchase_details_id')->references('id')->on('purchase_details');
            $table->foreign('admins_id')->references('id')->on('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_purchase_details');
    }
}
