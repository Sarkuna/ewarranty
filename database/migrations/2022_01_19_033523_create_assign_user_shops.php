<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignUserShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_user_shops', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('shop_id')->default(0);
            $table->unsignedBigInteger('manager_id')->default(0);
            $table->unsignedBigInteger('sales_id')->default(0);
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->foreign('manager_id')->references('id')->on('admins');
            $table->foreign('sales_id')->references('id')->on('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_user_shops');
    }
}
