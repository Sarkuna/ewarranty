<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->default(0);
            $table->unsignedBigInteger('shop_id')->default(0);
            $table->string('ref');            
            $table->date('date_of_purchase');
            $table->date('apply_date');
            $table->string('address1',100);
            $table->string('address2',100)->nullable();
            $table->string('city',50);
            $table->string('post_code',30);
            $table->string('state',50);
            $table->ipAddress('visitor');
            $table->string('status')->nullable()->default("accepted");
            $table->boolean('agree')->deault(false);                        
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
    }
}
