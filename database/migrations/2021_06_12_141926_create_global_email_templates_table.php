<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code',100)->unique();
            $table->string('subject');            
            $table->longText('template');
            $table->enum('status',['yes','no'])->default('yes');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_email_templates');
    }
}
