<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username',20)->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('mobile',20)->nullable();            
            $table->enum('type',['admin','hod','manager','sales']);
            $table->enum('status',['active','inactive','trash','black'])->default('active');
            $table->timestamp('date_last_login', 0)->nullable();
            $table->ipAddress('visitor')->nullable();            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
