<ol>
        <li>The Dulux Weathershield Powerflexx & Dulux Weathershield (Selected Products) warranty enables a customer who has purchased Dulux Weathershield Powerflexx & Dulux Weathershield to claim for replacement of the product(s) in accordance with these Terms and Conditions in the event of Peeling, Cracking, Excessive Chalking or Fungus & Algae grows on surface as described under point 4 below (“Warranty”). </li>
        <li>The Warranty is only applicable on Dulux Weathershield PowerFlexx and Dulux Weathershield paints' application on sites situated in Malaysia. Additionally, the Warranty is only applicable to consumers who are residents of Malaysia. The warranty is not applicable to employees of the AkzoNobel group of companies, their distributors, wholesalers, resellers, or retailers. Distributors, wholesalers, retailers and contractors/painters may not submit claims on behalf of their customers.</li>
        <li>The Warranty can be claimed on Dulux Weathershield Powerflexx for 12 years after the date of application and on Dulux Weathershield for 7 years after the date of application. Date of application must be within one month from date of purchase/receipt. </li>
        <li>
            Definition of Peeling, Cracking, Excessive Chalking & Algae/Fungus growth:
            <ul>
                <li>Peeling: This condition is manifested when the paint film peels away or detaches from the substrate or when one coat detaches itself from the other coat of paint.</li>
                <li>Cracking: This condition is manifested by any visible cracking on the paint film other than that caused by plastering/substrate defects and structural movements</li> 
                <li>Excessive Chalking: This condition is manifested when the paint film is Chalking more than normal for an emulsion paint and has adversely affected the appearance of paint work.</li>
                <li>Fungus & Algae growth: This condition is established when there is growth of microorganisms on the surface of the paint film which results in the appearance of the paint film through discoloration. This does not cover fungus growth caused from water seepage, high humidity, high moisture, excessive dirt accumulation or constant dampness within the walls.</li>

            </ul>
        </li>
        <li>The Warranty is subject to the following conditions, which include but not limited to:
            <ul>
                <li>Strict conformance to Dulux directions for use is a prerequisite for getting the warranty. The Dulux Weathershield Powerflexx or Dulux Weathershield must have been applied on properly prepared surfaces/application (in accordance to Product Data Sheet or product label) and manufacturer’s instruction of painting system (in accordance to Product Data Sheet or product label) to avail Warranty.</li>
                <li>The paint applied must be unadulterated Dulux Weathershield Powerflexx or/and Dulux Weathershield. AkzoNobel reserves the right to inspect the product at the site at any time and take samples for testing/analysis in its laboratory at any factory site or at such other laboratory as may be considered desirable, if required. In the event the Product is found to have been adulterated, this Warranty will be null and void.</li>
                <li>The substrate should not be affected by water leak or constant dampness. Moisture content on the wall must not exceed 15%, as per reading of the protimeter.</li>
                <li>The Warranty does not cover peeling in conditions where the substrate/building material is excessively powdery or loosely adhering.</li>
                <li>This Warranty does not apply to fungus/algae growth caused by leakages from structural/plaster defects and water proofing defects, inadequate or improper surface preparation, from constant dampness around and within the walls including those caused by air conditioning and excessive water collection as well as from high moisture retention areas such as plantation and vegetation areas.</li>
                <li>The Warranty shall not cover any paint defects arising from structural faults or movements, acts of God, job damage or excessive accumulation of atmospheric dirt, stains and/or other factors beyond the control of AkzoNobel.</li>
            </ul>
            </li>
            
            <li>The applicability of the Warranty commences after the painting job has been completed and the paint has dried completely, subject to the timeframe for application mentioned in point 3 above. </li>

<li>The Warranty covers only the replacement of the Dulux Weathershield PowerFlexx or Dulux Weathershield paints (as applicable) as per the material consumption 
on the affected area only (the material consumption shall be calculated based on the stated product 
coverage in the Product Data Sheet or product label). Compensation excludes any claim for labour cost or other costs in respect of the original or replacement product. In no case shall the liabilities undertaken by AkzoNobel within the scope of this Warranty exceed replacement value as per the aforementioned calculation.
</li>
<li>The replacement / claim liability will diminish as per the table below:</li>

<table class="table table-bordered">
    <tr>
        <th>Year from date of application</th>
        <th>Dulux Weathershield Powerflexx</th>
        <th>Dulux Weathershield</th>
    </tr>

    <tr>
        <td>1<sup>st</sup>Year</td>
        <td>100% of the defect area</td>
        <td>100% of the defect area</td>
    </tr>

    <tr>
        <td>2<sup>nd</sup>Year</td>
        <td>100% of the defect area</td>
        <td>100% of the defect area</td>
    </tr>

    <tr>
        <td>3<sup>nd</sup>Year</td>
        <td>100% of the defect area</td>
        <td>100% of the defect area</td>
    </tr>

    <tr>
        <td>4<sup>th</sup>Year</td>
        <td>100% of the defect area</td>
        <td>100% of the defect area</td>
    </tr>

    <tr>
        <td>5<sup>th</sup>Year</td>
        <td>100% of the defect area</td>
        <td>100% of the defect area</td>
    </tr>

    <tr>
        <td>6<sup>th</sup>Year</td>
        <td>50% of the defect area</td>
        <td>50% of the defect area</td>
    </tr>

    <tr>
        <td>7<sup>th</sup>Year</td>
        <td>45% of the defect area</td>
        <td>30% of the defect area</td>
    </tr>

    <tr>
        <td>8<sup>th</sup>Year</td>
        <td>40% of the defect area</td>
        <td></td>
    </tr>

    <tr>
        <td>9<sup>th</sup>Year</td>
        <td>30% of the defect area</td>
        <td></td>
    </tr>


    <tr>
        <td>10<sup>th</sup>Year</td>
        <td>20% of the defect area</td>
        <td></td>
    </tr>


    <tr>
        <td>11<sup>th</sup>Year</td>
        <td>15% of the defect area</td>
        <td></td>
    </tr>


    <tr>
        <td>12<sup>th</sup>Year</td>
        <td>10% of the defect area</td>
        <td></td>
    </tr>

</table>

<li>Registration of the Warranty:
    <ul>
        <li>To qualify for the Warranty, registration must be submitted within one month from date of purchase / receipt at www.duluxweathershieldwarranty.com.my</li>
        <li>The following would be the mandatory documents / information required to register for a valid Warranty:
           <ul>
               <li>Details of painted house/site owner: Name, mobile number, email, painted house/site address</li>
               <li>Details of retailer store where the purchase made: Name, address</li>
               <li>A clear copy of printed purchase receipt showing the details of the Selected Product(s) that were purchased, the product(s) that used for surface preparation (appropriate Primer or Sealer), the date of the purchase, the price paid for the Product(s).</li>
               <li>Before and after photo of exterior walls painted.</li>
           </ul>
        </li>
        <li>Upon successful registration, an approved digital e-warranty document will be issued to the customer via email.</li>
    </ul>
</li>

<li>Claim submission of the warranty:
    <ul>
	<li>The customer must submit the approved digital e-warranty document to <a href="mailto:customercare.my@akzonobel.com" style="color:#0563c1; text-decoration:underline">customercare.my@akzonobel.com</a> with required documents/information as below:</li>

	<li>Email &amp; contact number for communication purposes.</li>
	<li>Details of the problem being faced, including details of the Selected Product(s) applied such as brand name, colour code/name, area of the affected surface.</li>
	<li>At least 3 pictures in the following prescribed format clearly depicting the issue: 
<ul>
	<li>One long shot (10 ft distance or more) </li>
	<li>One close shot (3-5 ft distance) </li>
	<li>One at any other angle that helps in showing the issue in a clear way </li>
</ul>
</li>

	<li>After the claim is registered following the above procedure and all required details, an AkzoNobel representative may be sent to assess the painted area to verify that the claim is based on a bona fide purchase of a Product(s) and to ensure that the Product(s) and its application meets the conditions required for the warranty to apply.&nbsp; </li>

	<li>Notwithstanding anything above, we accept no responsibility for claims that are incomplete, invalid, illegible, or delayed. In such cases AkzoNobel will be constrained to close the customer request/claim if incomplete, ineligible or inaccurate. </li>

	<li>Upon establishing the defect and validation of the claim by AkzoNobel, the compensation will be made from the same retailer/store where it was originally bought. It will be the responsibility of the customer to arrange for logistics, at its cost, for the replaced product from the dealer/store to the relevant site. </li>

	<li>We reserve the right to reject a claim if we have reason to believe that there is misrepresentation, or the claim is fraudulent or is in violation of any of these Terms and Conditions (&ldquo;T&amp;Cs&rdquo;). </li>

	<li>No third party or joint submissions shall be accepted.</li>
	</ul>
</li>

<li>AkzoNobel shall not be liable or responsible for any loss/ consequential loss arising from any paint defects.</li>
<li>The Warranty shall be limited only to the extent of providing the Product(s) to the extent of defect can be established. AkzoNobel shall not be responsible for any other material, product or expenses of any nature.</li> 

<li>Claims not made in accordance with these T&Cs will be deemed invalid. If a claim is refused because the terms of the Warranty, in AkzoNobel’s sole opinion, have not been met, AkzoNobel’s decision shall be final and binding.</li> 

<li>If any provision in these T&Cs is found to be invalid, unlawful or 
unenforceable in any court or competent authority, the provision shall be deemed not to be part 
of the T&Cs and it shall not affect the enforceability of the rest of the provisions of the T&Cs, and the Parties shall negotiate in good faith to modify this Warranty to effect the original intent of the Parties as closely as possible in a mutually acceptable manner in order that the transactions contemplated herein are consummated as originally contemplated to the greatest extent possible</li>

<li>We reserve the right to withdraw, amend or terminate the Warranty without notice. All claims 
made in accordance with these T&Cs and made prior to the Warranty being withdrawn, amended, 
or terminated will still be honored. </li>

<li>No cash or alternative to the compensation shall be provided. </li>

<li>This Warranty constitutes the sole and entire liabilities of AkzoNobel as a result of any default or claim arising out of or in connection with the defects set out in point 4. All warranties, representations, conditions and terms (including any implied warranties of merchantability and fitness for a particular purpose) on the matters mentioned above are explicitly excluded to the fullest extent as permitted by law, whether they are express or implied, written or oral.</li>

<li>The reference to "We" or "AkzoNobel" under these T&Cs shall mean Akzo Nobel Paints (Malaysia) Sdn. Bhd.</li>

<li>AkzoNobel, or AkzoNobel logos such as that of Dulux, ICI Roundel, Flourish, Let's Colour, 
distinctive colour names and liveries are trademarks of the AkzoNobel group ©AkzoNobel 2018.</li>

<li>The customer care telephone number is: 1800-88-9338 (Mon-Fri, 9am-5pm) or email: customercare.my@akzonobel.com</li>


    </ol>