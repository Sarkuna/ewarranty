<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- Meta -->
        <meta name="description" content="E-Warranty">
        <meta name="author" content="AkzoNobel Malaysia">

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('themes/dashforge/assets/img/favicon.ico') }}"  type='image/x-icon'>
        
        <title>@yield('title') - E-Warranty</title>

        {{-- Include core + vendor Styles --}}
        @include('panels/styles')


    </head>

    <body class="page-profile">
        @include('panels/header')

        <!-- BEGIN: Content-->
        @yield('content')
        <!-- END: Content-->

        @include('panels/footer')

        {{-- Include core + vendor Script --}}
        @include('panels/scripts')


    </body>
    <!-- END: Body-->

</html>