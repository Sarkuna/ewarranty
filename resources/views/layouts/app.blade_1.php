<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Dulux Warranty')</title>

    <!-- Styles -->
    	<!-- bootstrap 4.x is supported. You can also use the bootstrap css 3.3.x versions -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->
        <!-- link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput-rtl.min.css" media="all" rel="stylesheet" type="text/css" /-->
        <!-- the font awesome icon library if using with `fas` theme (or Bootstrap 4.x). Note that default icons used in the plugin are glyphicons that are bundled only with Bootstrap 3.x. -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
            wish to resize images before upload. This must be loaded before fileinput.min.js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="text/javascript"></script>
        <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
            This must be loaded before fileinput.min.js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="text/javascript"></script>
        <!-- popper.min.js below is needed if you use bootstrap 4.x (for popover and tooltips). You can also use the bootstrap js 
           3.3.x versions without popper.min.js. -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
            dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <!-- the main fileinput plugin file -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
        <!-- following theme script is needed to use the Font Awesome 5.x theme (`fas`) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>
        <!-- optionally if you need translation for your language then include the locale file as mentioned below (replace LANG.js with your language locale) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/LANG.js"></script>
        
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>

        <!-- FontAwesome JS-->
        <script defer src="assets/fontawesome/js/all.min.js"></script>

        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="{{ asset('themes/akzo-nobel/assets/css/theme.css') }}">
        <link id="theme-style" rel="stylesheet" href="{{ asset('themes/akzo-nobel/assets/css/form.css') }}">
        <link id="theme-style" rel="stylesheet" href="{{ asset('themes/akzo-nobel/assets/css/custom.css') }}">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>
<body>
    <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
                    <div class="site-logo"><a class="navbar-brand" href="index">
                            <img class="logo-icon mr-2 img-responsive" src="{{ asset('themes/akzo-nobel/assets/images/DULUX_RGB_imp_05.png') }}" alt="logo" style=" width: 15%;"><span class="logo-text">Dulux</span></a></div>    
                </div><!--//docs-logo-wrapper-->
                <div class="docs-top-utilities d-flex justify-content-end align-items-center">

                    <!--<ul class="social-list list-inline mx-md-3 mx-lg-5 mb-0 d-none d-lg-flex">
                            <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        </ul>--><!--//social-list-->
                    <a href="#" class="btn btn-primary d-none d-lg-flex">FAQ</a>

                </div><!--//docs-top-utilities-->
            </div><!--//container-->
        </div><!--//branding-->
    </header><!--//header-->


    <div class="page-header theme-bg-dark1 py-5 text-center position-relative">
        <img class="" src="{{ asset('themes/akzo-nobel/assets/images/banner-contact.jpg') }}">
    </div><!--//page-header-->

    <div class="page-content">
        <div class="container">
            <div class="docs-overview py-5">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="form-modal">

                            <div class="form-toggle">
                                <button id="login-toggle" onclick="toggleLogin()">New Submission</button>
                                <button id="signup-toggle" onclick="toggleSignup()">Login</button>
                            </div>

                            <div id="login-form">
                                <?php
                                $path1 = "/themes/frest-admin-v1/app-assets/images/i1.jpeg";
                                $path2 = "/themes/frest-admin-v1/app-assets/images/i2.jpeg";
                                ?>
                                
                                <form action="#" class="needs-validation" method="post" novalidate>
                                    <div class="row">
                                        <img src="{{$path1}}" />
                                <img src="{{$path2}}" />
                                        <div class="col-lg-12"><h4 class="text-title">Personal Detail</h4></div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="fname" class="control-label">Name</label>
                                                <input type="text" class="form-control" id="fname" placeholder="Name" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="email" class="control-label">Email</label>
                                                <input type="email" class="form-control" id="email" placeholder="Email" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">IC</label>
                                                <input type="text" class="form-control" id="lname" placeholder="NRIC">
                                            </div>
                                            <!--<div class="form-group">
                                                <label for="lname" class="control-label">Mobile Number*</label>
                                                <input type="text" class="form-control" id="lname" placeholder="Mobile Number" required>
                                            </div>-->
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12"><h4 class="text-title">Purchase Detail</h4></div>
                                        
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Attach Receipt*</label>
                                                <input id="input-b1" name="input-b1" type="file" class="file" data-show-preview="false" multiple accept="image/*">
                                                <script>
                                                $("#input-b1").fileinput({
                                                        showUpload: false,
                                                        minFileCount: 5,
                                                        maxFileCount: 5,
                                                        validateInitialCount: true,
                                                        overwriteInitial: false,
                                                        allowedFileExtensions: ["jpg", "png", "gif"]
                                                });
                                                </script>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Attach Receipt*</label>
                                                <input id="input-b2" name="input-b2" type="file" class="file" data-show-preview="false" multiple accept="image/*">
                                                <script>
                                                $("#input-b2").fileinput({
                                                        showUpload: false,
                                                        minFileCount: 10,
                                                        maxFileCount: 10,
                                                        validateInitialCount: true,
                                                        overwriteInitial: false,
                                                        allowedFileExtensions: ["jpg", "png", "gif"]
                                                });
                                                </script>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Attach Photo Before*</label>
                                                <input id="input-b3" name="input-b3" type="file" class="file" data-show-preview="false" multiple accept="image/*">
                                                <script>
                                                $("#input-b3").fileinput({
                                                        showUpload: false,
                                                        minFileCount: 10,
                                                        maxFileCount: 10,
                                                        validateInitialCount: true,
                                                        overwriteInitial: false,
                                                        allowedFileExtensions: ["jpg", "png", "gif"]
                                                });
                                                </script>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Attach Front of Pack of All Paint can*</label>
                                                <input id="input-b4" name="input-b4" type="file" class="file" data-show-preview="false" multiple accept="image/*">
                                                <script>
                                                $("#input-b4").fileinput({
                                                        showUpload: false,
                                                        minFileCount: 5,
                                                        maxFileCount: 5,
                                                        validateInitialCount: true,
                                                        overwriteInitial: false,
                                                        allowedFileExtensions: ["jpg", "png", "gif"]
                                                });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12"><h4 class="text-title">Shop Info</h4></div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">State</label>
                                                <select class="form-control" required>
                                                    <option value="Johor">Johor</option>
                                                    <option value="Kedah">Kedah</option>
                                                    <option value="Kelantan">Kelantan</option>
                                                    <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                    <option value="Labuan">Labuan</option>
                                                    <option value="Malacca">Malacca</option>
                                                    <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                    <option value="Pahang">Pahang</option>
                                                    <option value="Perak">Perak</option>
                                                    <option value="Perlis">Perlis</option>
                                                    <option value="Penang">Penang</option>
                                                    <option value="Sabah">Sabah</option>
                                                    <option value="Sarawak">Sarawak</option>
                                                    <option value="Selangor">Selangor</option>
                                                    <option value="Terengganu">Terengganu</option>
                                                </select>
                                            </div>

                                            
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Shop Name*</label>
                                                <input type="text" class="form-control" id="lname" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Date of Purchase*</label>
                                                <input type="text" class="form-control" id="lname"  required>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12"><h4 class="text-title">Work Address</h4></div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Address 1</label>
                                                <input type="text" class="form-control" id="lname" placeholder="Address 1" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Address 2</label>
                                                <input type="text" class="form-control" id="lname" placeholder="Address 1">
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">City</label>
                                                <input type="text" class="form-control" id="lname" placeholder="City" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">Poscode</label>
                                                <input type="text" class="form-control" id="lname" placeholder="Poscode" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="lname" class="control-label">State</label>
                                                <select class="form-control" required>
                                                    <option value="Johor">Johor</option>
                                                    <option value="Kedah">Kedah</option>
                                                    <option value="Kelantan">Kelantan</option>
                                                    <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                    <option value="Labuan">Labuan</option>
                                                    <option value="Malacca">Malacca</option>
                                                    <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                    <option value="Pahang">Pahang</option>
                                                    <option value="Perak">Perak</option>
                                                    <option value="Perlis">Perlis</option>
                                                    <option value="Penang">Penang</option>
                                                    <option value="Sabah">Sabah</option>
                                                    <option value="Sarawak">Sarawak</option>
                                                    <option value="Selangor">Selangor</option>
                                                    <option value="Terengganu">Terengganu</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12">
                                           <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="gridCheck" required>
                                                    <label class="form-check-label" for="gridCheck">
                                                        Terms and Conditions  and Privacy Policy.
                                                    </label>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>

                            <div id="signup-form">


                                <form>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            			
                                            <div class="form-group">
                                                <label for="email" class="control-label">Email</label>
                                                <input type="email" class="form-control" id="email" placeholder="Email" required>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="fname" class="control-label">Password</label>
                                                <input type="password" class="form-control" id="fname" placeholder="Name" required>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Login</button> 
                                    </div>		
                                </form>
                            </div>

                        </div>
                    </div>			









                </div><!--//row-->
            </div><!--//container-->
        </div><!--//container-->
    </div><!--//page-content-->

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3">Launch Your Software Project Like A Pro</h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto">Want to launch your software project and start getting traction from your target users? Check out our premium <a class="text-white" href="https://themes.3rdwavemedia.com/bootstrap-templates/startup/coderpro-bootstrap-4-startup-template-for-software-projects/">Bootstrap 4 startup template CoderPro</a>! It has everything you need to promote your product.</div>

        </div>
    </section><!--//cta-section-->



    <footer class="footer">
        <div class="footer-bottom text-center py-5">
            <ul class="social-list2 list-unstyled pb-4 mb-0">
                <li class="list-inline-item"><a href="#">Cookies</a></li> 
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                <li class="list-inline-item"><a href="#">Legal</a></li>
                <li class="list-inline-item"><a href="#">Other Akzonobel Sites</a></li>
            </ul><!--//social-list-->
            <!--<ul class="social-list list-unstyled pb-4 mb-0">
                    <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
            <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
        </ul>--><!--//social-list-->

            <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
            <small class="copyright">Copyright © 2021 AkzoNobel Paints 
                <img src="https://www.dulux.com.my/content/dam/akzonobel-common/akzo-nobel.png" style="width: 7%;padding-left: 10px;"></small>


        </div>

    </footer>

    <!-- Javascript -->          
    <script src="{{ asset('themes/akzo-nobel/assets/plugins/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('themes/akzo-nobel/assets/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('themes/akzo-nobel/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>  
    <script id="rendered-js" >
function toggleSignup() {
document.getElementById("login-toggle").style.backgroundColor = "#fff";
document.getElementById("login-toggle").style.color = "#222";
document.getElementById("signup-toggle").style.backgroundColor = "#2fc48d";
document.getElementById("signup-toggle").style.color = "#fff";
document.getElementById("login-form").style.display = "none";
document.getElementById("signup-form").style.display = "block";
}

function toggleLogin() {
document.getElementById("login-toggle").style.backgroundColor = "#2fc48d";
document.getElementById("login-toggle").style.color = "#fff";
document.getElementById("signup-toggle").style.backgroundColor = "#fff";
document.getElementById("signup-toggle").style.color = "#222";
document.getElementById("signup-form").style.display = "none";
document.getElementById("login-form").style.display = "block";
}
//# sourceURL=pen.js
    </script>

    <script>
    // Self-executing function
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

</body>
</html>
