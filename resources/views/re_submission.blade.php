@extends('layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->
<link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>

<style>
    .fa-fw {width: 2.25em !important;}
    .bg-primary {background-color: #0168fad9 !important;}
    
    .masthead h1 {font-size: 4rem;margin: 0;padding: 0;}
    .file-input.theme-fa.file-input-ajax-new {width:100%;}
</style>
@endsection

@section('content')

<div class="content tx-13">
    <div>
    <h4 class="mg-b-5"> {{ $warranty->warranty_code }}</h4>
    <p class="mg-b-0 tx-color-03">Submitted on {{ $warranty->created_at->format('M d D, Y') }}</p>
    </div>
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="row">

            <div class="col-sm-6 col-lg-8 mg-t-40 mg-sm-t-0 mg-md-t-40">
                <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Work site Address</label>

                <p class="mg-b-0">{{ $warranty->address1 }}</p>
                <p class="mg-b-0">{{ $warranty->address2 }}</p>
                <p class="mg-b-0">{{ $warranty->city }}</p>
                <p class="mg-b-0">{{ $warranty->post_code }} - {{ $warranty->state }}</p>
            </div><!-- col -->
            <div class="col-sm-6 col-lg-4 mg-t-40">
                <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Shop Information</label>
                <ul class="list-unstyled lh-7">
                    <li class="d-flex justify-content-between">
                        <span>State</span>
                        <span>{{ $warranty->shop_info->state }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Shop Name</span>
                        <span>{{ $warranty->shop_info->company_name }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Date of Purchase</span>
                        <span>{{ $warranty->date_of_purchase->format('M d D, Y') }}</span>
                    </li>
                </ul>
            </div><!-- col -->



        </div><!-- row -->

    
<form id="form1" method="post" action="{{route('user.resubmit.save')}}" class="parsley-style-1" enctype="multipart/form-data" data-parsley-validate novalidate>
    {{csrf_field()}}
    <input type="hidden" value="{{ $warranty->id }}" name="id">
    <div class="table-responsive mg-t-40">
            <table class="table table-invoice bd-b">
                <thead>
                    <tr>
                        <th class="wd-30p">Image</th>
                        <th class="wd-40p d-none d-sm-table-cell">Comment</th>
                        <th class="wd-30p">Re Upload</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($warranty->getAllImagesNotApprove($warranty->ref)) > 0)
                        @foreach ($warranty->getAllImagesNotApprove($warranty->ref) as $key => $image)
                            <tr>
                                <td class="tx-nowrap">
                                  @if (\File::extension($image->real_filename) == 'pdf')

                                  
                                      <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                                      <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$key}}">View</a></h3>
                                  @else
                                  <a href="#" data-toggle="modal" data-target="#modal{{$key}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="wd-100p" alt="Responsive image"></a>
                                  @endif
                                    <!-- Modal -->
                                    <div class="modal fade" id="modal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content tx-14">
                                                <div class="modal-header">
                                                    <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </td>
                                <td class="d-none d-sm-table-cell tx-color-03">
                                    <input value="{{ $image->id }}" name="image_id[{{ $key }}]" type="hidden">
                                    {{ $image->summary }}
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <div class="file-loading">
                                        <input id="input-{{ $key }}" name="uploadFile" type="file" >
                                    </div>

                                </td>

                            </tr>
                        @endforeach
                    @else
                    <tr><td colspan="3"class="text-center">There are no records available.!</td></tr>
                    @endif    
                </tbody>
            </table>
        </div>
    
    @if(count($warranty->getAllImagesNotApprove($warranty->ref)) > 0)
    <button type="submit" class="btn btn-primary btn-upload-3" id="submit1">Submit</button>
    @endif 
</form>
        
        </div><!-- container -->
</div><!-- content -->
 @endsection  
 
 @section('vendor-script')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/fileinput.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/themes/fa/theme.min.js" type="text/javascript"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

 @endsection
 
 @section('page-script')
 
 <script type="text/javascript">    
    $(document).ready(function() {
        var $ref = $('#ref').val();
        @foreach ($warranty->getAllImagesNotApprove($warranty->ref) as $key => $image)
        //ATTACH RECEIPTS
        var $el{{ $key }} = $("#input-{{ $key }}");       
        $el{{ $key }}.fileinput({
            theme: 'fa',
            required: true,
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'pdf'],
            uploadUrl: "{{route('user.image.re.upload')}}",
            uploadExtraData: function() {
                return {
                    _key: "{{ $key }}",
                    _id: "{{ $image->id }}",
                    _token: "{{ csrf_token() }}",
                    _ref: $ref, 
                    _type: "{{ $image->type }}",
                };
            },
            uploadAsync: true,
            deleteExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                    _key: "{{ $key }}",
                };
            },
            showUpload: false, // hide upload button
            showCancel: false,
            overwriteInitial: false, // append files to initial preview
            //minFileCount: 2,
            maxFileCount: 1,
            maxFileSize: 2000,
            browseOnZoneClick: true,
            initialPreviewAsData: true,
        }).on("filebatchselected", function(event, files) {
            $el{{ $key }}.fileinput("upload"); 
        }).on("filedeleted", function(event, key, jqXHR, data) {
            $("#bb{{ $key }} .input-group.file-caption-main").show();
        }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
            //window.location.reload();
            //location.reload();
            window.location.href = '/user/re-submission/{{ $warranty->id }}'; //one level up
        });
        @endforeach
    });
 </script>
 @endsection
 
 
 