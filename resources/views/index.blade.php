@extends('layouts/authLayout')

@section('title', 'Register')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.profile.css') }}">
<style>
    .fa-fw {width: 2.25em !important;}
    .bg-primary {background-color: #0168fad9 !important;}
    .masthead {
            min-height: 30rem;
            position: relative;
            display: table;
            width: 100%;
            height: auto;
            padding-top: 8rem;
            padding-bottom: 8rem;
            background: linear-gradient(90deg, rgba(255, 255, 255, 0.1) 0%, rgba(255, 255, 255, 0.1) 100%), url("http://ewarranty.rewardssolution.com/themes/akzo-nobel/assets/images/banner-contact.jpg");
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
    }
    .masthead h1 {font-size: 4rem;margin: 0;padding: 0;}
    .file-input.theme-fa.file-input-ajax-new {width:100%;}
    .file-input {padding: 20px;width: 100%;}
    #v1play{display: none;}
    #v2play{display: none;}
</style>


@endsection

@section('content')

<div class="content content-auth-alt">
    <div class="container ht-100p">
        <!-- Secction 1 -->
        <div class="row">
            <div class="col-sm-12 col-lg-4 no-gutters mg-b-20 order-lg-first order-1">                
                <div class="col-lg-12">
                    <img src="{{ asset('themes/dashforge/assets/img/Powerflexx_WEB BANNER 640x670.jpg') }}" class="card-img-top">
                    <h5 class="heading-title mt-2" data-transition-item="" style="">Dulux Weathershield Powerflexx</h5>
                    <p class="mt-2">The top quality elastomeric exterior wall paint with highly advanced Triple Defense Technology that stretches more to cover 10X wider cracks</p>
                    <a href="https://www.dulux.com.my/en/dulux-weathershield-powerflexx" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>
                <div class="col-lg-12 mt-5">
                    <img src="{{ asset('themes/dashforge/assets/img/WS_WEB BANNER 640x670.jpg') }}" class="card-img-top">
                    <h5 class="heading-title mt-2" data-transition-item="" style="">Dulux Weathershield</h5>
                    <p class="mt-2">The premium exterior wall paint with advanced Dualshield Technology that provides 2X ultimate protection for your home.</p>
                    <a href="https://www.dulux.com.my/en/shield-your-home-with-double-the-protection" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>
            </div>
            <div class="col-sm-12 order-0 col-lg-8 no-gutters order-lg-0">
                <div class="col-lg-12">
                    <h4 class="heading-title">Welcome to Dulux Weathershield Warranty Registration Portal</h4>
                    <h5 class="heading-title" style="font-weight: normal;">Register your warranty by following these steps:</h5>
                </div>
                <div class="row">                    
                    
                    <div class="col-sm-6 col-lg-4">
                        <div class="card card-help bgbox1">
                            <div class="card-body tx-12">
                                <div class="tx-60 lh-0 mg-b-10"><i class="icon ion-ios-camera"></i></div>
                                <h5>Step 1 : Register</h5>
                                <p class="tx-color-03 mg-b-0">Create an account and submit your purchase details as per your receipt.</p>
                            </div><!-- card-body -->

                        </div><!-- card -->
                    </div><!-- col -->
                    
                    <div class="col-sm-6 col-lg-4 mg-t-20 mg-sm-t-0">
                        <div class="card card-help bgbox2">
                            <div class="card-body tx-12">
                                <div class="tx-60 lh-0 mg-b-10"><i class="fas fa-paint-roller"></i></div>
                                <h5>Step 2 : Monitor</h5>
                                <p class="tx-color-03 mg-b-0">View your warranty submission status by logging into the system.</p>
                            </div><!-- card-body -->

                        </div><!-- card -->
                    </div><!-- col -->
                    
                    <div class="col-sm-6 col-lg-4 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                        <div class="card card-help bgbox3">
                            <div class="card-body tx-12">
                                <div class="tx-60 lh-0 mg-b-10"><i class="fas fa-magic"></i></div>
                                <h5>Step 3 : Check</h5>
                                <p class="tx-color-03 mg-b-0">Approved warranty document will be available in your account for your reference.</p>
                            </div><!-- card-body -->

                        </div><!-- card -->
                    </div><!-- col -->

                </div>
                
                @include('new_form')
            </div>
        </div>
        
        
        <!-- Secction 2 -->
        
        <!--<div class="card-deck">
            <div class="col-lg-12 mt-5">
                <h5 class="heading-title">Premium Exterior Wall Paints</h5>
            </div>
            <div class="col-lg-6 text-center">
                <img src="{{ asset('themes/dashforge/assets/img/dw7.png') }}" class="card-img-top" alt="..." style="width: 350px;">
                <div class="card-body">
                    <h5 class="card-title">Dulux Weathershield</h5>
                    <a href="https://www.dulux.com.my/en/products/dulux-weathershield-acrylic-exterior-wall-finish" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>
            </div>
            <div class="col-lg-6 text-center">
                <img src="{{ asset('themes/dashforge/assets/img/dwp10.png') }}" class="card-img-top" alt="..." style="width: 350px;">
                <div class="card-body">
                    <h5 class="card-title">Dulux Weathershield Powerflexx</h5>
                    <a href="https://www.dulux.com.my/en/products/dulux-weathershield-powerflexx" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>
            </div>
            <p>High humidity, heavy rainfall and hot sun are all conditions that attack paintwork, causing problems like fungus and algae growth, cracking and peeling, or chalking of the surface.To safeguard your peace of mind, Dulux offers protection for a period of twelve (12) years for Dulux Weathershield Powerflexx and seven (7) years for Dulux Weathershield.To learn more about the protection/warranty, please click here.</p>
        </div>--->
        
        
        <!-- Section 3 -->
        
        <hr>
        <div class="row mg-t-20" id="guide">
            <h3 class="col-lg-12 heading-title text-left mg-b-20">Product Usage Guide</h3>
            <div class="col-sm-6 col-lg-4 text-center mg-b-20">
                <h5 class="heading-title text-left">Premium Exterior Wall Paints</h5>
                <p class="text-left">High humidity, heavy rainfall and hot sun are all conditions that attack paintwork, causing problems like fungus and algae growth, cracking and peeling, or chalking of the surface.<p>
                <p class="text-left">To safeguard your peace of mind, Dulux offers protection for your exterior walls.</p>
                <p class="text-left">To learn more about the protection/warranty, please click <a href="{{route('guest.terms')}}">here</a>.</p>
                
                <div class="col-lg-12">
                    <img src="{{ asset('themes/dashforge/assets/img/dwp10.png') }}" class="card-img-top" style="width: 80%;">
                    <h5 class="heading-title header-05 mt-2" data-transition-item="" style="">Dulux Weathershield Powerflexx</h5>
                    <a href="https://www.dulux.com.my/en/products/dulux-weathershield-powerflexx" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>
                <div class="col-lg-12 mt-5">
                    <img src="{{ asset('themes/dashforge/assets/img/dw7.png') }}" class="card-img-top" style="width: 80%;">
                    <h5 class="heading-title header-05 mt-2" data-transition-item="" style="">Dulux Weathershield</h5>
                    <a href="https://www.dulux.com.my/en/products/dulux-weathershield-acrylic-exterior-wall-finish" class="btn btn-outline-primary btn-xs-block" target="_blank">Discover more</a>
                </div>                
            </div>
            <div class="col-sm-6 col-lg-8">
                <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Surface Preparation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Problems & Solutions</a>
                    </li>
                </ul>
                <div class="tab-content bd bd-gray-300 bd-t-0 pd-20 ht-lg-700" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <h6> Before you use</h6>
                        <ul class="steps steps-vertical">
                            <li class="step-item active">
                                <a href="" class="step-link">
                                    <span class="step-number">1</span>
                                    <span class="step-title">REMOVE RESIDUE</span>
                                </a>
                                <ul>
                                    <li class="active">Remove any loose paint or powdery residue by brushing/scraping with a stiff fiber brush under running water or a high pressure water jet. </li>
                                </ul>
                            </li>
                            <li class="step-item active">
                                <a href="" class="step-link">
                                    <span class="step-number">2</span>
                                    <span class="step-title">TREAT AFFECTED AREAS</span>
                                </a>
                                <ul>
                                    <li class="active">Use Dulux Fungicidal Wash (A980-19260) to treat areas affected by mould, lichen, algae or moss. </li>
                                </ul>
                            </li>
                            <li class="step-item active">
                                <a href="" class="step-link">
                                    <span class="step-number">3</span>
                                    <span class="step-title">ENSURE DRY WALL</span>
                                </a>
                                <ul>
                                    <li class="active">Make sure that your wall is dry before painting. Do not paint if the wall is saturated with dampness (Protimeter reading higher than 15%). Allow the wall to dry further.</li>
                                </ul>
                            </li>
                            <li class="step-item active">
                                <a href="" class="step-link">
                                    <span class="step-number">4</span>
                                    <span class="step-title">CLEAN SURFACE</span>
                                </a>
                                <ul>
                                    <li class="active">Make sure that the surface to be painted is clean, dry and free from any defects, dirt, grease or wax.</li>
                                </ul>
                            </li>
                        </ul>
                        
                        <h6 class="mt-5">Dulux Weathershield Powerflexx™ /  Dulux Weathershield Painting System:</h6>
                        <p>It is strongly recommended to prepare the surface with one coat of Dulux Weathershield Primer / Sealer and overcoat it with minimum two coats to Dulux Weathershield Powerflexx™ / Dulux Weathershield</p>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Before you paint</h4>
                            <p>The performance of your Dulux Weathershield range of paint is dependent upon 2 coats being applied to a sound, properly prepared surface. Failure to follow complete instructions may result in below par performance. To help you on the correct surface preparation, the following are some common substrate defects and their solutions.</p>
                        </div>
                        <div id="accordion1" class="accordion accordion-style2">
                            <h6 class="accordion-title">Problem 1 : Powdery</h6>
                            <div class="accordion-body">
                                <div class="row"> 
                                    <div class="col-lg-3"><img src="{{ asset('themes/dashforge/assets/img/P1.jpg') }}" class="img-thumbnail"></div>
                                    <div class="col-lg-9">High-pressured water jetting (1,500 psi) is compulsory. Allow to dry and follow through with 1 coat of Dulux Weathershield Primer Adhesion Promoting (A579-15222) before applying 2 coats of Dulux Weathershield Powerflexx / Dulux Weathershield.</div>
                                </div>
                            </div>
                            <h6 class="accordion-title">Problem 2 : Flaking</h6>
                            <div class="accordion-body">
                                <div class="row"> 
                                    <div class="col-lg-3"><img src="{{ asset('themes/dashforge/assets/img/P2.jpg') }}" class="img-thumbnail"></div>
                                    <div class="col-lg-9">High-pressured water jetting (1,500 psi) is compulsory. Allow to dry and follow through with 1 coat Dulux Weathershield Primer Adhesion Promoting (A579-15222) before applying 2 coats of Dulux Weathershield Powerflexx / Dulux Weathershield.</div>
                                </div>
                            </div>
                            <h6 class="accordion-title">Problem 3 : Algae and Fungus Infested</h6>
                            <div class="accordion-body">
                                <div class="row"> 
                                    <div class="col-lg-3"><img src="{{ asset('themes/dashforge/assets/img/P3.jpg') }}" class="img-thumbnail"></div>
                                    <div class="col-lg-9">High-pressured water jetting (1,500 psi) is compulsory to remove algae and fungus.  Allow to dry and follow through with Dulux Fungicidal Wash (A980-19260) before applying 2 coats of Dulux Weathershield Powerflexx / Dulux Weathershield.</div>
                                </div>
                            </div>
                            <h6 class="accordion-title">Problem 4 : Damp</h6>
                            <div class="accordion-body">
                                <div class="row"> 
                                    <div class="col-lg-3"><img src="{{ asset('themes/dashforge/assets/img/P4.jpg') }}" class="img-thumbnail"></div>
                                    <div class="col-lg-9">Ensure that the surface is thoroughly dry before painting to avoid premature paint failure. Apply 1 coat of Dulux Weathershield Sealer Alkali Resisting (A931-18177) followed by 2 coats of Dulux Weathershield Powerflexx / Dulux Weathershield.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
            </div>
        </div>
        
        <!-- Section 4 -->
        <hr class=" mg-b-50">
        <div class="row mg-t-5">
            <div class="col-lg-4 mt-5 mg-b-10">
                <h5 class="heading-title">Dulux Painting System Guide:</h5>
                A clean and dry surface must be prepared with one coat of Dulux Weathershield Primer / Sealer 
                and overcoat with minimum two coats of Dulux Weathershield Powerflexx / Dulux Weathershield

            </div>

            <div class="bg-gray col-sm col-lg-4 col-xl text-center">
                <div class="">
                    <h5 class="mg-b-5">Primer / Sealer</h5>
                    <h5 class="mg-b-5">1 Coat</h5>
                    <div class="row">
                        <div class="col-sm col-lg-6 col-xl text-center">                            
                            <img src="{{ asset('themes/dashforge/assets/img/BannerA1.jpg') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-gray col-sm col-lg-4 col-xl text-center">
                <div class="">
                    <h5 class="mg-b-5">Apply TopCoat</h5>
                    <h5 class="mg-b-5">2 Coats</h5>
                    <div class="row">
                        <div class="col-sm col-lg-6 col-xl text-center">                            
                            <img src="{{ asset('themes/dashforge/assets/img/BannerB1.jpg') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- row -->
        <hr class=" mg-b-50">
        <div class="card-deck no-gutters mg-t-30" id="tutorial">
            <h3 class="col-lg-12 heading-title text-center mg-b-20">Watch the step-by-step Dulux E-Warranty Registration Submission Guide</h3>
            
            <div class="col-lg-6 text-center" style="padding: 10px;">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3p6OOSrD8R0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
            </div>

            <div class="col-lg-6 text-center" style="padding: 10px;">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/wFcqEFz4BeA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
            </div>
            
        </div>
        <hr class=" mg-b-50">
        <!-- Section 5 -->
        
        <div class="card-deck no-gutters">
            <div class="col-lg-12 mt-3">
                <h5 class="heading-title">Dulux Products and Services</h5>
            </div>
            <div class="card">
                <img src="{{ asset('themes/dashforge/assets/img/title_1.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <a href="https://www.dulux.com.my/en/products/filters/p_Interior" target="_blank"><h5 class="card-title">Choose a Dulux Product</h5></a>
                </div>
            </div>
            <div class="card">
                <img src="{{ asset('themes/dashforge/assets/img/title_2.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <a href="https://www.duluxpreviewservice.com/" target="_blank"><h5 class="card-title">Get personalised colour preview for your home</h5></a>
                </div>
            </div>
            <div class="card">
                <img src="{{ asset('themes/dashforge/assets/img/title_3.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <a href="https://www.duluxpainter.com.my/?utm_source=referral&utm_medium=banner&utm_campaign=hompage_multiheroblock_MY%20" target="_blank"><h5 class="card-title">Find a painter near you</h5></a>
                </div>
            </div>
            <div class="card">
                <img src="{{ asset('themes/dashforge/assets/img/Dulux-LazMall-E-Store-Soft-Launch.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <a href="https://www.lazada.com.my/shop/dulux/" target="_blank"><h5 class="card-title">The official Dulux Flagship Store is now online at Laz Mall!</h5></a>
                </div>
            </div>
        </div>
        
    </div>
    
    
</div>
        <!-- About -->
        

    </div><!-- content -->
    
    <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content tx-14">
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel4">Terms and Conditions</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span> 
            </button>
          </div>
          <div class="modal-body">
              @include('terms_text')
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal" id="modal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel7" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel7">Your submission is successful!</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mg-b-0">Please check your email for login details for 1st time login.</p>
                    <p class="mg-b-0">You can also click on Check Warranty to login and view the status of your submissions.</p>
                </div>
                <div class="modal-footer">
                    <button id="regclose" type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

 @endsection  
 
 @section('vendor-script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/fileinput.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/locales/kr.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/themes/fas/theme.js"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/typeahead.js/bootstrap3-typeahead.min.js') }}"></script>

 @endsection
 
 @section('page-script')
 

<script src="{{ asset('themes/dashforge/lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.js"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    });
</script>
<script>    
    $(function(){
        'use strict';
        
        


        $('#wizard2').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex) {
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var name = $('#name').parsley();
                var email = $('#email').parsley();
                var mobile = $('#mobile').parsley();
                
                var shopstate = $('#shop_state').parsley();                
                var shopname = $('#shop_name').parsley();
                var invoicenum = $('#invoice_num').parsley();
                var datepicker1 = $('#datepicker1').parsley();
                var datepicker2 = $('#datepicker2').parsley();
                
                var address1 = $('#address1').parsley();
                var state = $('#state').parsley();
                var wcity = $('#wcity').parsley();
                var wpostcode = $('#wpostcode').parsley();

                if(name.isValid() && email.isValid() && mobile.isValid() && shopstate.isValid() && shopname.isValid() && invoicenum.isValid() && datepicker1.isValid() && datepicker2.isValid() && address1.isValid() && state.isValid() && wcity.isValid() && wpostcode.isValid()) {
                  return true;
                } else {
                  name.validate();
                  email.validate();
                  mobile.validate();
                  shopstate.validate();
                  shopname.validate();
                  invoicenum.validate();
                  datepicker1.validate();
                  datepicker2.validate();
                  address1.validate();
                  state.validate();
                  wcity.validate();
                  wpostcode.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {                    
                    var receiptsdz1 = $('#img_receipts').val();
                    
                    if ($('input[name="surface[]"]:checked').length < 1){
                        $(".surfaceerror").show();
                        return false;
                    }else{                        
                        $(".surfaceerror").hide();                                 
                    }
                    
                    if ($('input[name="topcoat[]"]:checked').length < 1){
                        $(".topcoaterror").show();
                        return false;
                    }else{                        
                        $(".topcoaterror").hide();                                 
                    }
                    
                    
                    if(receiptsdz1 === undefined){                        
                        alert('Attach Receipts is required');
                        return false;
                    }
                    
                    return true;
              }
              
              if(currentIndex === 2) {
                  var attach_photo_before = $('#attach_photo_before').val();
                  if(attach_photo_before === undefined){                        
                        alert('Attach Before Photos is required');
                        return false;
                    }
                  
                   //remove default #finish button
                   return true;
              }

            // Always allow step back to the previous step even if the current step is not valid.
            } else { return true; }
          },
          labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            var attach_photo_after = $('#attach_photo_after').val();
            if(attach_photo_after === undefined){                        
                  alert('Attach After Photos is required');
                  return false;
            }
            
            var b1 = $('#b1').parsley();

            if(b1.isValid()) {
              $("#form1").submit();
            } else {
              b1.validate();
            }
        }
        });

        $('#datepicker1').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2019/01/01"),
            dateFormat: 'dd-mm-yy',
            startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D",
            onSelect: function(dateText, inst) {
                $('#datepicker2').datepicker('option', 'minDate', dateText);
            }
        });
        
        $('#datepicker2').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2023/05/09"),
            dateFormat: 'dd-mm-yy',
            //minDate: "+0M +0D",
            //startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D"
            
        });
        
        $('#email').on('change', function() {
            $.ajax({
                url: "{{route('guest.check.email')}}",
                data: {
                    'email' : $('#email').val()
                },
                dataType: 'json',
                success: function(data) {
                    if(data == 'yes'){
                        toastr.warning("Oops! We have noticed that you already have an account with us. Would you like to login to your account now? <br/> <a class='close-toastr-yes btn btn-success'> Yes </a> <a class='close-toastr-no btn btn-info'> No </a>", "", {
                            tapToDismiss: false
                            , timeOut: 0
                            , extendedTimeOut: 0
                            , allowHtml: true
                            , preventDuplicates: true
                            , preventOpenDuplicates: true
                            , newestOnTop: true
                        });
                        
                        $('.close-toastr-yes').on('click', function () {
                            toastr.clear($(this).closest('.toast'));
                            $('#email').val('');
                            window.location = "{{route('guest.check.warranty')}}";
                            
                        });
                        
                        $('.close-toastr-no').on('click', function () {
                            toastr.clear($(this).closest('.toast'));
                            $('#email').val('');
                        });
                    }
                    console.log(data);
                },
                error: function(data){
                    //error
                }
            });
        });
        
        var path = "{{ route('guest.autocomplete') }}";    
        $('#shop_name').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query, state:$('#shop_state').val() }, function (data) {
                    return process(data);
                });
            }
        });
        
        
        let removeFileFromServer = true;

        Dropzone.autoDiscover = false;
        var fileAttachDropzone = $(".fileAttachDropzone").dropzone({
          url: "{{route('guest.new.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "img_receipts");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='img_receipts' name='img_receipts[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('guest.new.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "img_receipts"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });

        //---------------------------Attach Photo Before---------------------------------
        var fileAttachDropzone = $(".fileAttachDropzone2").dropzone({
          url: "{{route('guest.new.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_before");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_before' name='attach_photo_before[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('guest.new.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_before"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        //-------------------------------Attach Photo After----------------------
        var fileAttachDropzone = $(".fileAttachDropzone3").dropzone({
          url: "{{route('guest.new.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_after");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_after' name='attach_photo_after[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('guest.new.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_after"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        //------------------------------------
        $('#accordion1').accordion({
            heightStyle: 'content',
            collapsible: true
        });
        
        $("#state").change(function(){
            $('#wpostcode').val("");
            $.ajax({
                url: "{{route('guest.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wcity').html(data.html);
                }
            });
        });
        
        $("#wcity").change(function(){
            $.ajax({
                url: "{{route('guest.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wpostcode').html(data.html);
                }
            });
        });
        
        
        $("#astate").change(function(){
            
            $('#a_postcode').val("");
            $.ajax({
                url: "{{route('guest.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_city').html(data.html);
                }
            });
        });

        
        $("#a_city").change(function(){
            $.ajax({
                url: "{{route('guest.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_postcode').html(data.html);
                }
            });
        });
    });
</script>

<script>
(function(){ 
  
var urlpath = window.location.href;
  
  if(urlpath.indexOf("success") > -1){
    openModalFunction();
  }  
  
  console.log(urlpath);
})();

function openModalFunction(){
    window.location.hash = '#guide';
    //window.location.reload(true);
  $("#modal7").modal('show');
}

$('#regclose').on('click', function(){
  window.location.href = "{{route('guest.home')}}/#guide";
})
</script>
    
 @endsection