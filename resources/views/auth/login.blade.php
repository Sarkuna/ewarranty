@extends('layouts/authLayout')

@section('title', 'Login')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">

@endsection

@section('content')

<div class="content content-auth">
    <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
            <div class="media-body align-items-center d-none d-lg-flex">
                <div class="mx-wd-700">
                    <img src="{{ asset('themes/dashforge/assets/img/Powerflexx_WEB BANNER 640x670.jpg') }}" class="img-fluid" alt="">
                </div>
            </div><!-- media-body -->
            <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
                <div class="wd-100p">
                    <h3 class="tx-color-01 mg-b-5">Check Warranty</h3>
                    <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please log in to check warranty.</p>
                    <form method="post" action="{{route('user.login.post')}}" class="parsley-style-1" data-parsley-validate novalidate>
                            {{csrf_field()}}
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" value="{{old('email')}}" name="email" class="form-control" placeholder="yourname@yourmail.com" required="">
                        @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between mg-b-5">
                            <label class="mg-b-0-f">Password</label>
                            <a href="{{route('forget.password.get')}}" class="tx-13">Forgot password?</a>
                        </div>
                        <input type="password" value="{{old('password')}}" name="password" class="form-control" placeholder="Enter your password" required="">
                        @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <button class="btn btn-brand-02 btn-block">Sign In</button>
                    </form>

                </div>
            </div><!-- sign-wrapper -->
        </div><!-- media -->
    </div><!-- container -->
</div><!-- content -->

 @endsection  
 
 @section('vendor-script')
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/typeahead.js/bootstrap3-typeahead.min.js') }}"></script>

 @endsection
 
 @section('page-script')


    
 @endsection