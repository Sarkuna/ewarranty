@extends('layouts/authLayout')

@section('title', 'Reset Password')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">



@endsection

@section('content')
<div class="content content-fixed content-auth-alt">
    <div class="container d-flex justify-content-center ht-100p">
        <div class="mx-wd-300 wd-sm-450 ht-100p d-flex flex-column align-items-center justify-content-center">
            <h4 class="tx-20 tx-sm-24">Reset your password</h4>
            
            <div class="d-flex align-items-center justify-content-center pd-y-40">
                <form method="POST" action="{{ route('reset.password.post') }}" novalidate class="bd bd-gray-300 rounded pd-30 bg-white">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="new_password" placeholder="New Password" required>
                        @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                        <div id='password-strength-status'></div>
                                    <span class="text-info">Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.</span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control" id="password_confirmation" placeholder="Confirm Password" required>
                        @if ($errors->has('password_confirmation'))
                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                </form>
            </div>
            
            
            

        </div>
    </div><!-- container -->
</div><!-- content -->
@endsection


@section('vendor-script')
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 @endsection
 
 @section('page-script')
 <script>
    $("#new_password").keyup(function() {
        var number = /([0-9])/;
        var lowercase = /([a-z])/;
        var uppercase = /([A-Z])/;
        var special = /([!@#$%^&*])/;
        if ($('#new_password').val().length < 8) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('alert alert-danger');
            $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
        } else {
            if ($('#new_password').val().match(number) && $('#new_password').val().match(lowercase) && $('#new_password').val().match(uppercase) && $('#new_password').val().match(special)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-success');
                $('#password-strength-status').html("Strong");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-warning');
                $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
            }
        }
    });
    </script>
 
 
 @endsection

