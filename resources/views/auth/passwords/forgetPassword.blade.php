@extends('layouts/authLayout')

@section('title', 'Recover Password')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">



@endsection

@section('content')
<div class="content content-fixed content-auth-alt">
    <div class="container d-flex justify-content-center ht-100p">
        <div class="mx-wd-300 wd-sm-450 ht-100p d-flex flex-column align-items-center justify-content-center">
            <h4 class="tx-20 tx-sm-24">Reset your password</h4>
            <p class="tx-color-03 mg-b-30 tx-center">Enter your email address we will send you a link to reset your password.</p>
            <form role="form" method="POST" action="{{ route('forget.password.post') }}" novalidate>
                {{ csrf_field() }}
                <div class="wd-100p d-flex flex-column flex-sm-row mg-b-10">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control wd-sm-250 flex-fill" id="user-name" placeholder="Enter email address" required="">                    
                    <button class="btn btn-brand-02 mg-sm-l-10 mg-t-10 mg-sm-t-0">Reset Password</button>
                </div>
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
            </form>

        </div>
    </div><!-- container -->
</div><!-- content -->
@endsection
