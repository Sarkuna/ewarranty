@extends('layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">
<style>
		.fa-fw {
			width: 2.25em !important;
		}

		.bg-primary {
			background-color: #0168fad9 !important;
		}

	</style>
		<style>
		.masthead {
			min-height: 30rem;
			position: relative;
			display: table;
			width: 100%;
			height: auto;
			padding-top: 8rem;
			padding-bottom: 8rem;
			background: linear-gradient(90deg, rgba(255, 255, 255, 0.1) 0%, rgba(255, 255, 255, 0.1) 100%), url("http://ewarranty.rewardssolution.com/themes/akzo-nobel/assets/images/banner-contact.jpg");
			background-position: center center;
			background-repeat: no-repeat;
			background-size: cover;
		}

		.masthead h1 {
			font-size: 4rem;
			margin: 0;
			padding: 0;
		}

	</style>


	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-YPQZH3JL0M');

	</script>

@endsection

@section('content')

<div class="content content-fixed mg-b-100">
            <div class="container">
        <div class="row row-xs">

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header d-sm-flex justify-content-between bd-b-0 pd-t-20 pd-b-0">
                        <div class="mg-b-10 mg-sm-b-0">
                            <h6 class="mg-b-5">Register new warranty</h6>
                            <p class="tx-12 tx-color-03 mg-b-0">Fill in the serial number and product code manually below
                                and click continue button.</p>
                        </div>
                    </div><!-- /.card-header -->
                    <div class="card-body pd-y-30">
                        <img src="{{ asset('/storage/purchase_detail/1234445555555/2021-06-06_222933.jpg')}}">
                        <form method="post" action="{{route('test.upload.post')}}" class="parsley-style-1" enctype="multipart/form-data" data-parsley-validate novalidater>
                            {{csrf_field()}}
                            <div class="file-loading">
                                <input id="input-705" name="uploadFile" type="file">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit Form</button>
                        </form>
                            
                    </div><!-- /.card-body -->
                </div><!-- /.card -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->


    </div>

@endsection