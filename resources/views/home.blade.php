@extends('layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<style>
    .file-input.theme-fa.file-input-ajax-new {width:100%;}
    .fa-fw {width: 2.25em !important;}
    .bg-primary {background-color: #0168fad9 !important;}
    .masthead {min-height: 30rem;position: relative;display: table;width: 100%;height: auto;padding-top: 8rem;padding-bottom: 8rem;background: linear-gradient(90deg, rgba(255, 255, 255, 0.1) 0%, rgba(255, 255, 255, 0.1) 100%), url("{{ asset('themes/akzo-nobel/assets/images/banner-contact.jpg') }}");background-position: center center;background-repeat: no-repeat;background-size: cover;}
    .masthead h1 {font-size: 4rem;margin: 0;padding: 0;}
    .form-group input[type=file] {
    padding-bottom: 37px;
}
	</style>
        



@endsection

@section('content')



    <div class="card">
        <div class="card-header d-sm-flex justify-content-between bd-b-0 pd-t-20 pd-b-0">
            <div class="mg-b-10 mg-sm-b-0">
                <h6 class="mg-b-5">Your Products Warranty Information</h6>
                <p class="tx-12 tx-color-03 mg-b-0">as of {{ now()->format('l, jS F Y')}}</p>
            </div>
        </div><!-- /.card-header -->
        <br>
        <div class="table-responsive">
            <table class="table table-dashboard mg-b-0" id="tableWarranties">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>Expiry Date</th>
                        <th>Shop Name</th>
                        <th>Date of Application</th>
                        <th>Status</th>
                        <th class="text-center">View</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($warranties) > 0)
                    @foreach ($warranties as $warranty)                                
                    <tr id="category_id_{{ $warranty->id }}">
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $warranty->warranty_code }}</td>
                        <td>
                        @if(!empty($warranty->expiry_date)) 
                            {{ $warranty->expiry_date->format('d-m-Y') }}
                        @else
                            N/A
                        @endif
                        </td>
                        <td>{{ $warranty->shop_info->company_name }}</td>
                        <td>{{ $warranty->apply_date->format('M d D, Y') }}</td>
                        <td>{{ ucwords($warranty->status) }}</td>
                        <td class="text-center">
                            <a href="{{route('user.warranty.show',$warranty->id)}}" id="view" data-toggle="tooltip" data-placement="bottom" title="View"><i class="far fa-eye"></i></a>
                            
                            @if($warranty->status == 'review' && $warranty->countAllImagesResubmit($warranty->ref) > 0)
                                <a href="{{route('user.re.submission',$warranty->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></a>
                            @endif
                            
                            @if($warranty->status == 'approved')
                                @if($warranty->product_year == '7')
                                <a href="{{route('user.warranty.pdf',$warranty->id)}}" id="pdf" data-toggle="tooltip" data-placement="bottom" title="Dulux Weathershield" target="_blank"><i class="far fa-file-pdf text-primary"></i></a>
                                @endif
                                @if($warranty->product_year_power == '12')
                                <a href="{{route('user.warranty.pdf.powerflexx',$warranty->id)}}" id="view" data-toggle="tooltip" data-placement="bottom" title="Dulux Weathershield Powerflexx" target="_blank"><i class="far fa-file-pdf text-success"></i></a>
                                @endif
                            @endif
                            
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td class="text-center" colspan="6">No records found!!!</td></tr>
                    @endif
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!-- /.card -->
    <div class="divider-text">New Submission</div>
@include('form')

<div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content tx-14">
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel4">Terms and Conditions</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span> 
            </button>
          </div>
          <div class="modal-body">
              @include('terms_text')
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@endsection


@section('vendor-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/fileinput.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/locales/kr.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/themes/fas/theme.js"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/typeahead.js/bootstrap3-typeahead.min.js') }}"></script>
 @endsection
 
 @section('page-script')
<script src="{{ asset('themes/dashforge/lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.js"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    });
</script>
<script>    
    $(function(){
        'use strict';


        $('#wizard2').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex) {
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var name = $('#name').parsley();
                var mobile = $('#mobile').parsley();
                
                var shopstate = $('#shop_state').parsley();
                var shopname = $('#shop_name').parsley();
                var invoicenum = $('#invoice_num').parsley();
                var datepicker1 = $('#datepicker1').parsley();
                var datepicker2 = $('#datepicker2').parsley();
                
                var address1 = $('#address1').parsley();
                var state = $('#state').parsley();
                var wcity = $('#wcity').parsley();
                var wpostcode = $('#wpostcode').parsley();

                if(name.isValid() && mobile.isValid() && shopstate.isValid() && shopname.isValid() && invoicenum.isValid() && datepicker1.isValid() && datepicker2.isValid() && address1.isValid() && state.isValid() && wcity.isValid() && wpostcode.isValid()) {
                  return true;
                } else {
                   name.validate();
                  mobile.validate(); 
                  shopstate.validate();
                  shopname.validate();
                  invoicenum.validate();
                  datepicker1.validate();
                  datepicker2.validate();
                  address1.validate();
                  state.validate();
                  wcity.validate();
                  wpostcode.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {
                    var receiptsdz1 = $('#img_receipts').val();
                    
                    if ($('input[name="surface[]"]:checked').length < 1){
                        $(".surfaceerror").show();
                        return false;
                    }else{                        
                        $(".surfaceerror").hide();                                 
                    }
                    
                    if ($('input[name="topcoat[]"]:checked').length < 1){
                        $(".topcoaterror").show();
                        return false;
                    }else{                        
                        $(".topcoaterror").hide();                                 
                    }
                    
                    if(receiptsdz1 === undefined){                        
                        alert('Attach Receipts is required');
                        return false;
                    }
                    
                    return true;
              }
              
              if(currentIndex === 2) {
                    var attach_photo_before = $('#attach_photo_before').val();
                    if(attach_photo_before === undefined){                        
                          alert('Attach Before Photos is required');
                          return false;
                      }

                     //remove default #finish button
                     return true;
              }
              

            // Always allow step back to the previous step even if the current step is not valid.
            } else { return true; }
          },
          labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            //alert("Alhamdulillah, Alkhery Member is already Registered.");
             //$("#form1").submit();
             var attach_photo_after = $('#attach_photo_after').val();
            if(attach_photo_after === undefined){                        
                  alert('Attach After Photos is required');
                  return false;
            }
            
            var b1 = $('#b1').parsley();

            if(b1.isValid()) {
              $("#form1").submit();
            } else {
              b1.validate();
            }
        }
        });

        
        $('#datepicker1').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2019/01/01"),
            dateFormat: 'dd-mm-yy',
            startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D",
            onSelect: function(dateText, inst) {
                $('#datepicker2').datepicker('option', 'minDate', dateText);
            }
        });
        
        $('#datepicker2').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2023/05/09"),
            dateFormat: 'dd-mm-yy',
            //minDate: "+0M +0D",
            //startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D"
            
        });

        var path = "{{ route('user.autocomplete') }}";
        $('#shop_name').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query, state:$('#shop_state').val() }, function (data) {
                    return process(data);
                });
            }
        });
        
        
        let removeFileFromServer = true;

        Dropzone.autoDiscover = false;
        var fileAttachDropzone = $(".fileAttachDropzone").dropzone({
          url: "{{route('user.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "img_receipts");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='img_receipts' name='img_receipts[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('user.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "img_receipts"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });

        //---------------------------Attach Photo Before---------------------------------
        var fileAttachDropzone = $(".fileAttachDropzone2").dropzone({
          url: "{{route('user.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_before");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_before' name='attach_photo_before[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('user.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_before"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        //-------------------------------Attach Photo After----------------------
        var fileAttachDropzone = $(".fileAttachDropzone3").dropzone({
          url: "{{route('user.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_after");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_after' name='attach_photo_after[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('user.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_after"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        
        //------------------------------------
        $("#state").change(function(){
            
            $('#wpostcode').val("");
            $.ajax({
                url: "{{route('user.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wcity').html(data.html);
                }
            });
        });
        
        $("#wcity").change(function(){
            $.ajax({
                url: "{{route('user.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wpostcode').html(data.html);
                }
            });
        });
        
        $("#astate").change(function(){
            
            $('#a_postcode').val("");
            $.ajax({
                url: "{{route('user.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_city').html(data.html);
                }
            });
        });

        
        $("#a_city").change(function(){
            $.ajax({
                url: "{{route('user.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_postcode').html(data.html);
                }
            });
        });
    });
</script>
 @endsection