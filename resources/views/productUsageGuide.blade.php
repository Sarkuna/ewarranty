@extends('layouts/authLayout')

@section('title', 'Product Usage Guide')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.profile.css') }}">
<style>
    .fa-fw {width: 2.25em !important;}
    .bg-primary {background-color: #0168fad9 !important;}
    .masthead {
            min-height: 30rem;
            position: relative;
            display: table;
            width: 100%;
            height: auto;
            padding-top: 8rem;
            padding-bottom: 8rem;
            background: linear-gradient(90deg, rgba(255, 255, 255, 0.1) 0%, rgba(255, 255, 255, 0.1) 100%), url("http://ewarranty.rewardssolution.com/themes/akzo-nobel/assets/images/banner-contact.jpg");
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
    }
    .masthead h1 {font-size: 4rem;margin: 0;padding: 0;}
    .file-input.theme-fa.file-input-ajax-new {width:100%;}
</style>


@endsection

@section('content')

<div class="container">
        <!-- Secction 1 -->
        
<div class="row">
    <div class="col-md-12 mt-5">
        <h4 class="mg-b-0">Product Usage Guide</h4>
    </div>
    
    <div class="col-md-12 mt-5"></div>


    </div>
</div>
</div>
 @endsection  
 
 @section('vendor-script')
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 @endsection
 
 @section('page-script')
 
 @endsection