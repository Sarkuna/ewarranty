@extends('layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->
<link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>

<style>
    .fa-fw {width: 2.25em !important;}
    .bg-primary {background-color: #0168fad9 !important;}
    
    .masthead h1 {font-size: 4rem;margin: 0;padding: 0;}
    .file-input.theme-fa.file-input-ajax-new {width:100%;}
</style>
@endsection

@section('content')

@include('form')

 @endsection  
 
 @section('vendor-script')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/js/fileinput.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/themes/fa/theme.min.js" type="text/javascript"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

 @endsection
 
 @section('page-script')
 <script src="{{ asset('themes/dashforge/assets/js/myform.js') }}"></script>
 <script type="text/javascript">
    
    var path = "{{ route('guest.autocomplete') }}";
    
    $('#shop_name').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query, state:$('#shop_state').val() }, function (data) {
                return process(data);
            });
        }
    });
</script>
 @endsection