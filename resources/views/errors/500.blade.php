@extends('errors.errors_layout')

@section('title')
    500 - Internal Server Error
@endsection

@section('error-content')
<section class="row flexbox-container">
    <div class="col-xl-6 col-md-7 col-9">
        <!-- w-100 for IE specific -->
        <div class="card bg-transparent shadow-none">
            <div class="card-content">
                <div class="card-body text-center">
                    <img src="{{ asset('themes/frest-admin-v1/app-assets/images/pages/500.png') }}" class="img-fluid my-3" alt="branding logo">
                    <h1 class="error-title mt-1">500</h1>
                    <p class="p-2">
                        System Error. Please contact administrator.
                    </p>
                    <a href="{{ url()->previous() }}" class="btn btn-primary round glow mt-2">Back</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection