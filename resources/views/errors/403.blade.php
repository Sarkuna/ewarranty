@extends('admin.layouts.app')

@section('title')
Sales PIC - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    

@endsection

@section('content')


<div class="ht-100p d-flex flex-column align-items-center justify-content-center">
          <div class="wd-70p wd-sm-250 wd-lg-300 mg-b-15">
              <img src="{{ asset('themes/frest-admin-v1/app-assets/images/pages/not-authorized.png') }}" class="img-fluid" alt="not authorized" width="400">
          </div>
          <h1 class="tx-color-01 tx-24 tx-sm-32 tx-lg-36 mg-xl-b-5">403 Forbidden</h1>
          <h5 class="tx-16 tx-sm-18 tx-lg-20 tx-normal mg-b-20">You are not authorized!</h5>
          <p class="tx-color-03 mg-b-30">{{ $exception->getMessage() }}</p>
          <div class="mg-b-40">
              <a href="{{ route('admin.dashboard') }}" class="btn btn-white bd-2 pd-x-30">Back to Dashboard</a>
          </div>

        </div>

@endsection

@section('vendor-script')
     
@endsection

@section('page-script')
    
@endsection