@extends('errors.errors_layout')

@section('title')
404 - Page Not Found
@endsection

@section('error-content')
<section class="row flexbox-container">
    <div class="col-xl-6 col-md-7 col-9">
        <div class="card bg-transparent shadow-none">
            <div class="card-content">
                <div class="card-body text-center">
                    <h1 class="error-title">404</h1>
                    <p class="pb-3">we couldn't find the page you are looking for</p>
                    <img class="img-fluid" src="{{ asset('themes/frest-admin-v1/app-assets/images/pages/404.png') }}" alt="404 error">
                    <a href="{{ url()->previous() }}" class="btn btn-primary round glow mt-3">Back</a>
                    @if (!Auth::check())
                        <a href="{{ route('admin.login') }}" class="btn btn-primary round glow mt-3">Login Again !</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection