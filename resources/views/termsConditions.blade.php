@extends('layouts/authLayout')

@section('title', 'Terms and Conditions')

@section('vendor-style')
  <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">

@endsection

@section('page-style')
{{-- Page Css files --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.profile.css') }}">
<style>
    ol>li {
    margin-bottom: 10px;
}
</style>


@endsection

@section('content')

<div class="container">
        <!-- Secction 1 -->
        
<div class="row">
    <div class="col-md-12 mt-5">
        <h4 class="mg-b-0">Terms and Conditions</h4>
    </div>
    <div class="col-md-12 mt-5">
    @include('terms_text')
    </div>

</div>
</div>
 @endsection  
 
 @section('vendor-script')
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 @endsection
 
 @section('page-script')
 
 @endsection