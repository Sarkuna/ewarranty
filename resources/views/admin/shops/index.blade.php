@extends('admin.layouts.app')

@section('title')
Shops - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="row row-xs">
    <div class="col-sm-6 col-lg-2">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Central</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('Central')}}</h3>
            </div>

        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-sm-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">East Coast</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('East Coast')}}</h3>                
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Northern</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('East Coast')}}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Sabah</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('Sabah')}}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Sarawak</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('Sarawak')}}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Southern</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{Helper::shopsRegion('Southern')}}</h3>
            </div>
        </div>
    </div><!-- col -->    

    
</div>
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">Shop Lists</h4></div>
  <div class="col-sm-4 text-right">
      <a class="btn btn-primary mr-1 mb-1 btn-sm" href="{{ route('shops.create') }}">Create New</a>
  </div>
</div>

<div class="df-example table-responsive">
    <table id="example1" class="table table-hover table-sm">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>  
                <!--<th>State</th> -->
                <th>Sales Person</th>
                <th>Manager</th>
                <th>Region</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($shops as $shop)
            <tr id="category_id_{{ $shop->id }}">
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $shop->company_name }}</td>
                <!--<td>{{ $shop->state }}</td> !-->
                
                @if(!empty($shop->assign_sale_shop->shop_id))    
                <td>
                    <p class="m-0">{{ $shop->assign_sale_shop->sale->name }}</p>
                    <p class="m-0">{{ $shop->assign_sale_shop->sale->mobile }}</p>
                    <p class="m-0">{{ $shop->assign_sale_shop->sale->email }}</p>
                </td>
                @else
                    <td>N/A</td>
                @endif
                
                @if(!empty($shop->assign_manager->shop_id))    
                    <td>
                        <p class="m-0">{{ $shop->assign_manager->manager->name }}</p>
                        <p class="m-0">{{ $shop->assign_manager->manager->email }}</p>
                    </td>
                    <td>{{ $shop->assign_manager->manager->region }}</td>
                @else
                    <td>N/A</td>
                    <td>N/A</td>
                @endif

                <td>
                    @if($shop->status=='active')
                    <span class="badge badge-success">{{ucwords($shop->status)}}</span>
                    @else
                    <span class="badge badge-danger">{{ucwords($shop->status)}}</span>
                    @endif
                </td>
                <td class="text-center">
                    <a href="{{route('shops.edit',$shop->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></a>
                    <!--<a href="{{route('global-email-templates.delete',[$shop->id])}}"  data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure to delete this item?')"><i data-feather="trash"></i></a>-->
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection