@extends('admin.layouts.app')

@section('title')
Create Shops - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    <link href="{{ asset('themes/dashforge/lib/quill/quill.core.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/quill/quill.bubble.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-12"><h4 class="mg-b-0">Create New Shop</h4></div>
  
</div>


<div class="row mg-t-20 mg-b-20">
    <div class="col-sm-9">
        <form method="post" action="{{route('shops.store')}}" class="form" novalidate>
            {{csrf_field()}}
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Company Name <span class="text-danger">*</span></label>
                    <input type="text" value="{{old('company_name')}}" id="companyname" class="form-control" placeholder="Leong Sdn Bhd" name="company_name" required>
                    @if ($errors->has('company_name'))
                    <span class="text-danger">{{ $errors->first('company_name') }}</span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="contact_num">Contact #</label>
                    <input type="text" value="{{old('contact_num')}}" id="contact_num" class="form-control" placeholder="+601118896855" name="contact_num">
                    @if ($errors->has('contact_num'))
                    <span class="text-danger">{{ $errors->first('contact_num') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="inputAddress">Address1<span class="text-danger">*</span></label>
                <input type="text" value="{{old('address1')}}" id="address1" class="form-control" placeholder="458, JLN Kepong" name="address1" required>
                @if ($errors->has('address1'))
                <span class="text-danger">{{ $errors->first('address1') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="inputAddress2">Address2</label>
                <input type="text" value="{{old('address2')}}" id="address2" class="form-control" placeholder="Apartment, studio, or floor" name="address2">
                @if ($errors->has('address2'))
                <span class="text-danger">{{ $errors->first('address2') }}</span>
                @endif
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="inputCity">City<span class="text-danger">*</span></label>
                    <input type="text" value="{{old('city')}}" id="city" class="form-control" placeholder="Kepong" name="city" required>
                    @if ($errors->has('city'))
                    <span class="text-danger">{{ $errors->first('city') }}</span>
                    @endif
                </div>
                <div class="form-group col-md-3">
                    <label for="inputpostcode">Postcode<span class="text-danger">*</span></label>
                    <input type="text" value="{{old('post_code')}}" id="post_code" class="form-control" placeholder="75000" name="post_code" required>
                    @if ($errors->has('post_code'))
                    <span class="text-danger">{{ $errors->first('post_code') }}</span>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="shop_state">State <span class="tx-danger">*</span></label>
                    <select name="state" class="custom-select" required12>
                        <option value="">Select</option>
                        @foreach($states as $key=>$state)
                            <option data-id="{{$state->id}}" value='{{$state->state}}'>{{$state->state}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('state'))
                    <span class="text-danger">{{ $errors->first('state') }}</span>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                <label for="manager_id" class="">Regional Manager</label>
                <select name="manager_id" class="custom-select">
                    <option value="" selected>--Select Regional Manager--</option>
                    @foreach($managers as $key=>$manager)
                    <option value='{{$manager->id}}'>{{$manager->name}} ({{$manager->region}})</option>
                    @endforeach
                </select>
                @if ($errors->has('manager_id'))
                <span class="text-danger">{{ $errors->first('manager_id') }}</span>
                @endif
            </div>
            
            <div class="form-group">
                <label for="sales_id" class="">Sales PIC</label>
                <select name="sales_id" class="custom-select">
                    <option value="" selected>--Select Regional Manager--</option>
                    @foreach($sales as $key=>$sale)
                    <option value='{{$sale->id}}'>{{$sale->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('sales_id'))
                <span class="text-danger">{{ $errors->first('sales_id') }}</span>
                @endif
            </div>

            <div class="form-row">
                <label for="summary">Summary</label>
                <textarea id="summary" rows="3" class="form-control" name="summary" placeholder="About shop"></textarea>
                @if ($errors->has('summary'))
                <span class="text-danger">{{ $errors->first('summary') }}</span>
                @endif
            </div>

            <div class="form-row">
                <div class="form-group">
                    <label for="status">Status <span class="text-danger">*</span></label>
                    <select name="status" class="form-control">
                        <option value="" selected>Pick Status</option>
                        <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                    </select>
                    @if ($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-actions">
                <a href="{{route('shops.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>

                <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>

    </div>
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
@endsection

@section('page-script')

@endsection