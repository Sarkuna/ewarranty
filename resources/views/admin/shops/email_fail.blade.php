@extends('admin.layouts.appGuest')

@section('title')
Edit Shops - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    <link href="{{ asset('themes/dashforge/lib/quill/quill.core.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/quill/quill.bubble.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-12">
      
      <h3>Oops, something went wrong!</h3>
      <p>Hmm. Looks like the link has expired.</p>
  
  </div>
  
</div>
@endsection

@section('vendor-script')
     <!-- Start datatable js -->

@endsection

@section('page-script')

@endsection