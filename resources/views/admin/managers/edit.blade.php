@extends('admin.layouts.app')

@section('title')
Create Regional Manager - Admin Panel
@endsection

@section('vendor-style')
    <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-12"><h4 class="mg-b-0">Edit - {{$model->name}}</h4></div>
  
</div>
<form method="post" action="{{route('managers.update',$model->id)}}" data-parsley-validate class="mg-t-20 mg-b-20">
    {{ method_field('PUT') }}
    {{ csrf_field() }} 
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label required">Name</label>
        <div class="col-sm-10">
            <input type="text" value="{{$model->name}}" id="name" class="form-control" placeholder="Lim Hong" name="name" required>
            @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label required">Email</label>
        <div class="col-sm-10">
            <input type="text" value="{{$model->email}}" id="email" class="form-control" placeholder="name@domain.com" name="email" required>
            @if ($errors->has('email'))
            <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>
    
    <div class="form-group row">
        <label for="region" class="col-sm-2 col-form-label required">Region</label>
        <div class="col-sm-10">
            <select class="custom-select" name="region" required>
                <option value="" selected>Select</option>
                <option value="Central" {{ ($model->region=="Central")? "selected" : "" }}>Central</option>
                <option value="East Coast" {{ ($model->region=="East Coast")? "selected" : "" }}>East Coast</option>
                <option value="Northern" {{ ($model->region=="Northern")? "selected" : "" }}>Northern</option>
                <option value="Sabah" {{ ($model->region=="Sabah")? "selected" : "" }}>Sabah</option>
                <option value="Sarawak" {{ ($model->region=="Sarawak")? "selected" : "" }}>Sarawak</option>
                <option value="Southern" {{ ($model->region=="Southern")? "selected" : "" }}>Southern</option>
            </select>
            @if ($errors->has('region'))
            <span class="text-danger">{{ $errors->first('region') }}</span>
            @endif
        </div>
    </div>
    
    <div class="form-group row">
        <label for="assign-role" class="col-sm-2 col-form-label required">Assign Roles</label>
        <div class="col-sm-10">
            <select name="roles[]" id="roles" class="form-control select2" multiple="multiple">
                @foreach ($roles as $role)
                <option value="{{ $role->name }}" {{ $model->hasRole($role->name) ? 'selected' : '' }}>{{ $role->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('roles'))
            <span class="text-danger">{{ $errors->first('roles') }}</span>
            @endif
        </div>
    </div>
    
    <div class="form-group row">
        <label for="status" class="col-sm-2 col-form-label required">Status</label>
        <div class="col-sm-10">
            <select class="custom-select" name="status" required>
                <option value="" selected>Select</option>
                <option value="active" {{ ($model->status=="active")? "selected" : "" }}>Active</option>
                <option value="inactive" {{ ($model->status=="inactive")? "selected" : "" }}>Inactive</option>
            </select>
            @if ($errors->has('status'))
            <span class="text-danger">{{ $errors->first('status') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group row mg-b-0">
        <div class="col-sm-10 offset-2">
            <a href="{{route('managers.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>

            <button type="submit" class="btn btn-primary">
                <i class="la la-check-square-o"></i> Save
            </button>
        </div>
    </div>
</form>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
<script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      // Adding placeholder for search input
      (function($) {

        'use strict'

        var Defaults = $.fn.select2.amd.require('select2/defaults');

        $.extend(Defaults.defaults, {
          searchInputPlaceholder: ''
        });

        var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');

        var _renderSearchDropdown = SearchDropdown.prototype.render;

        SearchDropdown.prototype.render = function(decorated) {

          // invoke parent method
          var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));

          this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));

          return $rendered;
        };

      })(window.jQuery);


      $(function(){
        'use strict'

        // Basic with search
        $('.select2').select2({
          placeholder: 'Choose one',
          searchInputPlaceholder: 'Search options'
        });

        // Disable search
        $('.select2-no-search').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one'
        });

        // Clearable selection
        $('.select2-clear').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one',
          allowClear: true
        });

        // Limit selection
        $('.select2-limit').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one',
          maximumSelectionLength: 2
        });

      });
    </script>
@endsection