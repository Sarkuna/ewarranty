@extends('admin.layouts.app')

@section('title')
Regional Sales Manager - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')


<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">All Regional Manager</h4></div>
  <div class="col-sm-4 text-right">
      <a class="btn btn-primary mr-1 mb-1 btn-sm" href="{{ route('managers.create') }}">Create New</a>
  </div>
</div>

<div class="df-example demo-table">
    <table id="example1" class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Region</th>
                <th>Last Login</th>
                <th>Status</th>
                <th class="text-center">View</th>
            </tr>
        </thead>
        <tbody>
            @if(count($models) > 0)
            @foreach ($models as $model)                                
            <tr id="category_id_{{ $model->id }}">
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $model->name }}</td>
                <td>{{ $model->email }}</td>
                <td>{{ $model->region }}</td>
                <td>
                    @if(!empty($model->date_last_login))
                    {{ $model->date_last_login->format('d-m-Y') }}
                    @else
                    N/A
                    @endif
                    
                </td>
                <td>{{ ucwords($model->status) }}</td>
                <td class="text-center">
                <!--<a href="" id="view" data-toggle="tooltip" data-placement="bottom" title="View"><i class="far fa-eye"></i></a>-->
                <a href="{{route('managers.edit',$model->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></a>
                </td>
            </tr>
            @endforeach
            @else
            <tr><td class="text-center" colspan="6">No records found!!!</td></tr>
            @endif
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection