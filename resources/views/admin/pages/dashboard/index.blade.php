@extends('admin.layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">

@endsection

@section('content')

<h4 class="mg-b-10 tx-spacing--1">Welcome to Dashboard</h4>
<div class="row row-xs">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-header d-sm-flex justify-content-between bd-b-0 pd-t-20 pd-b-0">
                <div class="mg-b-10 mg-sm-b-0">
                  <h6 class="mg-b-5">Current Submissions Status</h6>
                  <p class="tx-12 tx-color-03 mg-b-0"></p>
                </div>
                <ul class="list-inline tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03 mg-b-0">
                  <li class="list-inline-item">
                    <span class="d-inline-block wd-7 ht-7 bg-gray-400 rounded-circle mg-r-5"></span>
                    In Progress
                  </li>
                  <li class="list-inline-item mg-l-10">
                    <span class="d-inline-block wd-7 ht-7 bg-df-2 rounded-circle mg-r-5"></span>
                    Reviews
                  </li>
                  <li class="list-inline-item mg-l-10">
                    <span class="d-inline-block wd-7 ht-7 bg-primary rounded-circle mg-r-5"></span>
                    Approved
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="chart-fifteen">
                  <div id="flotChart1" class="flot-chart"></div>
                </div>
              </div><!-- card-body -->
              <div class="card-footer pd-y-15 pd-x-20">
                <div class="row row-sm">
                  <div class="col-6 col-sm-4 col-md-3 col-lg">
                    <h4 class="tx-normal tx-rubik mg-b-10">{{ $accepted }}</h4>
                    <div class="progress ht-2 mg-b-10">
                      <div class="progress-bar wd-100p bg-df-2" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h6 class="tx-uppercase tx-spacing-1 tx-semibold tx-10 tx-color-02 mg-b-2">In Progress</h6>
                    
                  </div><!-- col -->
                  <div class="col-6 col-sm-4 col-md-3 col-lg">
                    <h4 class="tx-normal tx-rubik mg-b-10">{{ $review }}</h4>
                    <div class="progress ht-2 mg-b-10">
                      <div class="progress-bar wd-85p bg-df-2" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h6 class="tx-uppercase tx-spacing-1 tx-semibold tx-10 tx-color-02 mg-b-2">Reviews</h6>
                    
                  </div><!-- col -->
                  <div class="col-6 col-sm-4 col-md-3 col-lg mg-t-20 mg-sm-t-0">
                    <h4 class="tx-normal tx-rubik mg-b-10">{{ $approved }}</h4>
                    <div class="progress ht-2 mg-b-10">
                      <div class="progress-bar wd-25p bg-df-2" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h6 class="tx-uppercase tx-spacing-1 tx-semibold tx-10 tx-color-02 mg-b-2">Approved</h6>
                    
                  </div><!-- col -->
                </div><!-- row -->
              </div><!-- card-footer -->
            </div><!-- card -->
            
            <div class="row row-xs mg-t-10">


                <div class="col-12 col-md-6 col-lg-12 mg-t-10">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h6 class="mg-b-0">Transaction History</h6>
                        </div>
                        @if(count($transaction_history) > 0)
                            @foreach ($transaction_history as $model)
                            <ul class="list-group list-group-flush tx-13">
                                <li class="list-group-item d-flex pd-sm-x-20">
                                    <div class="avatar d-none d-sm-block">
                                        
                                            @if($model->status == 'approved')
                                                <span class="avatar-initial rounded-circle bg-success op-5">
                                                <i class="icon ion-md-checkmark"></i>
                                                </span>
                                            @elseif($model->status == 'review')
                                                <span class="avatar-initial rounded-circle bg-primary op-5">
                                                <i class="icon ion-md-create"></i>
                                                </span>
                                            @else
                                                <span class="avatar-initial rounded-circle bg-warning op-5">
                                                <i class="icon ion-md-help"></i>
                                                </span>
                                            @endif
                                        
                                    </div>
                                    <div class="pd-sm-l-10">
                                        <p class="tx-medium mg-b-2">{{ $model->warranty_code }} </p>
                                        <small class="tx-12 tx-color-03 mg-b-0">{{ $model->created_at->format('M d, Y') }}</small>
                                    </div>
                                    <div class="mg-l-auto text-right">
                                        <p class="tx-medium mg-b-2">{{$model->product_name($model->product_year) }}</p>
                                        @if($model->status == 'approved')
                                            <small class="tx-12 tx-success mg-b-0">{{ ucwords($model->status) }}</small>
                                        @elseif($model->status == 'review')
                                            <small class="tx-12 tx-primary mg-b-0">{{ ucwords($model->status) }}</small>
                                        @else
                                            <small class="tx-12 tx-warning mg-b-0">{{ ucwords($model->status) }}</small>
                                        @endif
                                        
                                    </div>
                                </li>
                            </ul>
                            @endforeach
                        @else
                        <p class="text-center">No records found!!!</p>
                        @endif
                        
                        <div class="card-footer text-center tx-13">
                            <a href="{{ route('warranty.index') }}" class="link-03">View All Transactions <i class="icon ion-md-arrow-down mg-l-5"></i></a>
                        </div><!-- card-footer -->
                    </div><!-- card -->
                </div><!-- col -->  
            </div><!-- row -->
          </div><!-- col -->
          
          <div class="col-lg-4 mg-t-10 mg-lg-t-0">
            <div class="row row-xs">
              <div class="col-12 col-md-6 col-lg-12">
                <div class="card card-body">
                  <div class="media d-block d-sm-flex align-items-center">
                    <div class="d-inline-block pos-relative">
                      <span class="peity-donut" data-peity='{ "fill": ["#65e0e0","#e5e9f2"], "height": 110, "width": 110, "innerRadius": 46 }'>70,30</span>

                      <div class="pos-absolute a-0 d-flex flex-column align-items-center justify-content-center">
                        <h3 class="tx-rubik tx-spacing--1 mg-b-0">{{$product_year7P}}%</h3>
                        <span class="tx-9 tx-semibold tx-sans tx-color-03 tx-uppercase">Reached</span>
                      </div>
                    </div>
                    <div class="media-body mg-t-20 mg-sm-t-0 mg-sm-l-20">
                      <h6 class="mg-b-5">Dulux Weathershield</h6>
                      <p class="lh-4 tx-12 tx-color-03 mg-b-15">The average time taken to resolve complaints.</p>
                      <h3 class="tx-spacing--1 mg-b-0">{{ $product_year7 }} <small class="tx-13 tx-color-03">/ {{ $purchase_all }}</small></h3>
                    </div><!-- media-body -->
                  </div><!-- media -->
                </div>
              </div><!-- col -->
              
              <div class="col-12 col-md-6 col-lg-12 mg-t-10 mg-md-t-0 mg-lg-t-10">
                <div class="card card-body">
                  <div class="media d-block d-sm-flex align-items-center">
                    <div class="d-inline-block pos-relative">
                      <span class="peity-donut" data-peity='{ "fill": ["#69b2f8","#e5e9f2"], "height": 110, "width": 110, "innerRadius": 46 }'>69,31</span>

                      <div class="pos-absolute a-0 d-flex flex-column align-items-center justify-content-center">
                        <h3 class="tx-rubik tx-spacing--1 mg-b-0">{{$product_year12P}}%</h3>
                        <span class="tx-9 tx-semibold tx-sans tx-color-03 tx-uppercase">Reached</span>
                      </div>
                    </div>
                    <div class="media-body mg-t-20 mg-sm-t-0 mg-sm-l-20">
                      <h6 class="mg-b-5">Dulux Weathershield Powerflexx</h6>
                      <p class="lh-4 tx-12 tx-color-03 mg-b-15">Measure how quickly support staff answer incoming calls.</p>
                      <h3 class="tx-spacing--1 mg-b-0">{{ $product_year12 }} <small class="tx-13 tx-color-03">/ {{ $purchase_all }}</small></h3>
                    </div><!-- media-body -->
                  </div><!-- media -->
                </div>
              </div><!-- col -->
              
              <div class="col-12 col-md-6 col-lg-12 mg-t-10">
                <div class="card">
                  <div class="card-header pd-t-20 pd-b-0 bd-b-0 d-flex justify-content-between">
                    <h6 class="lh-5 mg-b-0">Submissions Received</h6>
                    <a href="" class="tx-13 link-03">This Month <i class="icon ion-ios-arrow-down tx-12"></i></a>
                  </div><!-- card-header -->
                  <div class="card-body pd-0 pos-relative">
                    <div class="pos-absolute t-10 l-20 z-index-10">
                      <div class="d-flex align-items-baseline">
                        <h1 class="tx-normal tx-rubik mg-b-0 mg-r-5">{{ $purchase_all }}</h1>
                        <p class="tx-11 tx-color-03 mg-b-0"><span class="tx-medium tx-success">0.3% <i class="icon ion-md-arrow-down"></i></span> than last month</p>
                      </div>
                      <p class="tx-12 tx-color-03 wd-60p">The total number of complaints that have been received.</p>
                    </div>

                    <div class="chart-sixteen">
                      <div id="flotChart2" class="flot-chart"></div>
                    </div>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->

            </div><!-- row -->
          </div><!-- col -->
        </div><!-- row -->
@endsection  


@section('vendor-script')
     <!-- Start datatable js -->   
    <script src="{{ asset('themes/dashforge/lib/jquery.flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/jquery.flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/jquery.flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/flot.curvedlines/curvedLines.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/chart.js/Chart.bundle.min.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('themes/dashforge/assets/js/dashforge.sampledata.js') }}"></script>
    <script src="{{ asset('themes/dashforge/assets/js/dashboard-four.js') }}"></script>

    <!-- append theme customizer -->
@endsection