@extends('admin.layouts.app')
@section('title', 'Password')

@section('vendor-style')
  <!-- vendor css files -->
<link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">

@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Change Password</div>

            <div class="card-body">
                <form id="form1" method="post" action="{{ route('admin.change.password') }}" class="parsley-style-1" enctype="multipart/form-data" data-parsley-validate novalidate>
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Current Password</label>
                        <div class="col-md-6">
                            <input type="password" id="current_password" class="form-control" placeholder="Current Password" name="current_password">
                            @if ($errors->has('current_password'))
                            <span class="text-danger">{{ $errors->first('current_password') }}</span>
                            @endif            
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="new_password" class="col-md-4 col-form-label text-md-right">New Password</label>

                        <div class="col-md-6">
                            <input type="password" id="new_password" class="form-control" placeholder="New Password" name="new_password">
                            <div id='password-strength-status'></div>
                            @if ($errors->has('new_password'))
                            <span class="text-danger">{{ $errors->first('new_password') }}</span>
                            @endif
                            <p id="passwordHelpBlock" class="form-text text-muted">
                                Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.
                            </p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="new_confirm_password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>

                        <div class="col-md-6">
                            <input type="password" id="new_confirm_password" class="form-control" placeholder="New Confirm Password" name="new_confirm_password">
                            @if ($errors->has('new_confirm_password'))
                            <span class="text-danger">{{ $errors->first('new_confirm_password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

 @endsection  
 
 @section('vendor-script')
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>

 @endsection
 
 @section('page-script')
 <script>
    $("#new_password").keyup(function() {
        var number = /([0-9])/;
        var lowercase = /([a-z])/;
        var uppercase = /([A-Z])/;
        var special = /([!@#$%^&*])/;
        if ($('#new_password').val().length < 8) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('alert alert-danger');
            $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
        } else {
            if ($('#new_password').val().match(number) && $('#new_password').val().match(lowercase) && $('#new_password').val().match(uppercase) && $('#new_password').val().match(special)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-success');
                $('#password-strength-status').html("Strong");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('alert alert-warning');
                $('#password-strength-status').html("Medium (should include a Uppercase, numbers, special character and Lowercase.)</>");
            }
        }
    });
    </script>
 
 @endsection