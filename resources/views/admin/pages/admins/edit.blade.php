
@extends('admin.layouts.app')

@section('title')
Admin Edit - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    <link href="{{ asset('themes/dashforge/lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('page-style')
    <!-- Start datatable css -->
    
@endsection

@section('content')

<!-- page title area start -->
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">Create Admin</h4></div>
  <div class="col-sm-4 text-right">
      
  </div>
</div>
<!-- page title area end -->


<form action="{{ route('admin.admins.update', $admin->id) }}" method="POST" data-parsley-validate>
    {{ csrf_field() }}
    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12">
            <label for="name">Admin Name <span class="tx-danger">*</span></label>
            <input type="text" class="form-control" id="name" value="{{ $admin->name }}" name="name" placeholder="Enter Name" required>
            @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
        </div>
        <div class="form-group col-md-6 col-sm-12">
            <label for="email">Admin Email <span class="tx-danger">*</span></label>
            <input type="text" class="form-control" id="email" value="{{ $admin->email }}" name="email" placeholder="Enter Email" required>
            @if ($errors->has('email'))
            <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12">
            <label for="password">Password <span class="tx-danger">*</span></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
            @if ($errors->has('password'))
            <span class="text-danger">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="form-group col-md-6 col-sm-12">
            <label for="password_confirmation">Confirm Password <span class="tx-danger">*</span></label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Password" required>
            @if ($errors->has('password_confirmation'))
            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6 col-sm-6">
            <label for="password">Assign Roles <span class="tx-danger">*</span></label>
            <select name="roles[]" id="roles" class="form-control select2" multiple="multiple">
                @foreach ($roles as $role)
                    <option value="{{ $role->name }}" {{ $admin->hasRole($role->name) ? 'selected' : '' }}>{{ $role->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('roles'))
            <span class="text-danger">{{ $errors->first('roles') }}</span>
            @endif
        </div>
    </div>

    
    <a href="{{route('admin.admins.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>

    <button type="submit" class="btn btn-primary">
        <i class="la la-check-square-o"></i> Save
    </button>
</form>

@endsection


@section('vendor-script')
<script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection


@section('page-script')
<script>
      // Adding placeholder for search input
      (function($) {

        'use strict'

        var Defaults = $.fn.select2.amd.require('select2/defaults');

        $.extend(Defaults.defaults, {
          searchInputPlaceholder: ''
        });

        var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');

        var _renderSearchDropdown = SearchDropdown.prototype.render;

        SearchDropdown.prototype.render = function(decorated) {

          // invoke parent method
          var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));

          this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));

          return $rendered;
        };

      })(window.jQuery);


      $(function(){
        'use strict'

        // Basic with search
        $('.select2').select2({
          placeholder: 'Choose one',
          searchInputPlaceholder: 'Search options'
        });

        // Disable search
        $('.select2-no-search').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one'
        });

        // Clearable selection
        $('.select2-clear').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one',
          allowClear: true
        });

        // Limit selection
        $('.select2-limit').select2({
          minimumResultsForSearch: Infinity,
          placeholder: 'Choose one',
          maximumSelectionLength: 2
        });

      });
    </script>
@endsection