@extends('admin.layouts.app')

@section('title')
Role Edit - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
@endsection

@section('page-style')
    <!-- Start datatable css -->
    
    
    
@endsection

@section('content')
<div class="df-example">
<form action="{{ route('admin.roles.update', $role->id) }}" method="POST">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-sm-12">
    <div class="form-group">
        <label for="name">Role Name</label>
        <input type="text" class="form-control" id="name" value="{{ $role->name }}" name="name" placeholder="Enter a Role Name">
    </div>
        </div>
        <div class="col-sm-12">
    <div class="form-group">
        <label for="name">Permissions</label>

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="checkPermissionAll" value="1" {{ App\User::roleHasPermissions($role, $all_permissions) ? 'checked' : '' }}>
                   <label class="form-check-label" for="checkPermissionAll">All</label>
        </div>
        <hr>
        @php $i = 1; @endphp
        @foreach ($permission_groups as $group)
        <div class="row">
            @php
            $permissions = App\User::getpermissionsByGroupName($group->name);
            $j = 1;
            @endphp

            <div class="col-3">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="{{ $i }}Management" value="{{ $group->name }}" onclick="checkPermissionByGroup('role-{{ $i }}-management-checkbox', this)" {{ App\User::roleHasPermissions($role, $permissions) ? 'checked' : '' }}>
                           <label class="form-check-label" for="checkPermission">{{ $group->name }}</label>
                </div>
            </div>

            <div class="col-9 role-{{ $i }}-management-checkbox">

                @foreach ($permissions as $permission)
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" onclick="checkSinglePermission('role-{{ $i }}-management-checkbox', '{{ $i }}Management', {{ count($permissions) }})" name="permissions[]" {{ $role->hasPermissionTo($permission->name) ? 'checked' : '' }} id="checkPermission{{ $permission->id }}" value="{{ $permission->name }}">
                    <label class="form-check-label" for="checkPermission{{ $permission->id }}">{{ $permission->name }}</label>
                </div>
                @php  $j++; @endphp
                @endforeach
                <br>
            </div>

        </div>
        @php  $i++; @endphp
        @endforeach


    </div>
        </div>
    </div>
    <button type="submit" class="btn btn-block btn-primary">Update Role</button>
</form>
</div>                    

@endsection

@section('vendor-script')
@endsection


@section('page-script')
     @include('admin.pages.roles.partials.scripts')
@endsection