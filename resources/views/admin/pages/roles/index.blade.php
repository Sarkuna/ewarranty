@extends('admin.layouts.app')

@section('title')
Roles - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">All Roles</h4></div>
  <div class="col-sm-4 text-right">
      @if (Auth::guard('admin')->user()->can('role.create'))
      <a class="btn btn-primary mr-1 mb-1 btn-sm" href="{{ route('admin.roles.create') }}">Create New Role</a>
      @endif
  </div>
</div>



<div class="df-example demo-table">
    <table id="example1" class="table">
        <thead>
            <tr>
                <th>#</th>
                <th class="wd-20p">Name</th>
                <th class="wd-65p">Permissions</th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($roles as $role)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $role->name }}</td>
                <td class="text-wrap">
                    @foreach ($role->permissions as $perm)
                    <span class="badge badge-info mr-1">
                        {{ $perm->name }}
                    </span>
                    @endforeach
                </td>
                <td>
                    @if (Auth::guard('admin')->user()->can('role.edit'))
                    <a href="{{ route('admin.roles.edit', $role->id) }}"><i class="far fa-edit"></i></a>
                    @endif

                    @if (Auth::guard('admin')->user()->can('role.delete'))
                    <a href="{{route('admin.roles.delete', $role->id)}}" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure to delete this item?')"><i class="fa fa-trash"></i></a>
                    
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection