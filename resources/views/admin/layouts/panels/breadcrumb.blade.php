@if(@isset($breadcrumbs))
<ol class="breadcrumb df-breadcrumbs mg-b-10">
    @foreach ($breadcrumbs as $breadcrumb)
    <li class="breadcrumb-item">
        @if(isset($breadcrumb['link']))
        <a href="{{ $breadcrumb['link'] == 'javascript:void(0)' ? $breadcrumb['link']:url($breadcrumb['link']) }}">
            @endif
            {{$breadcrumb['name']}}
            @if(isset($breadcrumb['link']))
        </a>
        @endif
    </li>
    @endforeach
</ol>
@endisset