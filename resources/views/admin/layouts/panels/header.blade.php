<header class="navbar navbar-header navbar-header-fixed">
      <a href="" id="sidebarMenuOpen" class="burger-menu"><i data-feather="arrow-left"></i></a>
      <div class="navbar-brand">
        <a href="/" class="df-logo">Akzo<span>Nobel</span></a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="/" class="df-logo">Akzo<span>Nobel</span></a>
          <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
      </div><!-- nav-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">
            <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
                <div class="avatar avatar-sm"><img src="{{ Helper::admin_avatar() }}" class="rounded-circle" alt=""></div>
            </a><!-- dropdown-link -->
            <div class="dropdown-menu dropdown-menu-right tx-13">
                
                <h6 class="tx-semibold mg-b-5">{{ Auth::guard('admin')->user()->name }}</h6>
                <p class="mg-b-25 tx-12 tx-color-03">{{ Auth::guard('admin')->user()->email }}</p>

                <!--<a href="" class="dropdown-item"><i data-feather="edit-3"></i> Edit Profile</a>-->
                <a href="{{ route('admin.change.password.form') }}" class="dropdown-item"><i data-feather="lock"></i> Change Password</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void" onclick="$('#logout-form').submit();"><i data-feather="log-out"></i> Logout</a>
                <form id="logout-form" action="{{ route('admin.logout.submit') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- az-header-right -->
    </header><!-- navbar -->