@php
     $usr = Auth::guard('admin')->user();
 @endphp
<div id="sidebarMenu" class="sidebar sidebar-fixed sidebar-components">
      <div class="sidebar-header">
        <a href="" id="mainMenuOpen"><i data-feather="menu"></i></a>
        <h5>Akzo Nobel</h5>
        <a href="" id="sidebarMenuClose"><i data-feather="x"></i></a>
      </div><!-- sidebar-header -->
      <div class="sidebar-body">
        <ul class="sidebar-nav">

          <li class="nav-item"><a href="/" class="nav-link "><i data-feather="monitor"></i> Dashboard</a></li>
          
          @if ($usr->can('warranty.view') || $usr->can('warranty.accepted') ||  $usr->can('warranty.reviews') ||  $usr->can('warranty.approve') ||  $usr->can('warranty.decline'))
          <li class="nav-item {{ (request()->is('admin/warranties*')) ? 'show' : '' }}">
            <a href="" class="nav-link with-sub {{ (request()->is('admin/warranties*')) ? 'active' : '' }}"><i data-feather="layers"></i> Submissions</a>
            <nav class="nav">
              @if ($usr->can('warranty.view'))  
              <a href="{{ route('warranty.index') }}" class="{{ (request()->is('admin/warranties/warranty*')) ? 'active' : '' }}">All</a>
              @endif
              @if ($usr->can('warranty.accepted'))
              <a href="{{ route('warranty.accepted') }}" class="{{ (request()->is('admin/warranties/accepted*')) ? 'active' : '' }}">In Progress</a>
              @endif
              @if ($usr->can('warranty.reviews'))
              <a href="{{ route('warranty.reviews') }}" class="{{ (request()->is('admin/warranties/reviews*')) ? 'active' : '' }}">Reviews</a>
              @endif
              @if ($usr->can('warranty.approve'))
              <a href="{{ route('warranty.approve') }}" class="{{ (request()->is('admin/warranties/approve*')) ? 'active' : '' }}">Approved</a>
              @endif
              @if ($usr->can('warranty.decline'))
              <a href="{{ route('warranty.decline') }}" class="{{ (request()->is('admin/warranties/decline*')) ? 'active' : '' }}">Decline</a>
              @endif
            </nav>
          </li>
          @endif
          <li class="nav-item"><a href="{{ route('sales.index') }}" class="nav-link {{ (request()->is('admin/sales*')) ? 'active' : '' }}"><i data-feather="users"></i> My Sales PIC</a></li>
          <li class="nav-item"><a href="{{ route('shops.index') }}" class="nav-link {{ (request()->is('admin/shops*')) ? 'active' : '' }}"><i data-feather="briefcase"></i> My Shops</a></li>
        </ul>
      </div><!-- sidebar-body -->
    </div><!-- sidebar -->