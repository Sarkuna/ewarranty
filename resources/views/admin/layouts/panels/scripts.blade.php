{{-- Admin Vendor Scripts --}}
<script src="{{ asset('themes/dashforge/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

@yield('vendor-script')
<script type="text/javascript" src="{{ asset('themes/dashforge/lib/toastr/extensions/toastr.min.js') }}"></script>

{{-- Theme Scripts --}}
<script src="{{ asset('themes/dashforge/assets/js/dashforge.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/toastr/toastr.js') }}"></script>

{{-- page script --}}
 @include('panels/messages')
@yield('page-script')
{{-- page script --}}
