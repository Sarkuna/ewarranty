@php
     $usr = Auth::guard('admin')->user();
 @endphp
<div id="sidebarMenu" class="sidebar sidebar-fixed sidebar-components">
      <div class="sidebar-header">
        <a href="" id="mainMenuOpen"><i data-feather="menu"></i></a>
        <h5>Akzo Nobel</h5>
        <a href="" id="sidebarMenuClose"><i data-feather="x"></i></a>
      </div><!-- sidebar-header -->
      <div class="sidebar-body">
        <ul class="sidebar-nav">

         <li class="nav-item"><a href="/" class="nav-link "><i data-feather="monitor"></i> Dashboard</a></li>
          @if ($usr->can('warranty.view') || $usr->can('warranty.accepted') ||  $usr->can('warranty.reviews') ||  $usr->can('warranty.approve') ||  $usr->can('warranty.decline'))
          <li class="nav-item {{ (request()->is('admin/warranties*')) ? 'show' : '' }}">
            <a href="" class="nav-link with-sub {{ (request()->is('admin/warranties*')) ? 'active' : '' }}"><i data-feather="layers"></i> Submissions</a>
            <nav class="nav">
              @if ($usr->can('warranty.on.behalf'))  
              <a href="{{ route('warranty.onbehalf') }}" class="{{ (request()->is('admin/warranties/on-behalf*')) ? 'active' : '' }}">New Submission</a>
              @endif  
              @if ($usr->can('warranty.view'))  
              <a href="{{ route('warranty.index') }}" class="{{ (request()->is('admin/warranties/warranty*')) ? 'active' : '' }}">All</a>
              @endif
              @if ($usr->can('warranty.accepted'))
              <a href="{{ route('warranty.accepted') }}" class="{{ (request()->is('admin/warranties/accepted*')) ? 'active' : '' }}">In Progress</a>
              @endif
              @if ($usr->can('warranty.reviews'))
              <a href="{{ route('warranty.reviews') }}" class="{{ (request()->is('admin/warranties/reviews*')) ? 'active' : '' }}">Reviews</a>
              @endif
              @if ($usr->can('warranty.approve'))
              <a href="{{ route('warranty.approve') }}" class="{{ (request()->is('admin/warranties/approve*')) ? 'active' : '' }}">Approved</a>
              @endif
              @if ($usr->can('warranty.decline'))
              <a href="{{ route('warranty.decline') }}" class="{{ (request()->is('admin/warranties/decline*')) ? 'active' : '' }}">Declined</a>
              @endif
            </nav>
          </li>
          @endif
          
          @if ($usr->can('managers.my.sales.team'))
          <li class="nav-item"><a href="{{ route('my.sales.team') }}" class="nav-link {{ (request()->is('admin/managers/my-sales-team')) ? 'active' : '' }}"><i data-feather="users"></i> My Sales Team</a></li>
          @endif
          @if ($usr->can('user.view'))
          <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link {{ (request()->is('admin/users*')) ? 'active' : '' }}"><i data-feather="users"></i> Users</a></li>
          @endif
          
          @if ($usr->can('managers.view'))
          <li class="nav-item"><a href="{{ route('managers.index') }}" class="nav-link {{ (request()->is('admin/managers*')) ? 'active' : '' }}"><i data-feather="users"></i> Regional Manager</a></li>
          @endif
          @if ($usr->can('user.view'))
          <li class="nav-item"><a href="{{ route('sales.index') }}" class="nav-link {{ (request()->is('admin/sales*')) ? 'active' : '' }}"><i data-feather="users"></i> Sales PIC</a></li>
          @endif
          @if ($usr->can('shop.view'))
            <li class="nav-item"><a href="{{ route('shops.index') }}" class="nav-link {{ (request()->is('admin/shops*')) ? 'active' : '' }}"><i data-feather="briefcase"></i> Shops</a></li>
          @endif
          
          @if ($usr->can('email.view') || $usr->can('role.view') ||  $usr->can('admin.view')) 
          <li class="nav-item {{ (request()->is('admin/settings*')) ? 'show' : '' }}">
            <a href="" class="nav-link with-sub {{ (request()->is('admin/settings*')) ? 'active' : '' }}"><i data-feather="settings"></i> Settings</a>
            <nav class="nav">
              
              @if ($usr->can('email.view'))  
              <a href="{{ route('global-email-templates.index') }}" class="{{ (request()->is('admin/settings/global-email-templates*')) ? 'active' : '' }}">Email Templates</a>
              @endif
              
              @if ($usr->can('role.create') || $usr->can('role.view') ||  $usr->can('role.edit') ||  $usr->can('role.delete'))
              <a href="{{ route('admin.roles.index') }}" class="{{ (request()->is('admin/settings/roles*')) ? 'active' : '' }}">Roles</a>
              @endif
              
              @if ($usr->can('admin.create') || $usr->can('admin.view') ||  $usr->can('admin.edit') ||  $usr->can('admin.delete'))
              <a href="{{ route('admin.admins.index') }}" class="{{ (request()->is('admin/settings/admins*')) ? 'active' : '' }}">Admins</a>
              @endif
            </nav>
          </li>
          @endif
          
          <li class="nav-item"><a href="{{ route('admin.change.password.form') }}" class="nav-link "><i data-feather="lock"></i> Change Password</a></li>
          <li class="nav-item"><a href="javascript:void" onclick="$('#logout-form').submit();" class="nav-link "><i data-feather="log-out"></i> Logout</a></li>
                <form id="logout-form" action="{{ route('admin.logout.submit') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
        </ul>
      </div><!-- sidebar-body -->
    </div><!-- sidebar -->