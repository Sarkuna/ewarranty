<header class="navbar navbar-header navbar-header-fixed">
      <a href="" id="sidebarMenuOpen" class="burger-menu"><i data-feather="arrow-left"></i></a>
      <div class="navbar-brand">
        <a href="/" class="df-logo">Akzo<span>Nobel</span></a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="/" class="df-logo">Akzo<span>Nobel</span></a>
          <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
      </div><!-- nav-wrapper -->
    </header><!-- navbar -->