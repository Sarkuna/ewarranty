@php
     $usr = Auth::guard('admin')->user();
 @endphp
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                        <div class="brand-logo"></div>
                        <h2 class="brand-text mb-0">Dulux</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
                <li><a href="/"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Home</span></a></li>

                <li class=" navigation-header"><span>Apps</span></li>
                @if ($usr->can('role.create') || $usr->can('role.view') ||  $usr->can('role.edit') ||  $usr->can('role.delete'))
                    
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="bulb"></i><span class="menu-title" data-i18n="Icons">Roles & Permissions</span></a>
                    <ul class="menu-content {{ Route::is('admin.roles.create') || Route::is('admin.roles.index') || Route::is('admin.roles.edit') || Route::is('admin.roles.show') ? 'in' : '' }}">
                        @if ($usr->can('role.view'))
                        <li><a href="{{ route('admin.roles.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="LivIcons">All Roles</span></a></li>
                        @endif
                        @if ($usr->can('role.create'))
                        <li><a href="{{ route('admin.roles.create') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="BoxIcons">Create Role</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="envelope-pull"></i><span class="menu-title" data-i18n="Email">Manage Shops</span></a>
                </li>
                <li class=" nav-item"><a href="app-chat.html"><i class="menu-livicon" data-icon="comments"></i><span class="menu-title" data-i18n="Chat">Manage Sales PIC</span></a>
                </li>
                <li class=" nav-item"><a href="app-todo.html"><i class="menu-livicon" data-icon="check-alt"></i><span class="menu-title" data-i18n="Todo">Regianal Managers</span></a>
                </li>
                <li class=" nav-item"><a href="app-calendar.html"><i class="menu-livicon" data-icon="calendar"></i><span class="menu-title" data-i18n="Calendar">Manage Warranties</span></a>
                </li>
                
                <li class=" navigation-header"><span>Reports</span></li>
                
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="drop"></i><span class="menu-title" data-i18n="Colors">Reports 1</span></a>
                </li>
                
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="drop"></i><span class="menu-title" data-i18n="Colors">Reports 1</span></a>
                </li>

                <li class=" navigation-header"><span>Settings</span></li>
                
                <li class=" nav-item"><a href="{{ route('global-email-templates.index') }}"><i class="menu-livicon" data-icon="settings"></i><span class="menu-title" data-i18n="Form Layout">Email Templates</span></a>
                </li>

                <li class=" navigation-header"><span>Support</span>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="morph-folder"></i><span class="menu-title" data-i18n="Documentation">Documentation</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="help"></i><span class="menu-title" data-i18n="Raise Support">Raise Support</span></a>
                </li>
            </ul>
        </div>
    </div>