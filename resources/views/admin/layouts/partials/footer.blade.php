<footer class="footer footer-static footer-light">
        <p class="clearfix mb-0">
            <span class="float-left d-inline-block">Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script></span>
            <span class="float-right d-sm-inline-block d-none">All rights Reserved
            <a class="text-uppercase" href="http://businessboosters.com.my" target="_blank">Rewards Solution Sdn Bhd</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>