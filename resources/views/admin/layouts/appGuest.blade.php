<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - E-Warranty</title>

        {{-- Include core + vendor Styles --}}
        @include('admin.layouts.panels/styles')


    </head>
  <body>
      @include('admin.layouts.panels/headerGuest')
      <div class="content content-fixed content-auth">
          <div class="container">
              <div class="media align-items-stretch justify-content-center ht-100p">
                  <div class="sign-wrapper mg-lg-r-50 mg-xl-r-60">
                      <div class="pd-t-20 wd-100p">
                          @yield('content')
                      </div>
                  </div><!-- sign-wrapper -->
              </div><!-- media -->
          </div><!-- container -->
      </div>

      <div class="container">
        

        @include('admin.layouts.panels/footer')

      </div><!-- container -->

    @include('admin.layouts.panels/scripts')
  </body>
</html>
