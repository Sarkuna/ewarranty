<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - E-Warranty</title>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link href="{{ asset('themes/dashforge/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('themes/dashforge/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('themes/dashforge/lib/typicons.font/typicons.css') }}" rel="stylesheet">
        <link href="{{ asset('themes/dashforge/lib/toastr/extensions/toastr.css') }}" rel="stylesheet" type="text/css">
        <!-- END: Vendor CSS-->

        {{-- Page Vendor --}}
        @yield('vendor-style')

        <!-- BEGIN: Theme CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.css') }}">
        <link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.demo.css') }}">
        <link rel="stylesheet" href="{{ asset('themes/dashforge/lib/toastr/toastr.css') }}">
        <link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/customadmin.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.css">
        
        
        <!-- END: Theme CSS-->

        {{-- Page Styles --}}
        @yield('page-style')


    </head>
  <body>

    @include('admin.layouts.panels/header')
    @php
     $usr = Auth::guard('admin')->user();
 @endphp
 
    
    
    @include('admin.layouts.panels/sidebar')

    <div class="content content-components">
      <div class="container">
        @include('admin.layouts.panels/breadcrumb')

        @yield('content')

        @include('admin.layouts.panels/footer')

      </div><!-- container -->
    </div><!-- content -->

    {{-- Admin Vendor Scripts --}}
<script src="{{ asset('themes/dashforge/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

@yield('vendor-script')
<script type="text/javascript" src="{{ asset('themes/dashforge/lib/toastr/extensions/toastr.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/fileinput.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/locales/kr.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/themes/fas/theme.js"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/typeahead.js/bootstrap3-typeahead.min.js') }}"></script>

{{-- Theme Scripts --}}
<script src="{{ asset('themes/dashforge/assets/js/dashforge.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/toastr/toastr.js') }}"></script>

{{-- page script --}}
 @include('panels/messages')
@yield('page-script')
{{-- page script --}}
  </body>
</html>
