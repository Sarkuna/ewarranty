<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - E-Warranty</title>

        {{-- Include core + vendor Styles --}}
        @include('admin.layouts.panels/styles')


    </head>
  <body>

    @include('admin.layouts.panels/header')
    @php
     $usr = Auth::guard('admin')->user();

 @endphp
 
    
    
    @include('admin.layouts.panels/sidebar')

    <div class="content content-components">
      <div class="container">
        @include('admin.layouts.panels/breadcrumb')

        @yield('content')

        @include('admin.layouts.panels/footer')

      </div><!-- container -->
    </div><!-- content -->

    @include('admin.layouts.panels/scripts')
  </body>
</html>
