<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>@yield('title', 'Dulux Warranty')</title>     

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    
    <link rel="apple-touch-icon" href="{{ asset('themes/frest-admin-v1/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('themes/frest-admin-v1/app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/themes/semi-dark-layout.css') }}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/plugins/forms/validation/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/app-assets/css/pages/authentication.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/frest-admin-v1/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
  </head>
  
  
  <body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
      <!-- BEGIN: Content-->
      <div class="app-content content">
          <div class="content-overlay"></div>
          <div class="content-wrapper">
              <div class="content-header row">
              </div>
              <div class="content-body">
                  <!-- login page start -->
                  @yield('content')
                  <!-- login page ends -->

              </div>
          </div>
      </div>
      <!-- END: Content--> 
  <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('themes/frest-admin-v1/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('themes/frest-admin-v1/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/scripts/configs/vertical-menu-dark.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/scripts/footer.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('themes/frest-admin-v1/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
    <!-- END: Page JS-->
</body>
<!-- END: Body-->

</html>