<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>{{$warranty_code}}</title>
        <style>
            body{width: 100%;}
            @font-face {font-family: "Colour-Sans-Regular";font-weight: normal;src: url("http://ewarranty.rewardssolution.com/themes/frest-admin-v1/app-assets/fonts/myfont/Colour-Sans-Regular.otf") format('truetype');}
           .fix{position:fixed;bottom:170px;}
           .img_header{top:-40px;}
           #certbackground{background-image: url('http://ewarranty.rewardssolution.com/themes/frest-admin-v1/app-assets/images/pdf/pdf_body_800.png');background-position: top left;background-repeat: no-repeat;background-size: 100%;width:100%;height:100%;top:10px;}
           .bodybg{position:absolute;left:145;top:140px;}
           .h3{font-family: "Colour-Sans-Regular" !important;}
        </style>
</head>

<body>
    <img class="img_header" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_header.png') }}" alt="branding logo" width="100%">
    <div class="bodybg">
        <p style="line-height:32px;">{{ $warranty_code }}</p>
        <p style="line-height:29px;">{{ $product_name }}</p>
        <p style="line-height:26px;">{{ $date_of_purchase }}</p>
        <p style="line-height:28px;">{{ $apply_date }}</p>
        <p style="line-height:28px;">{{ $store_name }}</p>
        <p style="line-height:24px;">{{ $address }} {{ $post_code }} {{$city}} {{$state}}.</p>
        <p style="line-height:30px;">{{ $name }}</p>
        <p style="line-height:20px;">{{ $customer_address }}</p>
        <p style="line-height:28px;">{{ $customer_post_code }} {{$customer_city}} {{$customer_state}}.</p>
        <p style="line-height:33px;">{{ $mobile }}</p>
        <p style="line-height:33px;">{{ $email }}</p>
        <p style="line-height:28px;">{{ $date_of_purchase }}</p>
        <p style="line-height:33px;">{{ $expiry_date }}</p>
        <p style="line-height:38px;">N/A</p>
        <p style="line-height:28px;">N/A</p>
    </div>
    <img class="img_header" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_body_1.png') }}" alt="branding logo" width="100%">
    <img class="fix" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_footer.png') }}" alt="branding logo" width="100%">    
</body>
</html>