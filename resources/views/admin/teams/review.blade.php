@extends('admin.layouts.app')

@section('title')
Reviews Warranties - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <style>
        .text-bold {font-weight: 600;}
    </style>
@endsection

@section('content')

@php
     $usr = Auth::guard('admin')->user();
 @endphp
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">Warranties Reviews</h4></div>
  <div class="col-sm-4 text-right">
      
  </div>
</div>

<div class="df-example demo-table">
    <table id="example1" class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Code</th>
                <th>Customer Name</th>
                <th>Shop Name</th>
                <th>Date of Application</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($models) > 0)
            @foreach ($models as $model)
                @if($model->countAllImagesPending($model->ref) > 0)
                    <?= $read = 'text-bold'; ?>
                @else
                    <?= $read = ''; ?>
                @endif
            
            <tr id="category_id_{{ $model->id }}" class='{{$read}}'>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $model->warranty_code }}</td>
                <td>{{ $model->customer_name }}</td>
                <td>{{ $model->shop_info->company_name }}</td>
                <td>{{ $model->apply_date->format('d-m-Y') }}</td>
                <td class="text-center">
                    <a href="{{route('warranty.show',$model->id)}}" id="view" data-toggle="tooltip" data-placement="bottom" title="View"><i class="far fa-eye"></i></a>
                    @if ($usr->type == 'sales')
                        <a href="{{route('team.edit',$model->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit Sales"><i class="far fa-edit"></i></a>
                    @else
                    <a href="{{route('warranty.edit',$model->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></a>
                    @endif
                    
                </td>
            </tr>
            @endforeach
            @else
            <tr><td class="text-center" colspan="6">No records found!!!</td></tr>
            @endif
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection