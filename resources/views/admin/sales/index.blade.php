@extends('admin.layouts.app')

@section('title')
Sales PIC - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-2">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Central</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('Central') }}</h3>
            </div>

        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-sm-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">East Coast</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('East Coast') }}</h3>                
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Northern</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('Northern') }}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Sabah</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('Sabah') }}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Sarawak</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('Sarawak') }}</h3>
            </div>
        </div>
    </div><!-- col -->
    
    <div class="col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
        <div class="card card-body">
            <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Southern</h6>
            <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">{{ Helper::salesPic('Southern') }}</h3>
            </div>
        </div>
    </div><!-- col -->    

    
</div>

<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">All Sales PIC</h4></div>
  <div class="col-sm-4 text-right">
      <a class="btn btn-primary mr-1 mb-1 btn-sm" href="{{ route('sales.create') }}">Create New</a>
  </div>
</div>

<div class="df-example demo-table">
    <table id="example1" class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Last Login</th>
                <th>Manager</th>
                <th>Region</th>
                <th>Status</th>
                <th class="text-center">View</th>
            </tr>
        </thead>
        <tbody>
            @if(count($models) > 0)
            @foreach ($models as $model)                                
            <tr id="category_id_{{ $model->id }}">
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $model->name }}</td>
                <td>{{ $model->email }}</td>
                <td>{{ $model->mobile }}</td>
                <td>
                    @if(!empty($model->date_last_login))
                    {{ $model->date_last_login->format('d-m-Y') }}
                    @else
                    N/A
                    @endif
                    
                </td>
                @if(!empty($model->manager_id))    
                    <td>
                        <p class="m-0">{{ $model->manager->name }}</p>
                        <p class="m-0">{{ $model->manager->email }}</p>
                    </td>
                    <td>{{ $model->manager->region }}</td>
                @else
                    <td>N/A</td>
                    <td>N/A</td>
                @endif
                <td>{{ ucwords($model->status) }}</td>
                <td class="text-center">
                    <!-- <a href="" id="view" data-toggle="tooltip" data-placement="bottom" title="View"><i class="far fa-eye"></i></a>-->
                    <a href="{{route('sales.edit',$model->id)}}" id="edit" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></a>
                </td>
            </tr>
            @endforeach
            @else
            <tr><td class="text-center" colspan="6">No records found!!!</td></tr>
            @endif
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection