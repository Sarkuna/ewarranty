@extends('admin.layouts.onbehalf')

@section('title')
On Behalf - Admin Panel
@endsection

@section('vendor-style')
<style>
    .title_c {
    font-size: 18px;
    color: #1c273c;
    font-weight: 600;
    margin-bottom: 5px;
}
.surfaceerror,
.topcoaterror{
    display: none;
}
</style>
@endsection

@section('content')
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-12"><h4 class="mg-b-0">New Submission</h4></div>
  
</div>


<div class="row mg-t-20 mg-b-20">
    <div class="col-sm-9">
        <form id="form1" method="post" action="{{route('warranty.store')}}" class="parsley-style-1 steps-basic" enctype="multipart/form-data" >    
            {{csrf_field()}}
        <input type="hidden" value="{{ date('His_Ymd') }}" id="ref" name="ref" readonly="">
        <div id="wizard2">
            <h3 class="title_c">Personal Details</h3>
            
            <section>
                <p class="text-danger">* Please fill up all mandatory fields</p>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Email <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="email" type="email" class="form-control" id="email" placeholder="Your Email" required>
                        @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="name" type="text" class="form-control disremove" id="name" placeholder="Your Name" required>
                        @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Mobile Number <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="mobile" type="text" class="form-control disremove" id="mobile" placeholder="Your Mobile" required>
                        @if ($errors->has('mobile'))
                        <span class="text-danger">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>
                </div>

                <h3 class="title_c">Shop Details</h3>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">State <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select id="shop_state" name="shop_state" class="custom-select" required>
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                            
                        </select>
                        @if ($errors->has('shop_state'))
                        <span class="text-danger">{{ $errors->first('shop_state') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Shop Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="shop_name" type="text" class="form-control" id="shop_name" placeholder="Shop Name" required="" autocomplete="off">
                        @if ($errors->has('shop_name'))
                        <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Receipt #<span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="invoice_num" type="text" class="form-control" id="invoice_num" placeholder="Enter Receipt#" required="">
                        @if ($errors->has('invoice_num'))
                        <span class="text-danger">{{ $errors->first('invoice_num') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date_of_purchase" class="col-sm-4 col-form-label">Date of Purchase <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="date_of_purchase" type="text" class="form-control" id="datepicker1" placeholder="Select Date" required="" readonly="">
                        @if ($errors->has('date_of_purchase'))
                        <span class="text-danger">{{ $errors->first('date_of_purchase') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="apply_date" class="col-sm-4 col-form-label">Paint Applied Date <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="apply_date" type="text" class="form-control" id="datepicker2" placeholder="Select Date" required="" readonly="">
                        @if ($errors->has('apply_date'))
                        <span class="text-danger">{{ $errors->first('apply_date') }}</span>
                        @endif
                    </div>
                </div>
                <h3 class="title_c">Work site Address</h3>
                <div class="form-group row">
                    <label for="address1" class="col-sm-4 col-form-label">Address <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="address1" type="text" class="form-control" id="address1" placeholder="Your Address" required="">
                        @if ($errors->has('address1'))
                        <span class="text-danger">{{ $errors->first('address1') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="state" class="col-sm-4 col-form-label">State <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="state" id="state" class="custom-select" required>
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option data-id="{{$state->id}}" value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state'))
                        <span class="text-danger">{{ $errors->first('state') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="city" class="col-sm-4 col-form-label">City <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="city" id="wcity" class="custom-select" required>
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('city'))
                        <span class="text-danger">{{ $errors->first('city') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="postcode" class="col-sm-4 col-form-label">Postcode <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="postcode" id="wpostcode" class="custom-select" required>
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('postcode'))
                        <span class="text-danger">{{ $errors->first('postcode') }}</span>
                        @endif
                    </div>
                </div>
                
                
                <h3 class="title_c">Painter Details</h3>
                <div class="form-group row">
                    <label for="aname" class="col-sm-4 col-form-label">Painter's Name </label>
                    <div class="col-sm-8">
                        <input name="a_name" type="text" class="form-control" id="a_name" maxlength="40">
                        @if ($errors->has('a_name'))
                        <span class="text-danger">{{ $errors->first('a_name') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="aaddress1" class="col-sm-4 col-form-label">Address</label>
                    <div class="col-sm-8">
                        <input name="a_address" type="text" class="form-control" id="a_address" placeholder="Address">
                        @if ($errors->has('a_address'))
                        <span class="text-danger">{{ $errors->first('a_address') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="astate" class="col-sm-4 col-form-label">State</label>
                    <div class="col-sm-8">
                        <select name="a_state" id="astate" class="custom-select">
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option data-id="{{$state->id}}" value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('a_state'))
                        <span class="text-danger">{{ $errors->first('a_state') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="acity" class="col-sm-4 col-form-label">City</label>
                    <div class="col-sm-8">
                        <select name="a_city" id="a_city" class="custom-select">
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('a_city'))
                        <span class="text-danger">{{ $errors->first('a_city') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="apostcode" class="col-sm-4 col-form-label">Postcode</label>
                    <div class="col-sm-8">
                        <select name="a_postcode" id="a_postcode" class="custom-select">
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('a_postcode'))
                        <span class="text-danger">{{ $errors->first('a_postcode') }}</span>
                        @endif
                    </div>
                </div>
                
            </section>
            
            <h3 class="title_c">Receipts</h3>
            <section>
                <small><span class="tx-danger">*Mandatory Fields</span> (select 1 or both)</small>
                <h5 class="mg-t-20">Surface Preparation </h5>
                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface[]" value="Dulux Weathershield Primer Adhesion Promoting (A579-15222)" type="checkbox" class="custom-control-input surface" id="customCheck1" data-parsley-required data-parsley-mincheck="1" required>
                    <label class="custom-control-label" for="customCheck1"><span class="tx-danger">*</span>Dulux Weathershield Primer Adhesion Promoting (A579-15222)</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface[]" value="Dulux Weathershield Sealer Alkali Resisting (A931-18177)" type="checkbox" class="custom-control-input surface" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2"><span class="tx-danger">*</span>Dulux Weathershield Sealer Alkali Resisting (A931-18177) </label>
                    <p class="mb-0 surfaceerror parsley-errors-list filled">Please select minimum 1 *mandatory field</p>	
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface_opt[]" value="Dulux Fungicidal Wash (A980-19260)" type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label surface_opt" for="customCheck3"> Dulux Fungicidal Wash (A980-19260)</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface_opt[]" value="Water Jetting" type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label surface_opt" for="customCheck4"> Water Jetting</label>
                </div>
                
                <h5 class="mg-t-20">Topcoat Purchased<span class="tx-danger">*</span></h5>
                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="topcoat[]" value="Dulux Weathershield Powerflexx" type="checkbox" class="custom-control-input topcoat" id="topcoatCheck1" data-parsley-required data-parsley-mincheck="1" required>
                    <label class="custom-control-label" for="topcoatCheck1"><span class="tx-danger">*</span>Dulux Weathershield Powerflexx</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="topcoat[]" value="Dulux Weathershield" type="checkbox" class="custom-control-input topcoat" id="topcoatCheck2">
                    <label class="custom-control-label" for="topcoatCheck2"><span class="tx-danger">*</span>Dulux Weathershield</label>
                    <p class="mb-0 topcoaterror parsley-errors-list filled">Please select minimum 1 *mandatory field</p>	
                </div>

                <h5 class="mg-t-20">Attach Receipts<span class="tx-danger">*</span></h5>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Note!</h4>
                    <p style="margin: 0;">Receipt must clearly show the topcoat and sealer/primer purchased</p>
                </div>
                <small>(Total 5MB upload limit and 10 receipt images max)</small>
                <div class="dropzone dropzoneSetting fileAttachDropzone myfile1">
                    <div class="dropzonePlaceholder dz-message" data-dz-message>
                        <i class="fa fa-upload" style="font-size: 18pt;"></i>
                        <br>
                        <b>Upload files here</b>
                        <br>
                        <p>
                            JPG/PNG/PDF files only
                            <br>
                            (Total 5MB upload limit and 10 receipt images max)
                        </p>
                    </div>
                </div>
            </section>
            <h3>Before Photos</h3>
            <section>
                <p class="text-danger">* Please fill up mandatory field</p>
                <small>(Total 5MB upload limit and 10 photos max)</small>
                <div class="dropzone dropzoneSetting fileAttachDropzone2">
                    <div class="dropzonePlaceholder dz-message" data-dz-message>
                        <i class="fa fa-upload" style="font-size: 18pt;"></i>
                        <br>
                        <b>Attach Before photos here<span class="tx-danger">*</span> </b>
                        <br>
                        <p>
                            JPG/PNG files only
                            <br>
                            (Total 5MB upload limit and 10 photos max)
                        </p>
                    </div>
                </div>
            </section>
            <h3>After Photos</h3>
            <section>
                <p class="text-danger">* Please fill up mandatory field</p>
                <small>(Total 5MB upload limit and 10 photos max)</small>
                <div class="dropzone dropzoneSetting fileAttachDropzone3">
                    <div class="dropzonePlaceholder dz-message" data-dz-message>
                        <i class="fa fa-upload" style="font-size: 18pt;"></i>
                        <br>
                        <b>Attach After photos here<span class="tx-danger">*</span></b>
                        <br>
                        <p>
                            JPG/PNG files only
                            <br>
                            (Total 5MB upload limit and 10 photos max)
                        </p>
                    </div>
                </div>
            </section>
        </div>
        </form>

    </div>

</div>




@endsection

@section('vendor-script')
     <!-- Start datatable js -->

@endsection

@section('page-script')
<script src="{{ asset('themes/dashforge/lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.js"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    });
</script>

<script>    
    $(function(){
        'use strict';


        $('#wizard2').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex) {
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var name = $('#name').parsley();
                var email = $('#email').parsley();
                var mobile = $('#mobile').parsley();
                
                var shopstate = $('#shop_state').parsley();
                var shopname = $('#shop_name').parsley();
                var invoicenum = $('#invoice_num').parsley();
                var datepicker1 = $('#datepicker1').parsley();
                var datepicker2 = $('#datepicker2').parsley();
                
                var address1 = $('#address1').parsley();
                var state = $('#state').parsley();
                var wcity = $('#wcity').parsley();
                var wpostcode = $('#wpostcode').parsley();

                if(name.isValid() && email.isValid() && mobile.isValid() && shopstate.isValid() && shopname.isValid() && invoicenum.isValid() && datepicker1.isValid() && datepicker2.isValid() && address1.isValid() && state.isValid() && wcity.isValid() && wpostcode.isValid()) {
                  return true;
                } else {
                  name.validate();
                  email.validate();
                  mobile.validate();  
                  shopstate.validate();
                  shopname.validate();
                  invoicenum.validate();
                  datepicker1.validate();
                  datepicker2.validate();
                  address1.validate();
                  state.validate();
                  wcity.validate();
                  wpostcode.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {
                    var receiptsdz1 = $('#img_receipts').val();
                    
                    if ($('input[name="surface[]"]:checked').length < 1){
                        $(".surfaceerror").show();
                        return false;
                    }else{                        
                        $(".surfaceerror").hide();                                 
                    }
                    
                    if ($('input[name="topcoat[]"]:checked').length < 1){
                        $(".topcoaterror").show();
                        return false;
                    }else{                        
                        $(".topcoaterror").hide();                                 
                    }
                    
                    if(receiptsdz1 === undefined){                        
                        alert('Attach Receipts is required');
                        return false;
                    }
                    
                    return true;
              }
              
              if(currentIndex === 2) {
                    var attach_photo_before = $('#attach_photo_before').val();
                    if(attach_photo_before === undefined){                        
                          alert('Attach Before Photos is required');
                          return false;
                      }

                     //remove default #finish button
                     return true;
              }
            } else { return true; }
          },
          labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            //alert("Alhamdulillah, Alkhery Member is already Registered.");
            //$("#form1").submit();
            var attach_photo_after = $('#attach_photo_after').val();
            if(attach_photo_after === undefined){                        
                alert('Attach After Photos is required');
                return false;
            }
            
            $("#form1").submit();
        }
        });

        
        $('#datepicker1').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2019/01/01"),
            dateFormat: 'dd-mm-yy',
            startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D",
            onSelect: function(dateText, inst) {
                $('#datepicker2').datepicker('option', 'minDate', dateText);
            }
        });
        
        $('#datepicker2').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2023/05/09"),
            dateFormat: 'dd-mm-yy',
            //minDate: "+0M +0D",
            //startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D"
            
        });
        
        $('#email').on('change', function() {
            $("#name").val('');
            $("#mobile").val('');
            
            $.ajax({
                //url: "queries/checkEmail.php",
                url: "{{route('warranty.check.email')}}",
                data: {
                    'email' : $('#email').val()
                },
                dataType: 'json',
                success: function(data) {
                    if(data == 'no'){
                        $('.disremove').prop('disabled', '');
                    }else {
                        $("#name").val(data.name);
                        $("#mobile").val(data.mobile);
                        //$(".disremove").prop("disabled", true);
                        //$("#mobile").prop("disabled", true);
                    }
                    
                },
                error: function(data){
                    //error
                }
            });
        });

        var path = "{{ route('warranty.sale.shops') }}";
        $('#shop_name').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query, state:$('#shop_state').val() }, function (data) {
                    return process(data);
                });
            }
        });
        
        
        let removeFileFromServer = true;

        Dropzone.autoDiscover = false;
        var fileAttachDropzone = $(".fileAttachDropzone").dropzone({
          url: "{{route('warranty.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "img_receipts");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='img_receipts' name='img_receipts[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('warranty.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "img_receipts"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });

        //---------------------------Attach Photo Before---------------------------------
        var fileAttachDropzone = $(".fileAttachDropzone2").dropzone({
          url: "{{route('warranty.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_before");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_before' name='attach_photo_before[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('warranty.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_before"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        //-------------------------------Attach Photo After----------------------
        var fileAttachDropzone = $(".fileAttachDropzone3").dropzone({
          url: "{{route('warranty.store.media')}}",
          acceptedFiles: "image/png,image/jpg,image/jpeg,image/gif,image/PNG,image/JPG,image/JPEG,.png,.jpg,.jpeg,.gif,.PNG,.JPG,.JPEG",
          maxFilesize: 5,
          maxFiles: 10,
          addRemoveLinks: true,
          dictMaxFilesExceeded: "Total number of images exceeded! Unable to upload",
          sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            formData.append("ref", $('#ref').val());
            formData.append("img_type", "attach_photo_after");
          },
          init: function(){
            this.on("success", function(file, response) {
              file.previewElement.dataset.url = response;
              $('.dz-preview[data-url="'+response+'"]').append("<input type='hidden' id='attach_photo_after' name='attach_photo_after[]' class='file_attach' value='"+response+"'/>");
            });

            this.on("removedfile",function(file){
              var _ref;
              return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            });
          },
          removedfile: function(file) {
            if (removeFileFromServer == true) {
              var fileURL = file.previewElement.dataset.url;
              let deleteUrl = "{{route('warranty.remove.media')}}";

              $.ajaxSetup({
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
              });

              $.ajax({
                type: "POST",
                data: {
                  "url": fileURL,
                  "ref": $('#ref').val(),
                  "img_type": "attach_photo_after"
                },
                url: deleteUrl,
                success: function(response){
                  $(".file_attach").each(function(){
                    if($(this).val() == fileURL){
                      $(this).remove();
                    }
                  });
                }
              });
            }
          }
        });
        
        //------------------------------------
        $("#state").change(function(){
            
            $('#wpostcode').val("");
            $.ajax({
                url: "{{route('warranty.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wcity').html(data.html);
                }
            });
        });
        
        $("#astate").change(function(){
            
            $('#a_postcode').val("");
            $.ajax({
                url: "{{route('warranty.cities.states')}}?state_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_city').html(data.html);
                }
            });
        });
        
        $("#wcity").change(function(){
            $.ajax({
                url: "{{route('warranty.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#wpostcode').html(data.html);
                }
            });
        });
        
        $("#a_city").change(function(){
            $.ajax({
                url: "{{route('warranty.postcode.cities')}}?city_id=" + $(this).children('option:selected').data('id'),
                method: 'GET',
                success: function(data) {
                    $('#a_postcode').html(data.html);
                }
            });
        });
        
    });
</script>
@endsection