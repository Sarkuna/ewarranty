@extends('admin.layouts.app')

@section('title')
View Warranties - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
@endsection

@section('page-style')
    <!-- Start datatable css -->
    <style>
        .df-example{border: none;}
        .bd-b {margin-top: 0;}
        .table th, .table td {padding: 8px 0px;}
        .table th, .table td {border: none;}
    </style>
@endsection

@section('content')
<div class="content content-fixed bd-b">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="d-sm-flex align-items-center justify-content-between">
          <div>
            <h4 class="mg-b-5">#{{ $warranty->warranty_code }}</h4>
            <p class="mg-b-0 tx-color-03">Submitted on {{ $warranty->created_at->format('M d D, Y') }}</p>
          </div>
          <div class="mg-t-20 mg-sm-t-0"></div>
        </div>
      </div><!-- container -->
    </div>

<div class="df-example">
    
    
    <div class="row">
        <div class="col-sm-6">
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03">Customer Info</label>

            <form method="post" action="{{route('warranty.customer.save')}}" class="form" data-parsley-validate>
                {{ csrf_field() }}
                <input type="hidden" value="{{$warranty->id}}" name="wid">
                <input type="hidden" value="{{$warranty->status}}" name="status">

                        <div class="form-group">
                            <label for="customer_name" class="control-label">Customer Name<span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->customer_name }}" name="customer_name" type="text" class="form-control" id="customer_name" placeholder="Customer Name" required="">
                            @if ($errors->has('customer_name'))
                            <span class="text-danger">{{ $errors->first('customer_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="address2" class="control-label">Customer Mobile</label>
                            <input value="{{ $warranty->customer_mobile }}" name="customer_mobile" type="text" class="form-control" id="customer_mobile" placeholder="Contact No">
                        </div>

                <button class="btn btn-block btn-primary">Submit</button>
            </form>
        </div><!-- col -->
        <div class="col-sm-6 tx-right d-none d-md-block">
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03">Status</label>
            <h1 class="tx-normal tx-color-04 mg-b-10 tx-spacing--2">{{ ucwords($warranty->status) }}</h1>
            @if($warranty->status == 'decline')            
                @if(!empty($warranty->status_remark))
                    <p class="text-danger">{{ $warranty->status_remark }}</p>
                @else
                    <p>N/A</p>
                @endif
            @endif    
        </div><!-- col -->
        
        
        <div class="col-sm-6 col-lg-6 mg-t-40">
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03">Shop Information</label>

            <p class="mg-b-0">{{ $warranty->shop_info->company_name }}</p>
            <p class="mg-b-0">{{ $warranty->shop_info->address1 }}</p>
            <p class="mg-b-0">{{ $warranty->shop_info->address2 }}</p>
            <p class="mg-b-0">{{ $warranty->shop_info->post_code }} {{ $warranty->shop_info->city }} {{ $warranty->shop_info->state }}</p>
        
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03 mg-t-40">Work site Address</label>
            <p class="mg-b-0">{{ $warranty->address1 }}</p>
            <p class="mg-b-0">{{ $warranty->address2 }}</p>
            <p class="mg-b-0">{{ $warranty->city }}</p>
            <p class="mg-b-0">{{ $warranty->post_code }} - {{ $warranty->state }}</p>
            
            
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03 mg-t-40">Painter Detail</label>
            <p class="mg-b-0">{{ $warranty->a_name }}</p>
            <p class="mg-b-0">{{ $warranty->a_address }}</p>
            <p class="mg-b-0">{{ $warranty->a_city }}</p>
            <p class="mg-b-0">{{ $warranty->a_postcode }} - {{ $warranty->a_state }}</p>
           
        </div><!-- col -->

        
        <div class="col-sm-6 col-lg-6 mg-t-40 mg-sm-t-0 mg-md-t-40">
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03">Purchase Information</label>
            <ul class="list-unstyled lh-7">
                    <li class="d-flex justify-content-between">
                        <span>Invoice#</span>
                        <span>{{ $warranty->invoice_num }}</span>
                    </li>

                    <li class="d-flex justify-content-between">
                        <span>Date of Purchase</span>
                        <span>{{ $warranty->date_of_purchase->format('d-m-Y') }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Date of Application</span>
                        <span>{{ $warranty->apply_date->format('d-m-Y') }}</span>
                    </li>
                    
                </ul>
            
            <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03 mg-t-10">Surface Preparation</label>
            @if(!empty($surfaces))
                @foreach ($surfaces as $surface)
                <p class="mg-b-0">{{ $surface }}</p>
                @endforeach
                <br>
            @endif
            
            @if(!empty($surfaceopts))
                @foreach ($surfaceopts as $surfaceopt)
                <p class="mg-b-0">{{ $surfaceopt }}</p>
                @endforeach
            @endif
        </div><!-- col -->
        
        
        
    </div>
    
    <div class="row row-sm"> 
            @foreach ($warranty->getAllImages($warranty->ref) as $image)
                @if($loop->index % 4===0)
                    </div><div class="row row-sm mg-t-40">
                @endif
                
 
                <div class="col-3 col-sm-3 col-md-3 col-xl-3 h-100">
                    <div class="card card-file">
                        <div class="card-body">
                            <h6>
                                @if($image->type=='img_receipts')
                                Receipts
                                @elseif($image->type=='attach_photo_before')
                                Photo Before
                                @elseif($image->type=='attach_photo_after')
                                Photo After
                                @elseif($image->type=='all_paint_can')
                                Attach Front of Pack of All Paint can  
                                @endif

                            </h6>
                        </div>
                        <div class="card-file-thumb tx-primary">
                            @if (\File::extension($image->real_filename) == 'pdf')
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                                <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$image->id}}">View</a></h3>
                            @else
                                <a href="#" data-toggle="modal" data-target="#modal{{$image->id}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="wd-100p" alt="Responsive image"></a>
                            @endif

                        </div>
                        
                    </div>
                </div>
                
                <div class="modal fade" id="modal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div class="modal-content tx-14">
                          <div class="modal-header">
                              <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                          </div>
                      </div>
                  </div>
              </div>
            @endforeach
        </div>
</div>

@endsection

@section('vendor-script')
     
@endsection

@section('page-script')

@endsection