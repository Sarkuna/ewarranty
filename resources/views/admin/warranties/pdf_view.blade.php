<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>{{$warranty_code}}</title>
        <style>
            body{width: 100%;}
            @font-face {font-family: "Colour-Sans-Regular";font-weight: normal;src: url("http://ewarranty.rewardssolution.com/themes/frest-admin-v1/app-assets/fonts/myfont/Colour-Sans-Regular.otf") format('truetype');}
           .fix{position:fixed;bottom:170px;}
           .img_header{top:-40px;}
           #certbackground{background-image: url('http://ewarranty.rewardssolution.com/themes/frest-admin-v1/app-assets/images/pdf/pdf_body_800.png');background-position: top left;background-repeat: no-repeat;background-size: 100%;width:100%;height:100%;top:10px;}
           .bodybg{position:absolute;left:145;top:140px;}
           .h3{font-family: "Colour-Sans-Regular" !important;}
        </style>
</head>

<body>
    <img class="img_header" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_header.png') }}" alt="branding logo" width="100%">
    <div class="bodybg">
        <p style="line-height:29px;position: absolute;top: 5px;">{{ $warranty_code }}</p>
        <p style="line-height:29px;position: absolute;top: 47px;">{{ $product_name }}</p>
        <p style="line-height:29px;position: absolute;top: 90px;">{{ $date_of_purchase }}</p>
        <p style="line-height:29px;position: absolute;top: 132px;">{{ $store_name }}</p>
        <p style="line-height:39px;position: absolute;top: 170px;">{{ substr($address,0,100) }} {{ $post_code }} {{$city}} {{$state}}.</p>
        <p style="line-height:29px;position: absolute;top: 263px;">{{ $name }}</p>
        <p style="line-height:39px;position: absolute;top: 300px;">{{ $customer_address }} {{ $customer_post_code }} {{$customer_city}} {{$customer_state}}.</p>
        <p style="line-height:29px;position: absolute;top: 393px;">{{ $mobile }}</p>
        <p style="line-height:29px;position: absolute;top: 436px;">{{ $email }}</p>
        <p style="line-height:29px;position: absolute;top: 481px;">{{ $date_of_purchase }}</p>
        <p style="line-height:29px;position: absolute;top: 530px;">{{ $expiry_date }}</p>
        <p style="line-height:29px;position: absolute;top: 587px;">{{ $painter_name }}</p>
        <p style="line-height:39px;position: absolute;top: 626px;">{{ $painter_address }}</p>
    </div>
    <img class="img_header" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_body_1.png') }}" alt="branding logo" width="100%">
    <img class="fix" src="{{ asset('themes/frest-admin-v1/app-assets/images/pdf/pdf_footer.png') }}" alt="branding logo" width="100%">    
</body>
</html>