@extends('admin.layouts.app')

@section('title')
Edit Warranties - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
@endsection

@section('page-style')
    <!-- Start datatable css -->
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
    <style>
        .df-example{border: none;}
        .bd-b {margin-top: 0;}
        .status_remark{display: none;}
    </style>
    
@endsection

@section('content')
<div class="content content-fixed bd-b">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="d-sm-flex align-items-center justify-content-between">
          <div>
            <h4 class="mg-b-5">#{{ $warranty->warranty_code }}</h4>
            <h6 id="section3" class="mg-b-10 tx-uppercase">Submitted on {{ $warranty->created_at->format('M d D, Y') }}</h6>
          </div>
          <div class="mg-t-20 mg-sm-t-0"></div>
        </div>
      </div><!-- container -->
    </div>

<div class="df-example">
    <form method="post" action="{{route('warranty.update',$warranty->id)}}" class="form" data-parsley-validate>
        {{ method_field('PUT') }}
        {{ csrf_field() }} 
        <div class="row">
            <div class="col-sm-6">
                <h6 id="section3" class="mg-b-10 tx-uppercase">Customer Info</h6>
                <p class="mg-b-0">{{ $warranty->customer_name }}</p>
                <p class="mg-b-0">{{ $warranty->customer_info->email }}</p>
                <p class="mg-b-0">{{ $warranty->customer_mobile }}</p>
            </div><!-- col -->
            <div class="col-sm-6 tx-right d-none d-md-block">
                <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Status</label>
                <h1 class="mg-b-10 tx-spacing--2">
                    
                    <span class="badge-lg text-bg-primary">{{ ucwords($warranty->status) }}</span>
                </h1>
            </div><!-- col -->

            <div class="col-sm-12 col-lg-6 mg-t-40">
                <h6 id="section3" class="mg-b-10 tx-uppercase">Shop Information</h6>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="form-group">
                            <label for="shop_state" class="control-label">State<span class="tx-danger">*</span></label>
                            <select name="shop_state" id="shop_state" class="custom-select" required>
                                <option value="">Select</option>
                                @foreach($states as $key=>$state)
                                    <option data-id="{{$state->id}}" {{ ($warranty->shop_info->state) == $state->state ? 'selected' : '' }} value='{{$state->state}}'>{{$state->state}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('state'))
                            <span class="text-danger">{{ $errors->first('state') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="shop_name">Shop Name <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->shop_info->company_name }}" name="shop_name" type="text" class="form-control" id="shop_name" placeholder="Shop Name" required="" autocomplete="off">
                            @if ($errors->has('shop_name'))
                            <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-lg-7">
                        <div class="form-group">
                            <label for="invoice_num">Invoice# <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->invoice_num }}" name="invoice_num" type="text" class="form-control" id="invoice_num" placeholder="Enter Invoice#" required="">
                            @if ($errors->has('invoice_num'))
                            <span class="text-danger">{{ $errors->first('invoice_num') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="date_of_purchase">Date of Purchase <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->date_of_purchase->format('d-m-Y') }}" name="date_of_purchase" type="text" class="form-control" id="date_of_purchase" placeholder="Select Date" required="" readonly="">
                            @if ($errors->has('date_of_purchase'))
                            <span class="text-danger">{{ $errors->first('date_of_purchase') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="apply_date">Paint Applied Date <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->apply_date->format('d-m-Y') }}" name="apply_date" type="text" class="form-control" id="apply_date" placeholder="Select Date" required="" readonly="">
                            @if ($errors->has('apply_date'))
                            <span class="text-danger">{{ $errors->first('apply_date') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

            </div><!-- col -->

            <div class="col-sm-12 col-lg-6 mg-t-40 mg-sm-t-0 mg-md-t-40">
                <h6 id="section3" class="mg-b-10 tx-uppercase">Work site Address</h6>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="address1" class="control-label">Address 1 <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->address1 }}" name="address1" type="text" class="form-control" id="lname" placeholder="No 3 Bandar Bhar Nilai" required="">
                            @if ($errors->has('address1'))
                            <span class="text-danger">{{ $errors->first('address1') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="address2" class="control-label">Address 2</label>
                            <input value="{{ $warranty->address2 }}" name="address2" type="text" class="form-control" id="lname" placeholder="Bandar Bukit Nilai">
                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="col-lg-12">
                        <div class="form-group">
                            <label for="state" class="control-label">State <span class="tx-danger">*</span></label>
                            <select value="{{ $warranty->state }}" name="state" class="custom-select" required>
                                <option value="">Select</option>
                                @foreach($states as $key=>$state)
                                    <option data-id="{{$state->id}}" {{ ($warranty->state) == $state->state ? 'selected' : '' }} value='{{$state->state}}'>{{$state->state}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('state'))
                            <span class="text-danger">{{ $errors->first('state') }}</span>
                            @endif
                        </div>
                    </div>	
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="city" class="control-label">City <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->city }}" name="city" type="text" class="form-control" id="lname" placeholder="Seremban" required="">
                            @if ($errors->has('state'))
                            <span class="text-danger">{{ $errors->first('city') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="postcode" class="control-label">Postcode <span class="tx-danger">*</span></label>
                            <input value="{{ $warranty->post_code }}" name="postcode" type="text" class="form-control" id="lname" placeholder="5000" required="">
                            @if ($errors->has('postcode'))
                            <span class="text-danger">{{ $errors->first('postcode') }}</span>
                            @endif
                        </div>
                    </div>
                   			
                </div>
            </div><!-- col -->
            
            <div class="col-sm-12 col-lg-6 mg-t-40 mg-sm-t-0 mg-md-t-40">
                <h6 id="section3" class="mg-b-10 tx-uppercase">Surface Preparation</h6>
                @if(!empty($surfaces))
                    @foreach ($surfaces as $surface)
                    <p class="mg-b-0">{{ $surface }}</p>
                    @endforeach
                    <br>
                @endif
                
                @if(!empty($surfaceopts))
                    @foreach ($surfaceopts as $surfaceopt)
                    <p class="mg-b-0">{{ $surfaceopt }}</p>
                    @endforeach
                @endif
                
                <h6 id="section3" class="mg-b-10 tx-uppercase">Topcoat Purchased</h6>
                @if(!empty($topcoats))
                    @foreach ($topcoats as $topcoat)
                    <p class="mg-b-0">{{ $topcoat }}</p>
                    @endforeach
                @endif
                
            </div>

        </div>

        @if(count($warranty->getAllImagesPending($warranty->ref)) > 0)
        <div class="table-responsive mg-t-40">
            <table class="table table-invoice bd-b">
                <thead>
                    <tr>
                        <th class="wd-30p">Upload</th>                        
                        <th class="wd-20p">Type</th>
                        <th>Status</th>
                        <th class="wd-30p d-none d-sm-table-cell">Comment</th>
                    </tr>
                </thead>
                <tbody>                    
                    @foreach ($warranty->getAllImagesPending($warranty->ref) as $key => $image)
                    
                    <tr>
                        <td class="d-none d-sm-table-cell">
                            <div class="file-loading">
                                <input id="input-{{ $key }}" name="uploadFile" type="file" accept="image/*">
                            </div>

                        </td>
                        
                        <td class="d-none d-sm-table-cell">
                            @if($image->type=='img_receipts')
                            Receipts
                            @elseif($image->type=='attach_photo_before')
                            Photo Before
                            @elseif($image->type=='attach_photo_after')
                            Photo After
                            @elseif($image->type=='all_paint_can')
                            Attach Front of Pack of All Paint can  
                            @endif
                        </td>
                        <td>
                            <select id="{{ $key }}" name="image_status[{{ $key }}]" class="custom-select image_status">
                                <option value="">Select</option>
                                <option {{ ($image->status) == 'pending' ? 'selected' : '' }} value="pending">Pending</option>
                                <option {{ ($image->status) == 're_submit' ? 'selected' : '' }} value="re_submit">Re Submit</option>
                                <option {{ ($image->status) == 'approve' ? 'selected' : '' }} value="approve">Approve</option>

                            </select>
                            @if($errors->has('image_status.'.$key))
                            <span class="text-danger">{{ $errors->first('image_status.'.$key) }}</span>
                        @endif
                        </td>
                        <td class="d-none d-sm-table-cell tx-color-03" id="mytd-{{ $key }}">
                            <input value="{{ $image->id }}" name="image_id[{{ $key }}]" type="hidden">
                            <textarea id="summary-{{ $key }}" rows="3" cols="80" class="form-control summary-{{ $key }}" name="image_summary[{{ $key }}]" placeholder="Summary">{{ $image->summary }}</textarea>

                            @if($errors->has('image_summary.'.$key))
                            <span class="text-danger">{{ $errors->first('image_summary.'.$key) }}</span>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        @endif
        
        @if(count($warranty->getAllImagesResubmit($warranty->ref)) > 0)
        <div class="table-responsive mg-t-40">
            <table class="table table-invoice bd-b">
                <thead>
                    <tr>
                        <th class="wd-10p">Image</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($warranty->getAllImagesResubmit($warranty->ref) as $keyr => $imageR)
                    <tr>
                        <td class="tx-nowrap">
                            @if (\File::extension($imageR->real_filename) == 'pdf')
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$imageR->real_filename)}}" frameborder="0" width="100%" height="auto">
                                <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modalr{{$imageR->id}}">View</a></h3>
                            @else
                                <a href="#" data-toggle="modal" data-target="#modalr{{$keyr}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$imageR->real_filename)}}" class="wd-100p" alt="Responsive image"></a>
                            @endif
                            
                        </td>
                        
                        <td class="d-none d-sm-table-cell">
                            @if($imageR->type=='img_receipts')
                            Receipts
                            @elseif($imageR->type=='attach_photo_before')
                            Photo Before
                            @elseif($imageR->type=='attach_photo_after')
                            Photo After
                            @elseif($imageR->type=='all_paint_can')
                            Attach Front of Pack of All Paint can  
                            @endif
                        </td>
                        <td>
                            {{ $imageR->status }}
                        </td>
                        <td>
                            {{ $imageR->summary }}
                        </td>
                    </tr>
                    
                    <div class="modal fade" id="modalr{{$keyr}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content tx-14">
                                <div class="modal-header">
                                    <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$imageR->real_filename)}}" frameborder="0" width="100%">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                    
                </tbody>
            </table>
        </div>
        
        @endif
        
        @if(count($warranty->getAllImagesApproved($warranty->ref)) > 0)
        <div class="table-responsive mg-t-40">
            <table class="table table-invoice bd-b">
                <thead>
                    <tr>
                        <th class="wd-10p">Image</th>
                        <th>Type</th>
                        <th>Status</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($warranty->getAllImagesApproved($warranty->ref) as $key => $image)
                    <tr>
                        <td class="tx-nowrap">
                            @if (\File::extension($image->real_filename) == 'pdf')
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                                <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$image->id}}">View</a></h3>
                            @else
                                <a href="#" data-toggle="modal" data-target="#modal{{$key}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="wd-100p" alt="Responsive image"></a>
                            @endif
                            
                        </td>
                        
                        <td class="d-none d-sm-table-cell">
                            @if($image->type=='img_receipts')
                            Receipts
                            @elseif($image->type=='attach_photo_before')
                            Photo Before
                            @elseif($image->type=='attach_photo_after')
                            Photo After
                            @elseif($image->type=='all_paint_can')
                            Attach Front of Pack of All Paint can  
                            @endif
                        </td>
                        <td>
                            {{ $image->status }}
                        </td>
                    </tr>
                    
                    <div class="modal fade" id="modal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content tx-14">
                                <div class="modal-header">
                                    <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                    
                </tbody>
            </table>
        </div>
        
        @endif

        <div class="row justify-content-between">
            <div class="col-sm-6 col-lg-6 order-2 order-sm-0 mg-t-40 mg-sm-t-0">

            </div><!-- col -->
            <div class="col-sm-6 col-lg-4 order-1 order-sm-0">                
                <div class="form-group">
                    <label for="products" class="control-label" style="display:block;">Products</label>
                    <div id="cbWrapper" class="parsley-checkbox">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="product_year" value="7" id="b1" {{ ($warranty->product_year) == '7' ? 'checked' : '' }}>
                        <label class="custom-control-label" for="b1">Dulux Weathershield</label>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="product_year_power" value="12" id="b2" {{ ($warranty->product_year_power) == '12' ? 'checked' : '' }}>
                        <label class="custom-control-label" for="b2">Dulux Weathershield Powerflexx</label>
                    </div>
                </div>
                <div id="cbErrorContainer"></div>
                </div>
                <div class="form-group">
                    <label for="status" class="control-label">Status</label>
                    <select id="act_status" name="status" class="custom-select" required>
                        <option value="">Select Status</option>
                        <option {{ ($warranty->status) == 'review' ? 'selected' : '' }} value="review">Review</option>                        
                        <option {{ ($warranty->status) == 'approved' ? 'selected' : '' }} value="approved">Approved</option>
                        <option {{ ($warranty->status) == 'decline' ? 'selected' : '' }} value="decline">Decline</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="text-danger">{{ $errors->first('status') }}</span>
                    @endif
                </div>
                
                <div class="form-group status_remark">
                    <label for="status_remark" class="control-label">Status Remark</label>
                    <input value="{{ $warranty->status_remark }}" name="status_remark" type="text" class="form-control" id="status_remark" placeholder="Remark" >
                    @if ($errors->has('status_remark'))
                        <span class="text-danger">{{ $errors->first('status_remark') }}</span>
                    @endif
                </div>

                <button class="btn btn-block btn-primary">Submit</button>
            </div><!-- col -->
        </div>
    </form>        
</div>

@endsection

@section('vendor-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.0/js/fileinput.min.js" type="text/javascript"></script>
<script src="{{ asset('themes/dashforge/assets/theme.js') }}" type="text/javascript"></script>
 <script src="{{ asset('themes/dashforge/lib/prismjs/prism.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/parsleyjs/parsley.min.js') }}"></script>
 <script src="{{ asset('themes/dashforge/lib/jqueryui/jquery-ui.min.js') }}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
 @endsection
 
 @section('page-script')
 <script type="text/javascript">    
    $(document).ready(function() {
        var $ref = $('#ref').val();
        @foreach ($warranty->getAllImagesPending($warranty->ref) as $key => $image)
        //ATTACH RECEIPTS
        var $el{{ $key }} = $("#input-{{ $key }}");       
        $el{{ $key }}.fileinput({
            theme: 'fas',
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'pdf'],
            uploadUrl: "{{route('warranty.image.re.upload')}}",
            uploadExtraData: function() {
                return {
                    _key: "{{ $key }}",
                    _id: "{{ $image->id }}",
                    _token: "{{ csrf_token() }}",
                    _ref: $ref, 
                    _type: "{{ $image->type }}",
                };
            },
            uploadAsync: true,
            deleteExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                    _key: "{{ $key }}",
                };
            },
            showRemove: false,
            showUpload: false, // hide upload button
            showCancel: false,
            overwriteInitial: false, // append files to initial preview
            //minFileCount: 2,
            maxFileCount: 1,
            maxFileSize: 2000,
            browseOnZoneClick: true,
            initialPreview:[
                "{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}",
                //"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg",
                //"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg"
            ],
            initialPreviewAsData: true,
        }).on("filebatchselected", function(event, files) {
            $el{{ $key }}.fileinput("upload"); 
        }).on("filedeleted", function(event, key, jqXHR, data) {
            $("#bb{{ $key }} .input-group.file-caption-main").show();
        }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
            //window.location.reload();
            //location.reload();
            window.location.href = '/admin/warranties/warranty/{{ $warranty->id }}/edit'; //one level up
        });
        @endforeach
    });
 </script>
 <script type="text/javascript">     
    $('.image_status').on('change', function() {
         if($(this).val() == 're_submit'){
             $('#summary-'+$(this).attr('id')).show();
             $('#summary-'+$(this).attr('id')).prop('required',true);             
         }else{
             $('#mytd-'+$(this).attr('id')).find("ul").remove(); 
             $('#summary-'+$(this).attr('id')).hide();
             $('#summary-'+$(this).attr('id')).prop('required',false);
         }
    });
    
    $('#act_status').change(function () {
         if($(this).val() == 'decline'){
             $('.status_remark').show();
             $('#status_remark').prop('required',true);
             $('.image_status').prop('required',false);
             $('#b1').prop('required',false);
             $('#b1').find("data-parsley-mincheck").remove();
             $('#cbWrapper').find("ul").remove();
             $('#cbErrorContainer').removeClass('parsley-error');
         }else{
             $('.image_status').prop('required',true);
             $('.status_remark').hide();
             $('#status_remark').prop('required',false);
             $('#cbErrorContainer').remove();
         }
         
         
         /*if($(this).val() == 'approved'){
             $('#b1').prop('required',true);
         }else{
             $('#b1').prop('required',false);
             $('#cbWrapper').find("ul").remove();
             $('#cbErrorContainer').remove();
         }*/
         
         //product_year_power
    });
    $(function(){
        //'use strict' 
        $('#date_of_purchase').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2019/01/01"),
            dateFormat: 'dd-mm-yy',
            startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D"
            
        });
        
        $('#apply_date').datepicker({
            todayHighlight: false,
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            //defaultDate: new Date("2019/01/01"),
            dateFormat: 'dd-mm-yy',
            startDate: '-15d',
            //endDate: '+0d',
            maxDate: "+0M +0D"
            
        });
    });
 
    
    var path = "{{ route('warranty.autocomplete') }}";
    
    $('#shop_name').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query, state:$('#shop_state').val() }, function (data) {
                return process(data);
            });
        }
    });
</script>
 @endsection