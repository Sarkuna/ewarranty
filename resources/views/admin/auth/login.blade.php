@extends('admin.layouts.authLayout')

@section('title')
Login - Admin Panel
@endsection

@section('content')


<section id="auth-login" class="row flexbox-container">
    <div class="col-xl-8 col-11">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-login -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="text-center mb-2">Welcome Back</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                @include('admin.layouts.partials.messages')
                                <form method="POST" action="{{ route('admin.login.submit') }}" novalidate>
                                    {{ csrf_field() }}
                                    <div class="form-group mb-50">
                                        <div class="controls">
                                            <label class="text-bold-600" for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="exampleInputEmail1" placeholder="Email address" required data-validation-required-message="This field is required">
                                        </div>
                                    </div>
                                    <div class="form-group controls">
                                        <div class="controls">
                                            <label class="text-bold-600" for="exampleInputPassword1">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required data-validation-required-message="This field is required">
                                        </div>
                                    </div>
                                    <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                        <div class="text-left">
                                            <div class="checkbox checkbox-sm">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                <label class="checkboxsmall" for="exampleCheck1"><small>Keep me logged
                                                        in</small></label>
                                            </div>
                                        </div>
                                        <div class="text-right"><a href="{{ route('admin.forgot_password') }}" class="card-link"><small>Forgot Password?</small></a></div>
                                    </div>
                                    <button type="submit" class="btn btn-primary glow w-100 position-relative">Login<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <div class="card-content">
                        <img class="img-fluid" src="{{ asset('themes/dashforge/assets/img/Powerflexx_WEB BANNER 640x670.jpg') }}" alt="branding logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






@endsection
