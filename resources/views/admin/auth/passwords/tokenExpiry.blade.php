@extends('admin.layouts.authLayout')

@section('title')
Token Expired - Admin Panel
@endsection

@section('content')


<section class="row flexbox-container">
    <div class="col-xl-7 col-10">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-login -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right d-flex justify-content-center mb-0 p-2 h-100">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="text-center mb-2">The link has expired</h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-center">
                                <p>Hmm. Looks like the link to reset your password has expired. But don't worry, we can send you a new one.</p>
                                <p><a href="{{ route('admin.forgot_password') }}" class="btn btn-primary card-link">Reset my password now</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <img class="img-fluid" src="{{ asset('themes/frest-admin-v1/app-assets/images/pages/reset-password.png') }}" alt="branding logo">
                </div>
            </div>
        </div>
    </div>
</section>


@endsection