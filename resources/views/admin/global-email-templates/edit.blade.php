@extends('admin.layouts.app')

@section('title')
Email Template - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->    
@endsection

@section('content')
<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-12"><h4 class="mg-b-0">Edit - {{$emailtemplate->name}}</h4></div>
  
</div>


<div class="row mg-t-20 mg-b-20">
    <div class="col-sm-9">

        <form method="post" action="{{route('global-email-templates.update',$emailtemplate->id)}}" class="form" novalidate>
            {{ method_field('PUT') }}
            {{ csrf_field() }} 
            <div class="form-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="code">Code</label>
                            <input type="text" value="{{$emailtemplate->code}}" id="code" class="form-control" placeholder="Template Code" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="name" class="required">Name</label>
                            <input type="text" value="{{$emailtemplate->name}}" id="name" class="form-control" placeholder="Template Name" name="name" required>
                            @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="subject" class="required">Subject</label>
                            <input type="text" value="{{$emailtemplate->subject}}" id="subject" class="form-control" placeholder="Email Subject" name="subject" required>
                            @if ($errors->has('subject'))
                            <span class="text-danger">{{ $errors->first('subject') }}</span>
                            @endif
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label for="projectinput8" class="required">Body</label>
                    <textarea id="classic-editor" rows="10" cols="80" class="form-control" name="template" placeholder="Email Body">{{$emailtemplate->template}}</textarea>
                    @if ($errors->has('code'))
                            <span class="text-danger">{{ $errors->first('code') }}</span>
                            @endif
                </div>

                <div class="form-group">
                    <label for="status" class="required">Status</label>
                    <select name="status" class="form-control">
                        <option selected>Pick Status</option>
                        <option value="yes" <?= $emailtemplate->status === 'yes' ? 'selected' : '' ?>>Yes</option>
                        <option value="no" <?= $emailtemplate->status === 'no' ? 'selected' : '' ?>>No</option>
                    </select>
                    @if ($errors->has('status'))
                            <span class="text-danger">{{ $errors->first('status') }}</span>
                            @endif
                </div>
            </div>

            <div class="form-actions">
                <a href="{{route('global-email-templates.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>

    </div>
    <div class="col-sm-3">
        <?php
        //$path = storage_path() . "/json/shortcode.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $path = storage_path('app/public/json/shortcode.json');
        $shortcodes = json_decode(file_get_contents($path), true);
        //dd($shortcodes['shortcodes']);
        foreach ($shortcodes['shortcodes'] as $shortcode) {
            echo $shortcode;
        }
        ?>
    </div>
</div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
    <script src="{{ asset('themes/modern-admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('page-script')

  <!-- Page js files -->
  @include('panels/messages')
  <script src="{{ asset('themes/modern-admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('template', {
        //fullPage : true,
        allowedContent : true,
        filebrowserBrowseUrl : '/admin/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/admin/elfinder/ckeditor',
        //uiColor : '#9AB8F3',
        height : 600
    });
</script>
@endsection