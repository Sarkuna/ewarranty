@extends('admin.layouts.app')

@section('title')
Email Template - Admin Panel
@endsection

@section('vendor-style')
    <!-- Start datatable css -->
    
    <link href="{{ asset('themes/dashforge/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/dashforge/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')


<div class="row mg-t-20 mg-b-20">
  <div class="col-sm-8"><h4 class="mg-b-0">Email Template</h4></div>
  <div class="col-sm-4 text-right">
      <a class="btn btn-primary mr-1 mb-1 btn-sm" href="{{ route('global-email-templates.create') }}">Create New</a>
  </div>
</div>

<div class="df-example demo-table">
    <table id="example1" class="table">
        <thead>
            <tr>
                <th width="5%">#</th>
                <th width="10%">Code</th>
                <th width="60%">Name</th>
                <th width="15%">Status</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($emailtemplates as $emailtemplate)
            <tr id="category_id_{{ $emailtemplate->id }}">
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $emailtemplate->code }}</td>
                <td>{{ $emailtemplate->name }}</td>
                <td>
                    @if($emailtemplate->status=='yes')
                    <span class="badge badge-success">{{ucwords($emailtemplate->status)}}</span>
                    @else
                    <span class="badge badge-danger">{{ucwords($emailtemplate->status)}}</span>
                    @endif
                </td>
                <td>
                    <a href="{{route('global-email-templates.edit',$emailtemplate->id)}}" id="edit-email-templates" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="bottom" title="Edit"><i data-feather="edit"></i></a>
                    <a href="{{route('global-email-templates.delete',[$emailtemplate->id])}}" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="return confirm('Are you sure to delete this item?')"><i data-feather="trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>

@endsection

@section('vendor-script')
     <!-- Start datatable js -->
    <script src="{{ asset('themes/dashforge/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/dashforge/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page-script')
    <script>
      $(function(){
        'use strict'

        $('#example1').DataTable({
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
      });
    </script>
@endsection