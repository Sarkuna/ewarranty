<style>
    .title_c{
        font-size: 18px;
    color: #1c273c;
    font-weight: 600;
    margin-bottom: 5px;
    }
</style>
<div class="tx-13 mg-b-25 mg-t-15">
    {!! NoCaptcha::renderJs() !!}

    @if ($errors->has('g-recaptcha-response'))
    <span class="help-block">
        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
    </span>
    @endif

    <form id="form1" method="post" action="{{route('user.new.warranty.store')}}" class="parsley-style-1 steps-basic" enctype="multipart/form-data" >
        {{csrf_field()}}
        <input type="hidden" value="{{ date('His_Ymd') }}" id="ref" name="ref" readonly="">
        <div id="wizard2">
            <h3>Personal Details</h3>
            
            <section>
                <p class="text-danger">* Please fill up all mandatory fields</p>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="name" type="text" class="form-control" id="name" placeholder="Your Name" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Mobile Number <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Your Mobile" required>
                        @if ($errors->has('mobile'))
                        <span class="text-danger">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>
                </div>
                
                <h3 class="title_c">Shop Details</h3>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">State <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select id="shop_state" name="shop_state" class="custom-select" required>
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('shop_state'))
                        <span class="text-danger">{{ $errors->first('shop_state') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Shop Name <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="shop_name" type="text" class="form-control" id="shop_name" placeholder="Shop Name" required="" autocomplete="off">
                        @if ($errors->has('shop_name'))
                        <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Receipt #<span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="invoice_num" type="text" class="form-control" id="invoice_num" placeholder="Enter Receipt#" required="">
                        @if ($errors->has('invoice_num'))
                        <span class="text-danger">{{ $errors->first('invoice_num') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date_of_purchase" class="col-sm-4 col-form-label">Date of Purchase <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="date_of_purchase" type="text" class="form-control" id="datepicker1" placeholder="Select Date" required="" readonly="">
                        @if ($errors->has('date_of_purchase'))
                        <span class="text-danger">{{ $errors->first('date_of_purchase') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="apply_date" class="col-sm-4 col-form-label">Paint Applied Date <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="apply_date" type="text" class="form-control" id="datepicker2" placeholder="Select Date" required="" readonly="">
                        @if ($errors->has('apply_date'))
                        <span class="text-danger">{{ $errors->first('apply_date') }}</span>
                        @endif
                    </div>
                </div>
                <h3 class="title_c">Work site Address</h3>
                <div class="form-group row">
                    <label for="address1" class="col-sm-4 col-form-label">Address <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="address1" type="text" class="form-control" id="address1" placeholder="Your Address" required="">
                        @if ($errors->has('address1'))
                        <span class="text-danger">{{ $errors->first('address1') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="state" class="col-sm-4 col-form-label">State <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="state" id="state" class="custom-select" required>
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option data-id="{{$state->id}}" value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state'))
                        <span class="text-danger">{{ $errors->first('state') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="city" class="col-sm-4 col-form-label">City <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="city" id="wcity" class="custom-select" required>
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('city'))
                        <span class="text-danger">{{ $errors->first('city') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="postcode" class="col-sm-4 col-form-label">Postcode <span class="tx-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="postcode" id="wpostcode" class="custom-select" required>
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('postcode'))
                        <span class="text-danger">{{ $errors->first('postcode') }}</span>
                        @endif
                    </div>
                </div>
                
                <h3 class="title_c">Painter Details</h3>
                <div class="form-group row">
                    <label for="aname" class="col-sm-4 col-form-label">Painter's Name </label>
                    <div class="col-sm-8">
                        <input name="a_name" type="text" class="form-control" id="a_name" maxlength="40">
                        @if ($errors->has('a_name'))
                        <span class="text-danger">{{ $errors->first('a_name') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="aaddress1" class="col-sm-4 col-form-label">Address</label>
                    <div class="col-sm-8">
                        <input name="a_address" type="text" class="form-control" id="a_address" placeholder="Address" >
                        @if ($errors->has('a_address'))
                        <span class="text-danger">{{ $errors->first('a_address') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="astate" class="col-sm-4 col-form-label">State</label>
                    <div class="col-sm-8">
                        <select name="a_state" id="astate" class="custom-select">
                            <option value="">Select</option>
                            @foreach($states as $key=>$state)
                                <option data-id="{{$state->id}}" value='{{$state->state}}'>{{$state->state}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('a_state'))
                        <span class="text-danger">{{ $errors->first('a_state') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="acity" class="col-sm-4 col-form-label">City</label>
                    <div class="col-sm-8">
                        <select name="a_city" id="a_city" class="custom-select">
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('a_city'))
                        <span class="text-danger">{{ $errors->first('a_city') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="apostcode" class="col-sm-4 col-form-label">Postcode</label>
                    <div class="col-sm-8">
                        <select name="a_postcode" id="a_postcode" class="custom-select">
                            <option value="">Select</option>
                        </select>
                        @if ($errors->has('a_postcode'))
                        <span class="text-danger">{{ $errors->first('a_postcode') }}</span>
                        @endif
                    </div>
                </div>
            </section>
            <h3>Receipts</h3>
            <section>
                <small><span class="tx-danger">*Mandatory Fields</span> (select 1 or both)</small>
                <h5 class="mg-t-20">Surface Preparation </h5>
                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface[]" value="Dulux Weathershield Primer Adhesion Promoting (A579-15222)" type="checkbox" class="custom-control-input surface" id="customCheck1" data-parsley-required data-parsley-mincheck="1" required>
                    <label class="custom-control-label" for="customCheck1"><span class="tx-danger">*</span>Dulux Weathershield Primer Adhesion Promoting (A579-15222)</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface[]" value="Dulux Weathershield Sealer Alkali Resisting (A931-18177)" type="checkbox" class="custom-control-input surface" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2"><span class="tx-danger">*</span>Dulux Weathershield Sealer Alkali Resisting (A931-18177) </label>
                    <p class="mb-0 surfaceerror parsley-errors-list filled">Please select minimum 1 *mandatory field</p>	
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface_opt[]" value="Dulux Fungicidal Wash (A980-19260)" type="checkbox" class="custom-control-input" id="customCheck3">
                    <label class="custom-control-label surface_opt" for="customCheck3"> Dulux Fungicidal Wash (A980-19260)</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="surface_opt[]" value="Water Jetting" type="checkbox" class="custom-control-input" id="customCheck4">
                    <label class="custom-control-label surface_opt" for="customCheck4"> Water Jetting</label>
                </div>
                
                <h5 class="mg-t-20">Topcoat Purchased<span class="tx-danger">*</span></h5>
                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="topcoat[]" value="Dulux Weathershield Powerflexx" type="checkbox" class="custom-control-input topcoat" id="topcoatCheck1" data-parsley-required data-parsley-mincheck="1" required>
                    <label class="custom-control-label" for="topcoatCheck1"><span class="tx-danger">*</span>Dulux Weathershield Powerflexx</label>
                </div>

                <div class="col-sm-12 custom-control custom-checkbox">
                    <input name="topcoat[]" value="Dulux Weathershield" type="checkbox" class="custom-control-input topcoat" id="topcoatCheck2">
                    <label class="custom-control-label" for="topcoatCheck2"><span class="tx-danger">*</span>Dulux Weathershield</label>
                    <p class="mb-0 topcoaterror parsley-errors-list filled">Please select minimum 1 *mandatory field</p>	
                </div>
                
                <h5 class="mg-t-20">Attach Receipts<span class="tx-danger">*</span></h5>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Note!</h4>
                    <p style="margin: 0;">Receipt must clearly show the topcoat and sealer/primer purchased</p>
                </div>
                <small>(Total 5MB upload limit and 10 receipt images max)</small>
                <div class="dropzone dropzoneSetting fileAttachDropzone myfile1">
                    <div class="dropzonePlaceholder dz-message" data-dz-message>
                        <i class="fa fa-upload" style="font-size: 18pt;"></i>
                        <br>
                        <b>Upload files here</b>
                        <br>
                        <p>
                            JPG/PNG/PDF files only
                            <br>
                            (Total 5MB upload limit and 10 receipt images max)
                        </p>
                    </div>
                </div>
            </section>
            <h3>Before Photos</h3>
            <section>
                <p class="text-danger">* Please fill up mandatory field</p>
                <small>(Total 5MB upload limit and 10 photos max)</small>
                <div class="dropzone dropzoneSetting fileAttachDropzone2">
                    <div class="dropzonePlaceholder dz-message" data-dz-message>
                        <i class="fa fa-upload" style="font-size: 18pt;"></i>
                        <br>
                        <b>Attach Before photos here<span class="tx-danger">*</span> </b>
                        <br>
                        <p>
                            JPG/PNG files only
                            <br>
                            (Total 5MB upload limit and 10 photos max)
                        </p>
                    </div>
                </div>
            </section>
            <h3>After Photos</h3>
            <section>
                <section>
                    <p class="text-danger">* Please fill up mandatory field</p>
                    <small>(Total 5MB upload limit and 10 photos max)</small>
                    <div class="dropzone dropzoneSetting fileAttachDropzone3">
                        <div class="dropzonePlaceholder dz-message" data-dz-message>
                            <i class="fa fa-upload" style="font-size: 18pt;"></i>
                            <br>
                            <b>Attach After photos here<span class="tx-danger">*</span></b>
                            <br>
                            <p>
                                JPG/PNG files only
                                <br>
                                (Total 5MB upload limit and 10 photos max)
                            </p>
                        </div>
                    </div>
                </section>

                <div class="form-group mg-t-20 col-md-12 align-self-start">
                    
                    <div id="cbWrapper" class="parsley-checkbox">
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="terms" value="1" data-parsley-mincheck="1" data-parsley-class-handler="#cbWrapper" data-parsley-errors-container="#cbErrorContainer" required="" id="b1">
                          <label class="custom-control-label" for="b1"><a href="#modal4" data-toggle="modal">Agree with Terms & Conditions</a></label>
                          @if ($errors->has('terms'))
                        <span class="text-danger">{{ $errors->first('terms') }}</span>
                        @endif
                        </div>
                    </div>

                </div>
                {!! NoCaptcha::display() !!}

            </section>
        </div>
    </form>
</div>




