@extends('layouts/app')

@section('title', 'Home')

@section('vendor-style')
  <!-- vendor css files -->

@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.dashboard.css') }}">


@endsection

@section('content')


<div class="content tx-13">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
            <div class="d-sm-flex align-items-center justify-content-between">
                <div>
                    <h4 class="mg-b-5">{{ $warranty->warranty_code }}</h4>
                    <p class="mg-b-0 tx-color-03">Submitted on {{ $warranty->created_at->format('M d D, Y') }}</p>
                </div>
                <div class="mg-t-20 mg-sm-t-0">
                    <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Status</label>
                    <h1 class="tx-color-03 mg-b-10 tx-spacing--2">{{ ucwords($warranty->status) }}</h1>
                </div>
            </div>

        
        <div class="row">

            <div class="col-sm-6 col-lg-6 mg-t-40 mg-sm-t-0 mg-md-t-40">
                <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Work site Address</label>

                <p class="mg-b-0">{{ $warranty->address1 }}</p>
                <p class="mg-b-0">{{ $warranty->address2 }}</p>
                <p class="mg-b-0">{{ $warranty->city }}</p>
                <p class="mg-b-0">{{ $warranty->post_code }} - {{ $warranty->state }}</p>
                
                
                <label class="tx-sans tx-uppercase tx-12 tx-medium tx-spacing-1 tx-color-03 mg-t-40">Painter Detail</label>
            <p class="mg-b-0">{{ $warranty->a_name }}</p>
            <p class="mg-b-0">{{ $warranty->a_address }}</p>
            <p class="mg-b-0">{{ $warranty->a_city }}</p>
            <p class="mg-b-0">{{ $warranty->a_postcode }} - {{ $warranty->a_state }}</p>
            <br>
            </div><!-- col -->
            <div class="col-sm-6 col-lg-6 mg-t-40">
                <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Shop Information</label>
                <ul class="list-unstyled lh-7">
                    <li class="d-flex justify-content-between">
                        <span>State</span>
                        <span>{{ $warranty->shop_info->state }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Shop Name</span>
                        <span>{{ $warranty->shop_info->company_name }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Receipt #</span>
                        <span>{{ $warranty->invoice_num }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Date of Purchase</span>
                        <span>{{ $warranty->date_of_purchase->format('M d D, Y') }}</span>
                    </li>
                    <li class="d-flex justify-content-between">
                        <span>Date of Application</span>
                        <span>{{ $warranty->apply_date->format('M d D, Y') }}</span>
                    </li>
                    
                    @if(!empty($warranty->expiry_date)) 
                    <li class="d-flex justify-content-between">
                        <span>Expiry Date</span>
                        <span>{{ $warranty->expiry_date->format('d-m-Y') }}</span>
                    </li>
                    @endif
                </ul>
            </div><!-- col -->



        </div><!-- row -->

        
        
        
        
        
            <div class="row row-sm">
            <h6 class="col-12">Attach Receipts</h6>    
            @foreach ($warranty->getImgReceipts($warranty->ref) as $image)
                <div class="col-3 col-sm-3 col-md-3 col-xl h-100">
                @if (\File::extension($image->real_filename) == 'pdf')
                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                    <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$image->id}}">View</a></h3>
                @else
                    <a href="#" data-toggle="modal" data-target="#modal{{$image->id}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="img-thumbnail" alt="Responsive image"></a>
                @endif
                </div>
            
                <div class="modal fade" id="modal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content tx-14">
                            <div class="modal-header">
                                <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="row row-sm">
            <h6 class="col-12">Photo Before</h6>
            @foreach ($warranty->getPhotoBefore($warranty->ref) as $image)
                <div class="col-3 col-sm-3 col-md-3 col-xl h-100">
                @if (\File::extension($image->real_filename) == 'pdf')
                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                    <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$image->id}}">View</a></h3>
                @else
                    <a href="#" data-toggle="modal" data-target="#modal{{$image->id}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="img-thumbnail" alt="Responsive image"></a>
                @endif
                </div>
            
                <div class="modal fade" id="modal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content tx-14">
                            <div class="modal-header">
                                <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="row row-sm">
            <h6 class="col-12">Photo After</h6> 
            @foreach ($warranty->getPhotoAfter($warranty->ref) as $image)
                <div class="col-3 col-sm-3 col-md-3 col-xl h-100">
                @if (\File::extension($image->real_filename) == 'pdf')
                    <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%" height="auto">
                    <h3 class="text-center"><a href="#" data-toggle="modal" data-target="#modal{{$image->id}}">View</a></h3>
                @else
                    <a href="#" data-toggle="modal" data-target="#modal{{$image->id}}"><img src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" class="img-thumbnail" alt="Responsive image"></a>
                @endif
                </div>
            
                <div class="modal fade" id="modal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content tx-14">
                            <div class="modal-header">
                                <h6 class="modal-title" id="exampleModalLabel4">View</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <embed src="{{ asset('/storage/purchase_detail/'.$warranty->ref.'/'.$image->real_filename)}}" frameborder="0" width="100%">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div><!-- container -->
</div><!-- content -->

@endsection