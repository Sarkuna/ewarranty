@if (Auth::check())
  <footer class="footer">
    <div>
        <span>&copy; 2019 Akzo Nobal v1.0.0. </span>
        <span>Made by <a href="">Business Boosters</a></span>
    </div>
    <div>
        <nav class="nav">
            <a class="nav-link" href="https://www.dulux.com.my/en/about-us" target="_blank">About Dulux</a>
            <a class="nav-link" href="https://www.dulux.com.my/en/find-a-retailer" target="_blank">Find a Dulux store</a>
            <a class="nav-link" href="https://www.dulux.com.my/en/colour-palettes#tabId=item0" target="_blank">Find a colour</a>
            <a class="nav-link" href="https://www.dulux.com.my/en/legal" target="_blank">Legal / Privacy policy / Cookies</a>
        </nav>
    </div>
</footer><!-- content-footer -->
@else
  <footer class="footer text-center">
      
    <div class="container">
        <ul class="list-inline mb-5">
            <li class="list-inline-item">
                <a href="https://www.dulux.com.my/en/about-us" target="_blank">About Dulux</a>
            </li>
            <li class="list-inline-item">
                <a href="https://www.dulux.com.my/en/find-a-retailer" target="_blank">Find a Dulux store</a>
            </li>
            <li class="list-inline-item">
                <a href="https://www.dulux.com.my/en/colour-palettes#tabId=item0" target="_blank">Find a colour</a>
            </li>
            <li class="list-inline-item">
                <a href="https://www.dulux.com.my/en/legal" target="_blank">Legal / Privacy policy / Cookies</a>
            </li>
            
            <li class="list-inline-item" style="vertical-align: middle;">
                <a href="https://www.facebook.com/DuluxMalaysia" target="_blank"><i class="icon ion-logo-facebook fa-3x" size="large"></i></a>
            </li>
            <li class="list-inline-item" style="vertical-align: middle;">
                <a href="https://www.youtube.com/user/DuluxMalaysia" target="_blank"><i class="icon ion-logo-youtube fa-3x" size="large"></i></a>
            </li>
            <li class="list-inline-item" style="vertical-align: middle;">
                <a href="http://www.instagram.com/duluxmalaysia" target="_blank"><i class="icon ion-logo-instagram fa-3x" size="large"></i></a>
            </li>

        </ul>
        <p class="text-muted small mb-0">Copyright &copy; AkzoNobel Paints Sdn. Bhd.
            <?php echo date('Y') ?></p>
    </div>
</footer>
@endif


