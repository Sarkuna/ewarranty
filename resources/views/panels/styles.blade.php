<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link href="{{ asset('themes/dashforge/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('themes/dashforge/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('themes/dashforge/lib/typicons.font/typicons.css') }}" rel="stylesheet">
<!-- END: Vendor CSS-->

{{-- Page Vendor --}}
@yield('vendor-style')

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/dashforge.css') }}">
<link rel="stylesheet" href="{{ asset('themes/dashforge/assets/css/custom.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
<!-- END: Theme CSS-->

{{-- Page Styles --}}
@yield('page-style')