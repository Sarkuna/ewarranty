@if ($message = Session::get('success'))
<script>
    toastr['success']('{{ $message }}', 'Success!', {
      closeButton: true,
      tapToDismiss: true,
    });
</script>
@endif


@if ($message = Session::get('error'))
<script>
      toastr['error']('{{ $message }}', 'Error!', {
      closeButton: true,
      tapToDismiss: true,
    });
    </script>
@endif


@if ($message = Session::get('warning'))
<script>
    toastr['warning']('{{ $message }}', 'Warning!', {
      closeButton: true,
      tapToDismiss: false,
    });
</script>
@endif


@if ($message = Session::get('info'))
<script>
    toastr['info']('{{ $message }}', 'Info!', {
      closeButton: true,
      tapToDismiss: true,
    });
</script>
@endif