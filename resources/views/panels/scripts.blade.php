{{-- Vendor Scripts --}}
<script src="{{ asset('themes/dashforge/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('themes/dashforge/lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('themes/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>



@yield('vendor-script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>

{{-- Theme Scripts --}}
<script type="text/javascript">toastr.options = {"closeButton":true,"closeClass":"toast-close-button","closeDuration":300,"closeEasing":"swing","closeHtml":"<button><i class=\"icon-off\"><\/i><\/button>","closeMethod":"fadeOut","closeOnHover":true,"containerId":"toast-container","debug":false,"escapeHtml":false,"extendedTimeOut":10000,"hideDuration":1000,"hideEasing":"linear","hideMethod":"fadeOut","iconClass":"toast-info","iconClasses":{"error":"toast-error","info":"toast-info","success":"toast-success","warning":"toast-warning"},"messageClass":"toast-message","newestOnTop":false,"onHidden":null,"onShown":null,"positionClass":"toast-top-right","preventDuplicates":true,"progressBar":true,"progressClass":"toast-progress","rtl":false,"showDuration":300,"showEasing":"swing","showMethod":"fadeIn","tapToDismiss":true,"target":"body","timeOut":5000,"titleClass":"toast-title","toastClass":"toast"};</script>
<script src="{{ asset('themes/dashforge/assets/js/dashforge.js') }}"></script>

    

{{-- page script --}}
 @include('panels/messages')
 
 
@yield('page-script')
{{-- page script --}}
