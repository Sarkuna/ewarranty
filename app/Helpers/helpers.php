<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;


use Config;
use Illuminate\Support\Str;
use Mail;
use YoHang88\LetterAvatar\LetterAvatar;
use Auth;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\States;
use App\Models\Cities;
use App\Models\Postcodes;
use App\Models\Shops;
use App\Models\AssignUserShops;
class Helper
 {

    public static function shout(string $string) {
        return strtoupper($string);
    }

    public static function basic_email($bcc = null) {
        $data = array('name' => "Virat Gandhi");
        $html = "test email";
        $tomail = 'shihanagni@gmail.com';
        $subject = 'hi';
        //$emailBody =

        Mail::send(array(), array(), function ($message) use ($bcc) {
        //Mail::send('mail.mail-welcome', $data, function($message) use($data,$bcc) {
            $message->from('no-reply@rewardssolution.com');
            $message->to('shihanagni@gmail.com');
            $message->bcc($bcc);
            $message->subject('New email!!!');
            $message->setBody('test', 'text/html');
        });
        if (Mail::failures()) {
            return "False Basic Email Sent. Check your inbox.";
        } else {
            return "True Basic Email Sent. Check your inbox.";
        }
    }

    public static function sendEmailAdmin($userId, $emailTemplateCode, $data = null, $cc = null, $bcc = null) {
        $emailTemplate = \App\Models\GlobalEmailTemplate::where('code', '=', $emailTemplateCode)->first();
        $emailBody = $emailTemplate->parse($data);
        $subject = $emailTemplate->parsesubject($subject);
        Mail::send(array(), array(), function ($message) use ($tomail, $cc, $bcc, $subject, $emailBody) {
            $message->to($tomail)->from(env('APP_NO_REPLAY'),'support');
            if(!empty($cc)) {
                $message->bcc($cc);
            }
            
            if(!empty($bcc)) {
                $message->bcc($bcc);
            }
            $message->subject($subject);
            $message->setBody($emailBody, 'text/html');
        });

        if (Mail::failures()) {
            return "False Basic Email Sent. Check your inbox.";
        } else {
            return "True Basic Email Sent. Check your inbox.";
        }
    }
    
    public static function sendEmailAdmin2($tomail, $subject = null, $emailTemplateCode, $data = null, $cc = null, $bcc = null)
    {
         $emailTemplate = \App\Models\GlobalEmailTemplate::where('code', '=', $emailTemplateCode)->first();
         $emailBody = $emailTemplate->parse($data);
         
         $subject = $emailTemplate->parsesubject($subject);
         Mail::send(array(), array(), function ($message) use ($tomail, $cc, $bcc, $subject,$emailBody) {
            
            $message->to($tomail)->from(env('APP_NO_REPLAY'),'support');
            if(!empty($cc)) {
                $message->bcc($cc);
            }
            
            if(!empty($bcc)) {
                $message->bcc($bcc);
            }
            $message->subject($subject);
            $message->setBody($emailBody, 'text/html');
         });

         if (Mail::failures()) {
             return false;
         }else  {
             return true;
         } 
    }
    
    public static function sendEmailAdminwithAttachment($tomail, $subject = null, $emailTemplateCode, $data = null, $cc = null, $bcc = null, $pdf = null, $pdf2 = null)
    {
         $emailTemplate = \App\Models\GlobalEmailTemplate::where('code', '=', $emailTemplateCode)->first();
         $emailBody = $emailTemplate->parse($data);
         $subject = $emailTemplate->parsesubject($subject);
         Mail::send(array(), array(), function ($message) use ($tomail, $cc, $bcc, $subject,$emailBody,$pdf,$pdf2) {                         
            $message->to($tomail)->from(env('APP_NO_REPLAY'),'support')->subject($subject);
            if(!empty($cc)) {
                $message->bcc($cc);
            }
            
            if(!empty($bcc)) {
                $message->bcc($bcc);
            }
            
            if(!empty($pdf)){
                $message->attachData($pdf->output(), date('d-m-Y').".pdf");
            }
            if(!empty($pdf2)){
                $message->attachData($pdf2->output(), date('d-m-Y').".pdf");
            }
            $message->setBody($emailBody, 'text/html');
         });

         if (Mail::failures()) {
             return false;
         }else  {
             return true;
         } 
    }


    public static function my_avatar() {
        if (Auth::check()) {
            $name = Auth::guard('web')->user()->name;
            $avatar = new LetterAvatar($name);
            return $avatar;
        }
    }
    
    public static function admin_avatar() {
        if (Auth::check()) {
            $name = Auth::guard('admin')->user()->name;
            $avatar = new LetterAvatar($name);
            return $avatar;
        }
    }

    public static function my_prefix() {
        return request()->segment(1);
    }
    
    public static function getToken($length) {
        $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
    
    public static function salesPic($string) {
        $countmanage = Admin::where('type', '=', 'manager')->where('region', '=', $string)->count();
        if($countmanage > 0) {
            $manageid = Admin::select('id')->where('type', '=', 'manager')->where('region', '=', $string)->get()->pluck('id');
            $totalmanages = Admin::where('type', '=', 'sales')->whereIn('manager_id',$manageid)->count();
        }else {
            $totalmanages = 0;
        }
        return $totalmanages;
    }
    
    
    public static function myStates($state_id)
    {
        if (!$state_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $cities = Cities::orderby("city_name","asc")->where('state_id', $state_id)->get();
            foreach ($cities as $city) {
                $html .= '<option data-id="'.$city->id.'" value="'.$city->city_name.'">'.$city->city_name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
    
    public static function myCities($city_id)
    {
        if (!$city_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $postcodes = Postcodes::orderby("postcode","asc")->where('city_id', $city_id)->get();
            foreach ($postcodes as $pcode) {
                $html .= '<option value="'.$pcode->postcode.'">'.$pcode->postcode.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
    
    public static function myShops($query,$state)
    {
        $datas = Shops::select("company_name")
                ->where('company_name','LIKE','%'.$query.'%')
                ->where('state','=',$state)
                ->where('status','=','active')
                ->get();
        
        $dataModified = array();
        foreach ($datas as $data)
        {
          $dataModified[] = $data->company_name;
        }

        return response()->json($dataModified);
    }
    
    public static function shopsRegion($region)
    {
        $datas = Shops::where('region','=',$region)->where('status','=','active')->count();

        return $datas;
    }
    
    public static function assign_shops($type,$saleID)
    {
        if($type == 'manager') {
            $type = 'manager_id';
        }else {
            $type = 'sales_id';
        }
        $collection = AssignUserShops::where($type, $saleID)->get();
        $array = $collection->pluck('shop_id');
        return $array;
    }
}