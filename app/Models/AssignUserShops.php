<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignUserShops extends Model
{
    protected $fillable = [
        'shop_id',
        'manager_id',
        'sales_id',
    ];
    
    public function sale(){
        return $this->hasOne('App\Models\Admin','id','sales_id');
    }
    
    
}
