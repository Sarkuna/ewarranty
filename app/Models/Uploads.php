<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uploads extends Model
{
    protected $fillable = [
        'ref', 'file_name', 'real_filename', 'type'
    ];
}
