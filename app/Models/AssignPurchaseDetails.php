<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignPurchaseDetails extends Model
{
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();           
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });       
    }
    
    protected $fillable = [
        'purchase_details_id',
        'admins_id',
        'visitor',
    ];
}
