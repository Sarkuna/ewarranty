<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignManagerShops extends Model
{
    protected $fillable = [
        'shop_id',
        'manager_id'
    ];
    
    public function manager(){
        return $this->hasOne('App\Models\Admin','id','manager_id');
    }
}
