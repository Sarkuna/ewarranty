<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Wuwx\LaravelAutoNumber\AutoNumberTrait;
use App\Models\Uploads;

class PurchaseDetails extends Model
{
    use AutoNumberTrait;
    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            /*$user = Auth::user();           
            $model->created_by = $user->id;
            $model->updated_by = $user->id;*/
            $model->warranty_code = $model->warranty_code;
        });       
    }
    protected $dates = ['date_of_purchase', 'expiry_date', 'apply_date', 'expiry_date_power'];
    
    
    protected $fillable = [
        'user_id',
        'customer_name',
        'customer_mobile',
        'shop_id',
        'ref',
        'date_of_purchase',
        'apply_date',
        'address1',
        'address2',
        'city',
        'post_code',
        'state',
        'status_remark',
        'visitor',
        'status',
        'agree',
        'product_year',
        'product_year_power',
        'expiry_date',
        'expiry_date_power',
        'invoice_num',
        'surface',
        'surface_opt',
        'topcoat',
        'a_name',
        'a_address',
        'a_state',
        'a_city',
        'a_postcode',
        'sales_id',
        'manager_id',
        'created_by'
    ];
    
        
    public function getAutoNumberOptions()
    {
        return [
            'warranty_code' => [
                'format' => function () {
                    return date('Y') . '/?'; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 6, // The number of digits in the autonumber
            ],
        ];
    }
    
    public function shop_info(){
        return $this->hasOne('App\Models\Shops','id','shop_id');
    }
    
    public function manager(){
        return $this->hasOne('App\Models\Admin','id','manager_id');
    }
    
    public function sales(){
        return $this->hasOne('App\Models\Admin','id','sales_id');
    }
    
    public function assign_manager(){
        return $this->hasOne('App\Models\AssignManagerShops','shop_id','shop_id');
    }
    
    public function assign_sale(){
        return $this->hasOne('App\Models\AssignUserShops','shop_id','shop_id');
    }

    public function customer_info(){
        return $this->hasOne('App\User','id','user_id');
    }
    
    public static function getAllPost($ref,$type){
        return Uploads::where('ref',$ref)->where('type',$type)->orderBy('id','DESC')->latest()->paginate(2);
    }
    
    public static function getAllImages($ref){
        return Uploads::where('ref',$ref)->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getImgReceipts($ref){
        return Uploads::where(['ref' => $ref, 'type' => 'img_receipts'])->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    
    public static function getPhotoBefore($ref){
        return Uploads::where(['ref' => $ref, 'type' => 'attach_photo_before'])->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getPhotoAfter($ref){
        return Uploads::where(['ref' => $ref, 'type' => 'attach_photo_after'])->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getAllImagesApproved($ref){
        return Uploads::where('ref',$ref)->where('status','approve')->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getAllImagesResubmit($ref){
        return Uploads::where('ref',$ref)->where('status','re_submit')->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getAllImagesPending($ref){
        return Uploads::where('ref',$ref)->where('status','=','pending')->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function getAllImagesNotApprove($ref){
        return Uploads::where('ref',$ref)->where('status','!=','approve')->orderBy('type','DESC')->latest()->paginate(30);
    }
    
    public static function countAllImagesPending($ref){
        return Uploads::where('ref',$ref)->where('status','=','pending')->count();
    }
    
    public static function countAllImagesResubmit($ref){
        return Uploads::where('ref',$ref)->where('status','=','re_submit')->count();
    }

    public static function product_name($id) {
        //$product_name = 'N/A'.$id;
        if($id == 12) {
            $product_name = 'Dulux Weathershield Powerflexx';
        }else if($id == 7) {
            $product_name = 'Dulux Weathershield';
        }else {
            $product_name = 'N/A';
        }
        
        return $product_name;
    }
}
