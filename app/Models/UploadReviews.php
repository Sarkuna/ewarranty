<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UploadReviews extends Model
{
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth()->guard('admin')->user();
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });       
    }
    
    protected $fillable = [
        'uploads_id', 'summary'
    ];
}
