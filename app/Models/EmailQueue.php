<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailQueue extends Model
{
    public $timestamps = false;
    protected $dates = ['date_to_send', 'created_date'];
    
    protected $casts = [
        'data' => 'array',
        'bcc' => 'array'
    ];
    
    protected $fillable = [
        'code',
        'user_id',
        'purchase_id',
        'name',
        'email',
        'cc',
        'bcc',
        'data',
        'status',
        'date_to_send',
        'created_date'
    ];
    
    public function purchase_info(){
        return $this->hasOne('App\Models\PurchaseDetails','id','purchase_id');
    }
}
