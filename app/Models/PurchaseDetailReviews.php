<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetailReviews extends Model
{
    protected $fillable=['user_id','purchase_id','rate','review','status'];
}
