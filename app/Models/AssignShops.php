<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignShops extends Model
{
    protected $fillable = [
        'shop_id',
        'manager_id',
        'sales_id',
    ];
}
