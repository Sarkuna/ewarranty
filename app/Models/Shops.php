<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

Use App\Models\AssignUserShops;

class Shops extends Model
{
    protected $fillable = [
        'company_name',
        'contact_num',
        'address1',
        'address2',
        'city',
        'post_code',
        'state',
        'summary',
        'status',
        'token'
    ];
    
    public function assign_manager(){
        return $this->hasOne('App\Models\AssignManagerShops','shop_id','id');
    }
    
    public function assign_sale_shop(){
        return $this->hasOne('App\Models\AssignUserShops','shop_id','id');
    }
    
}
