<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use \App\Models\EmailQueue;
use DB;
use Carbon\Carbon;
use Helper;

class ReminderEmailReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:emailreviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (EmailQueue::where([['status','P'],['created_date',Carbon::today()]])->limit(100)->get() as $emailQueue) {
            $cc = '';
            $subject = ['code' => 'Reminder '.$emailQueue->purchase_info->warranty_code];
            Helper::sendEmailAdmin2($emailQueue->email, $subject, $emailQueue->code, $emailQueue->data, $cc, $emailQueue->bcc);

            EmailQueue::where('id', $emailQueue->id)->update([
                'status' => 'S'  
            ]);
        }
    }
}
