<?php

namespace App\Http\Controllers\Backend;

use App\Models\PurchaseDetailReviews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchaseDetailReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurchaseDetailReviews  $purchaseDetailReviews
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseDetailReviews $purchaseDetailReviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurchaseDetailReviews  $purchaseDetailReviews
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseDetailReviews $purchaseDetailReviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseDetailReviews  $purchaseDetailReviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseDetailReviews $purchaseDetailReviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurchaseDetailReviews  $purchaseDetailReviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseDetailReviews $purchaseDetailReviews)
    {
        //
    }
}
