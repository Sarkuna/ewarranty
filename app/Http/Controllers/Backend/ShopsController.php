<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\Shops;
use App\Models\Admin;
use App\Models\AssignManagerShops;
use App\Models\AssignUserShops;
use App\Models\States;

class ShopsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('role.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Shops"]];
        $shops = Shops::latest()->get();
        return view('admin.shops.index', ['breadcrumbs' => $breadcrumbs], compact('shops'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('role.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $managers = Admin::select('id','name','region')->where('type', 'manager')->get();
        $sales = Admin::select('id','name')->where('type', 'sales')->get();
        $states = States::select('id','state')->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('shops.index'), 'name' => "All Shops"], ['name' => "Create"]];
        return view('admin.shops.create', compact('breadcrumbs','managers','sales', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required|max:250',
            //'contact_num' => 'required|max:13',
            'address1' => 'required',
            'city' => 'required',
            'post_code' => 'required',
            'state' => 'required',
            'status' => 'required',
            //'manager_id' => 'required',
        ]);
        
        $data=$request->all();
        
        $status=Shops::create($data);


        if($status){
            if(!empty($request->manager_id)) {
                $manager_assign = new AssignManagerShops();
                $manager_assign->shop_id = $status->id;
                $manager_assign->manager_id = $request->manager_id;
                $manager_assign->save();
            }
            if(!empty($request->sales_id)) {
                $sales_assign = new AssignUserShops();
                $sales_assign->shop_id = $status->id;
                $sales_assign->manager_id = $request->manager_id;
                $sales_assign->sales_id = $request->sales_id;
                $sales_assign->save();
            }
            request()->session()->flash('success','Shop Successfully added');
            //$msg = ['success', 'Venue Successfully added'];
        }
        else{
            request()->session()->flash('error','Please try again!!');
            //$msg = ['error', 'Please try again!!'];
        }
        return redirect()->route('shops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function show(Shops $shops)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function edit(Shops $shops, $id)
    {
        //$post = Shops::findOrFail($id);
        $managers = Admin::select('id','name','region')->where('type', 'manager')->get();
        $sales = Admin::select('id','name')->where('type', 'sales')->get();
        $states = States::select('id','state')->get();
        $model = Shops::findOrFail($id);
        if(!empty($model->assign_manager->id)) {
            $manage_id = $model->assign_manager->manager_id;
        }else {
            $manage_id = '';
        }
        
        if(!empty($model->assign_sale_shop->id)) {
            $sale_id = $model->assign_sale_shop->sales_id;
        }else {
            $sale_id = '';
        }
        
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('shops.index'), 'name' => "All Shops"], ['name' => "Edit"]];
        return view('admin.shops.edit', compact('breadcrumbs','model','managers','sales', 'manage_id', 'sale_id', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$model->assign_sale_shop->id;
        $post = Shops::findOrFail($id);
        if(!empty($post->assign_manager->id)) {
            AssignManagerShops::destroy($post->assign_manager->id);
        }
        
        if(!empty($post->assign_sale_shop->id)) {
            AssignUserShops::destroy($post->assign_sale_shop->id);
        }
        
        $request->validate([
            'company_name' => 'required|max:250',
            //'contact_num' => 'required|max:12',
            'city' => 'required',
            'post_code' => 'required',
            'state' => 'required',
            'status' => 'required',
            //'manager_id' => 'required',
        ]);
        
        $data=$request->all();
        
        $status=$post->fill($data)->save();

        if($status){
            if(!empty($request->manager_id)) {
                $manager_assign = new AssignManagerShops();
                $manager_assign->shop_id = $id;
                $manager_assign->manager_id = $request->manager_id;
                $manager_assign->save();
            }
            if(!empty($request->sales_id)) {
                $sales_assign = new AssignUserShops();
                $sales_assign->shop_id = $id;
                $sales_assign->manager_id = $request->manager_id;
                $sales_assign->sales_id = $request->sales_id;
                $sales_assign->save();
            }
            request()->session()->flash('success','Shop Successfully updated');
            //$msg = ['success', 'Venue Successfully added'];
        }
        else{
            request()->session()->flash('error','Please try again!!');
            //$msg = ['error', 'Please try again!!'];
        }
        return redirect()->route('shops.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shops $shops)
    {
        //
    }
}