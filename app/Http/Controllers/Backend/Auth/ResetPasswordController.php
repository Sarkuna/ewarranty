<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Admin;
use Hash;

class ResetPasswordController extends Controller
{
    public function getPassword($token) {
        $admin = DB::table('password_resets')
                            ->where(['token' => $token])
                            ->first();
        if(!empty($admin)){
            return view('admin.auth.passwords.reset', ['token' => $token]);
        }else {
            return view('admin.auth.passwords.tokenExpiry');
        }
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            //'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',

        ]);
        
        $admin = DB::table('password_resets')
                            ->where(['token' => $request->token])
                            ->first();

        $updatePassword = DB::table('password_resets')
                            ->where(['email' => $admin->email, 'token' => $request->token])
                            ->first();

        if(!$updatePassword)
            return back()->withInput()->with('error', 'Invalid token!');

          $user = Admin::where('email', $admin->email)
                      ->update(['password' => Hash::make($request->password)]);

          DB::table('password_resets')->where(['email'=> $admin->email])->delete();

          return redirect('/admin')->with('success', 'Your password has been changed!');

    }
}
