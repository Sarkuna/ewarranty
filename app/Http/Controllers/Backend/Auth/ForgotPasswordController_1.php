<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use DB, Auth, Image;
use App\Models\Admin;
use Mail;

//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;
    
   // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /*protected $redirectTo = RouteServiceProvider::ADMIN_DASHBOARD;
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }*/
    /**
     * show login form for admin guard
     *
     * @return void
     */
    public function forgot()
    {
        return view('admin.auth.forgot');
    }
    
    public function password(Request $request)
    {
        $admin = Admin::whereEmail($request->email)->first();
        if(count($admin) == 0) {
            return redirect()->back()->with(['error' => 'Email not exists']);
        }
        
        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => str_random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();

        if ($this->sendEmail($request->email, $tokenData->token)) {
            return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
        } else {
            return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
        }
        
        //$admin = Sentinel::findById($admin->id);
        //$reminder = Reminder::exists($admin) ? : Reminder::create($admin);
        //$this->sendEmail($admin,$reminder->code);
        
        return redirect()->back()->with(['success' => 'Reset code send to your email']);
    }
    
    public function sendEmail($admin, $code)
    {
        $admin = Admin::whereEmail($admin)->first();
        Mail::send(
                'admin.email.forgot',
                ['admin' => $admin, 'code' => $code],
                function($message) use ($admin){
                    $message->from('noreplay@rewardssolution.com');
                    $message->to($admin->email);
                    $message->subject("$admin->email, rest your password.");
                }
        );
    }
}
