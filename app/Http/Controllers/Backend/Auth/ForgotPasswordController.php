<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use DB, Auth, Image;
use App\Models\Admin;
use Mail;
use Helper;

//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    public function getEmail()
    {

       return view('admin.auth.forgot');
    }

    public function postEmail(Request $request)
    {
        $admin = Admin::whereEmail($request->email)->first();
        if(count($admin) == 0) {
            return redirect()->back()->with(['error' => 'Email not exists']);
        }
        
        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => str_random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();
        
        $reset_token = $request->getSchemeAndHttpHost().'/admin/reset-password/'.$tokenData->token;
        
        $dataemail = ['name' => $request->name, 'email' => $request->email, 'reset_token' => $reset_token];    
        $subject = ['name' => $request->name]; 
        $return = Helper::sendEmailAdmin2($request->email, $subject, 'ZX6RKR', $dataemail);
        
        if($return) {
           return back()->with('success','A reset link has been sent to your email address.'); 
        }else {
          return back()->with('error','A Network Error occurred. Please try again.');  
        }
        /*if ($this->sendEmail($request->email, $tokenData->token)) {
            return back()->with('success','A reset link has been sent to your email address.');
        } else {
            return back()->with('error','A Network Error occurred. Please try again.');
        }*/
    }
    
    public function sendEmail($admin, $token)
    {
        $admin = Admin::whereEmail($admin)->first();
        Mail::send(
                'admin.email.forgot',
                ['admin' => $admin, 'token' => $token],
                function($message) use ($admin){
                    $message->from(env('APP_NO_REPLAY'),'support');
                    $message->to($admin->email);
                    $message->subject("$admin->email, rest your password.");
                }
        );
        
        if (Mail::failures()) {
            return false; 
        }else  {
            return true;
        }
    }
}
