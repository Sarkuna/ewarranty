<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\GlobalEmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
use PDF;

//Models
use App\Models\PurchaseDetails;
use App\Models\Shops;
use App\Models\Uploads;
use App\Models\UploadReviews;

use App\Models\States;
use App\Models\Cities;
use App\Models\Postcodes;
use App\Models\EmailQueue;

use App\User;

use Mail;
use Helper;

class PurchaseController extends Controller
{
    public $user;


    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('warranty.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Warranties"]];
        
        $query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->latest()->get();

        //}
        return view('admin.warranties.index', compact('breadcrumbs','models'));
    }
    
    public function accepted()
    {
        if (is_null($this->user) || !$this->user->can('warranty.accepted')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Warranties"]];
        
        $query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'in progress')->latest()->get();
        return view('admin.warranties.accepted', compact('breadcrumbs','models'));
    }
    
    public function reviews()
    {
        if (is_null($this->user) || !$this->user->can('warranty.reviews')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Warranties"]];
        $query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'review')->latest()->get();
        
        return view('admin.warranties.review', compact('breadcrumbs','models'));
    }
    
    
    
    public function approve()
    {
        if (is_null($this->user) || !$this->user->can('warranty.approve')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Warranties"]];
        $query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'approved')->latest()->get();
        return view('admin.warranties.approve', compact('breadcrumbs','models'));
    }
    
    public function decline()
    {
        if (is_null($this->user) || !$this->user->can('warranty.decline')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Warranties"]];
        $query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'decline')->latest()->get();
        return view('admin.warranties.decline', compact('breadcrumbs','models'));
    }
    
    
    public function onBehalf()
    {
        if (is_null($this->user) || !$this->user->can('warranty.on.behalf')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('warranty.index'), 'name' => "Warranties"], ['name' => "New Submission"]];
        $states = States::select('id','state')->get();
        return view('admin.warranties.create', compact('breadcrumbs','states'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $return= '';
        $msg = array();
        $newflag = 0;
        $req->validate([
            //'name' => 'required|max:200',
            //'email' => 'required|email|max:255|unique:users',
            //'mobile' => 'required|max:15',            
            'shop_state' => 'required',
            'shop_name' => 'required',
            'invoice_num' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required'           
        ]);
        
        
        DB::beginTransaction();
        try {// all good
            
            $myuser = User::where([['email',$req->email]])->first();
            
            if($myuser){
                $userid = $myuser->id;
                $newflag = 1;                
            }else{
                $password = Str::random(8);            
                $user = User::create([
                    'name' => $req->name,
                    'email' => $req->email,
                    'mobile' => $req->mobile,
                    'password' => Hash::make($password),
                    'status' => 'active',
                ]);
                
                $userid = $user->id;
            }

            $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();

            if($shops){
                $shopid = $shops->id;
            }else {
                $shop = Shops::create([
                    'company_name' => $req->shop_name,
                    'state' => $req->shop_state,
                    'status' => 'new',
                ]);
                $shopid = $shop->id;
            }
            
            if(!empty($req->surface_opt)){
                $surface_opt = implode(', ', $req->surface_opt);
            }else {
                $surface_opt = null;
            }

            $purchase = PurchaseDetails::create([
                'user_id' => $userid,
                'customer_name' => $req->name,
                'customer_mobile' => $req->mobile,
                'shop_id' => $shopid,
                'ref' => $req->ref,
                'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
                'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
                'address1' => $req->address1,
                'city' => $req->city,
                'post_code' => $req->postcode,
                'state' => $req->state,
                'visitor' => $req->ip(),
                //'agree' => $req->terms('agree') ? true : false,
                'agree' => 1,
                'invoice_num' => $req->invoice_num,
                'surface' => implode(', ', $req->surface),
                'surface_opt' => $surface_opt,
                'topcoat' => implode(', ', $req->topcoat),
                'a_name' => $req->a_name,
                'a_address' => $req->a_address,
                'a_state' => $req->a_state,
                'a_city' => $req->a_city,
                'a_postcode' => $req->a_postcode,
                'created_by' => Auth::user()->name,
                'sales_id' => null,
                'manager_id' => null,
            ]);
            
            if($purchase) {
                DB::commit();
                
                if($newflag == 0) {
                    $data = ['name' => $user->name,'email' => $user->email, 'password' => $password];    
                    $subject = '';
                    $return = Helper::sendEmailAdmin2($user->email, $subject, 'M50T7A', $data);
                }
                //dd($msg);
                $msg = ['success' => 'Thank you. Your form was successfully submitted.'];
                //return redirect()->route('sales.dashboard')->with($msg);
                return redirect()->route('warranty.index')->with($msg);
            }else {
                //dd($purchase);
                DB::rollback();                
                //$msg = 'Please try again!!';
                $msg = ['error' => 'Please try again!!'];
            }
            return redirect()->back()->with($msg);
            
        } catch (\Exception $e) {// something went wrong
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error','Oops! Please try again!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (is_null($this->user) || !$this->user->can('warranty.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        
        /*$query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'in progress')->latest()->get();*/
        $states = States::select('id','state')->get();
        $warranty = PurchaseDetails::find($id);
        if(!empty($warranty->surface)) {
            $surfaces = explode (",",$warranty->surface);
        }else {
            $surfaces = '';
        }
        
        if(!empty($warranty->surface_opt)) {
            $surfaceopts = explode (",",$warranty->surface_opt);
        }else {
            $surfaceopts = '';
        }
        
        if(!empty($warranty->topcoat)) {
            $topcoats = explode (",",$warranty->topcoat);
        }else {
            $topcoats = '';
        }
        
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('warranty.index'), 'name' => "Warranties"], ['name' => $warranty->warranty_code]];        
        
        if($warranty->status == 'approved') {
            return view('admin.warranties.show_approved',compact('breadcrumbs','warranty','surfaces','surfaceopts','topcoats'));
        }else {
           return view('admin.warranties.show',compact('breadcrumbs','warranty','surfaces','surfaceopts','topcoats','states')); 
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }
        $warranty = PurchaseDetails::find($id);
        
        if(!empty($warranty->surface)) {
            $surfaces = explode (",",$warranty->surface);
        }else {
            $surfaces = '';
        }
        
        if(!empty($warranty->surface_opt)) {
            $surfaceopts = explode (",",$warranty->surface_opt);
        }else {
            $surfaceopts = '';
        }
        
        if(!empty($warranty->topcoat)) {
            $topcoats = explode (",",$warranty->topcoat);
        }else {
            $topcoats = '';
        }
        
        $states = States::select('id','state')->get();
        if($warranty->status == 'approved'){
            return redirect()->route('warranty.show',$warranty->id);    
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/", 'name' => "Warranties"], ['name' => $warranty->warranty_code]];        
        return view('admin.warranties.edit',compact('breadcrumbs','warranty','states','surfaces','surfaceopts','topcoats'));
    }
    
    public function editCustomer($id)
    {
        if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }
        $warranty = PurchaseDetails::find($id);
        
        if(!empty($warranty->surface)) {
            $surfaces = explode (",",$warranty->surface);
        }else {
            $surfaces = '';
        }
        
        if(!empty($warranty->surface_opt)) {
            $surfaceopts = explode (",",$warranty->surface_opt);
        }else {
            $surfaceopts = '';
        }
        
        if(!empty($warranty->topcoat)) {
            $topcoats = explode (",",$warranty->topcoat);
        }else {
            $topcoats = '';
        }
        
        $states = States::select('id','state')->get();
        if($warranty->status == 'approved'){
            return redirect()->route('warranty.show',$warranty->id);    
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/", 'name' => "Warranties"], ['name' => $warranty->warranty_code]];        
        return view('admin.warranties.edit_customer',compact('breadcrumbs','warranty','states','surfaces','surfaceopts','topcoats'));
    }
    
    public function updateCustomer(Request $req)
    {

        if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }
        
        $id = $req->wid;
        $postPurchase=PurchaseDetails::findOrFail($id);
        $purchase = array(
            'customer_name' => $req->customer_name,
            'customer_mobile' => $req->customer_mobile,
        );
        
        $status = $postPurchase->fill($purchase)->save();
        if($status){
            $msg = ['success' => 'Customer Info Successfully update'];
        } else {
            $msg = ['error' => 'Please try again!!'];
        }
        
        if($req->status == 'in progress'){
            return redirect()->route('warranty.accepted')->with($msg);
        }elseif($req->status == 'review'){
            return redirect()->route('warranty.reviews')->with($msg);
        }else {        
            return redirect()->route('warranty.index')->with($msg);
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //dd($req->all());
        if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }
        
        $this->validate($req,[
            'shop_state' => 'required',
            'shop_name' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'status' => 'required',
            //"image_status.*" => "required",
            //"image_summary.*" => "required_if:image_status.*,in:Re Submit",
            //"image_summary.*" => "required_with:image_status.*,re_submit",
        ],
        [
            //'image_status.*.required' => 'Select Image Status',
            //'image_summary.*.required' => 'Comment can not be blank'
            //'image_summary.*.required_with:image_status.*,re_submit' => 'Comment can not be blank'
        ]);

        $postPurchase=PurchaseDetails::findOrFail($id);
        $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();
        $emailq = EmailQueue::where([['purchase_id',$id]])->count();

        if($shops){
            $shopid = $shops->id;
        }else {
            $shop = Shops::create([
                'company_name' => $req->shop_name,
                'state' => $req->shop_state,
                'status' => 'active',
            ]);
            $shopid = $shop->id;
        }
        
        $date_reminder = null;
        if($req->status == 'review') {
            if($emailq == 0){
                $date_reminder = now()->addDays(7)->format('Y-m-d');
            }
            
            if($emailq == 1){
                $date_reminder = now()->addDays(14)->format('Y-m-d');
            } 
        }
        
        if($req->status == 'approved') {
            if(!empty($req->product_year)) {
                $expiry_date = date('Y-m-d', strtotime($req->apply_date . " +$req->product_year year") );
            }else {
                $expiry_date = null;
            }
            
            if(!empty($req->product_year_power)) {
                $expiry_date_power = date('Y-m-d', strtotime($req->apply_date . " +$req->product_year_power year") );
            }else {
                $expiry_date_power = null;
            }
            
        }else {
            $expiry_date = null;
            $expiry_date_power = null;
        }
        
        if(!empty($postPurchase->assign_manager->id)) {
            $manage_id = $postPurchase->assign_manager->manager_id;
        }else {
            $manage_id = null;
        }
        
        if(!empty($postPurchase->assign_sale->id)) {
            $sale_id = $postPurchase->assign_sale->sales_id;
        }else {
            $sale_id = null;
        }
        
        if(!empty($req->status_remark)){
            $status_remark = $req->status_remark;
        }else {
            $status_remark = null;
        }

        $purchase = array(
            'shop_id' => $shopid,
            'invoice_num' => $req->invoice_num,
            'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
            'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
            'address1' => $req->address1,
            'address2' => $req->address2,
            'city' => $req->city,
            'post_code' => $req->postcode,
            'state' => $req->state,
            'product_year' => $req->product_year,
            'product_year_power' => $req->product_year_power,
            'expiry_date' => $expiry_date,
            'expiry_date_power' => $expiry_date_power,
            'status' => $req->status,
            'status_remark' => $status_remark,
            'manager_id' => $manage_id,
            'sales_id' => $sale_id,
        );

        $status = $postPurchase->fill($purchase)->save();
        if($status){
            $emailbcc = array();
            if(!empty($postPurchase->manager_id)) {
                $emailbcc[] = $postPurchase->manager->email;
                //$emailbcc[] = 'shihanagni@gmail.com';
            }

            if(!empty($postPurchase->sales_id)) {
                $emailbcc[] = $postPurchase->sales->email;
                //$emailbcc[] = 'shmshihan@gmail.com';
            }
            
            $emailbcc[] = env('APP_SUPPORT');

            $lists = '';
            if(!empty($req->image_id)) {
                $lists = '<ol>';
                foreach ($req->image_id as $key => $value) {
                    Uploads::where('id', $value)->update([
                        'summary' => $req->image_summary[$key],
                        'status' => $req->image_status[$key],    
                    ]);
                    if($req->image_status[$key] == 're_submit') {
                        UploadReviews::create([
                            'uploads_id' => $value,
                            'summary' => $req->image_summary[$key]
                        ]);
                        $upload = Uploads::where('id', $value)->first();
                        if($upload->type=='img_receipts'){
                            $imgtype = 'Receipts';
                        }else if($upload->type=='attach_photo_before'){
                            $imgtype = 'Photo Before';
                        }else if($upload->type=='attach_photo_after'){
                            $imgtype = 'Photo After';
                        }else if($upload->type=='all_paint_can'){
                            $imgtype = 'Attach Front of Pack of All Paint can';
                        }
                        $lists .= '<li>'.$imgtype.' - '.$req->image_summary[$key].'</li>';
                    }

                }
                $lists .= '</ol>';
            }

            $cc = null;
            
            if($req->status == 'review') {
                $dataemail = ['name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'list_of_resubmit' => $lists];    
                $subject = ['code' => $postPurchase->warranty_code];
                
                $input = [
                    'code' => 'Y9VCH3',
                    'user_id' => $postPurchase->customer_info->id,
                    'purchase_id' => $id,
                    'name'  => $postPurchase->customer_info->name,
                    'email'  => $postPurchase->customer_info->email,
                    'cc'  => $cc,
                    'bcc'  => $emailbcc,
                    'data' => $dataemail,
                    'created_date'  => $date_reminder,
                ];
                $item = EmailQueue::create($input);
                
                Helper::sendEmailAdmin2($postPurchase->customer_info->email, $subject, 'Y9VCH3', $dataemail, $cc, $emailbcc);
            }
            
            if($req->status == 'decline') {
                $dataemail = ['code' => $postPurchase->warranty_code, 'submission_date' => $postPurchase->created_at->format('M d D, Y'), 'reason_for_decline' => $postPurchase->status_remark, 'name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'list_of_resubmit' => $lists];    
                $subject = ['code' => $postPurchase->warranty_code];
                Helper::sendEmailAdmin2($postPurchase->customer_info->email, $subject, 'VIE1M6', $dataemail, $cc, $emailbcc);
            }
            
            if($req->status == 'approved') {
                if(!empty($postPurchase->product_year)) {
                    $pdf = $this->emailattach($id);
                    $pro1 = $postPurchase->product_name($postPurchase->product_year).', ';
                    $exp1 = date('d-m-Y', strtotime($postPurchase->expiry_date)).', ';
                }else {
                    $pdf = ""; $pro1="";$exp1="";
                }
                
                if(!empty($postPurchase->product_year_power)) {
                    $pdf2 = $this->emailattachPowerflexx($id);
                    $pro2 = $postPurchase->product_name($postPurchase->product_year_power);
                    $exp2 = date('d-m-Y', strtotime($postPurchase->expiry_date_power));
                }else {
                    $pdf2 = ""; $pro2=""; $exp2 ="";
                }
                
                $product_name = $pro1.$pro2;
                $expiry_date = $exp1.$exp2;
                
                $dataemail = ['expiry_date' => $expiry_date,'code' => $postPurchase->warranty_code, 'name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'product_name' => $product_name];    
                $subject = ['code' => $postPurchase->warranty_code];
                Helper::sendEmailAdminwithAttachment($postPurchase->customer_info->email, $subject, 'VIE1M5', $dataemail, $cc, $emailbcc, $pdf, $pdf2);
            }
            $msg = ['success' => 'Submissions Successfully update'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('warranty.index')->with($msg);
    }
    
    public function editsales($id)
    {
        /*if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }*/
        $warranty = PurchaseDetails::find($id);
        
        /*$query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'in progress')->latest()->get();*/
        if($warranty->status == 'approved'){
            return redirect()->route('warranty.show',$warranty->id);    
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/", 'name' => "Warranties"], ['name' => $warranty->warranty_code]];        
        return view('admin.warranties.sales_edit',compact('breadcrumbs','warranty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function updatesales(Request $req, $id)
    {
        $this->validate($req,[
            'shop_state' => 'required',
            'shop_name' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
        ],
        [
            //'image_status.*.required' => 'Select Image Status',
            //'image_summary.*.required' => 'Comment can not be blank'
            //'image_summary.*.required_with:image_status.*,re_submit' => 'Comment can not be blank'
        ]);

        $postPurchase=PurchaseDetails::findOrFail($id);
        $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();

        if($shops){
            $shopid = $shops->id;
        }else {
            $shop = Shops::create([
                'company_name' => $req->shop_name,
                'state' => $req->shop_state,
                'status' => 'active',
            ]);
            $shopid = $shop->id;
        }
        
        if($req->status == 'approved') {
            if(!empty($req->product_year)) {
                $expiry_date = date('Y-m-d', strtotime($req->apply_date . " +$req->product_year year") );
            }else {
                $expiry_date = null;
            }
            
            if(!empty($req->product_year_power)) {
                $expiry_date_power = date('Y-m-d', strtotime($req->apply_date . " +$req->product_year_power year") );
            }else {
                $expiry_date_power = null;
            }
            
        }else {
            $expiry_date = null;
            $expiry_date_power = null;
        }
        
        if(!empty($postPurchase->assign_manager->id)) {
            $manage_id = $postPurchase->assign_manager->manager_id;
        }else {
            $manage_id = null;
        }
        
        if(!empty($postPurchase->assign_sale->id)) {
            $sale_id = $postPurchase->assign_sale->sales_id;
        }else {
            $sale_id = null;
        }
        
        if(!empty($req->status_remark)){
            $status_remark = $req->status_remark;
        }else {
            $status_remark = null;
        }

        $purchase = array(
            'shop_id' => $shopid,
            'invoice_num' => $req->invoice_num,
            'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
            'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
            'address1' => $req->address1,
            'address2' => $req->address2,
            'city' => $req->city,
            'post_code' => $req->postcode,
            'state' => $req->state,
            'product_year' => $req->product_year,
            'product_year_power' => $req->product_year_power,
            'expiry_date' => $expiry_date,
            'expiry_date_power' => $expiry_date_power,
            'status' => $req->status,
            'status_remark' => $status_remark,
            'manager_id' => $manage_id,
            'sales_id' => $sale_id,
        );

        $status = $postPurchase->fill($purchase)->save();
        if($status){
            $cc = null;
            $emailbcc = array();
            if(!empty($postPurchase->manager_id)) {
                $emailbcc[] = $postPurchase->manager->email;
                //$emailbcc[] = 'shihanagni@gmail.com';
            }

            if(!empty($postPurchase->sales_id)) {
                $emailbcc[] = $postPurchase->sales->email;
                //$emailbcc[] = 'shmshihan@gmail.com';
            }            
            $emailbcc[] = env('APP_SUPPORT');
            $lists = '';
            if(!empty($req->image_id)) {
                $lists = '<ol>';
                foreach ($req->image_id as $key => $value) {
                    Uploads::where('id', $value)->update([
                        'summary' => $req->image_summary[$key],
                        'status' => $req->image_status[$key],    
                    ]);
                    if($req->image_status[$key] == 're_submit') {
                        UploadReviews::create([
                            'uploads_id' => $value,
                            'summary' => $req->image_summary[$key]
                        ]);
                        $upload = Uploads::where('id', $value)->first();
                        if($upload->type=='img_receipts'){
                            $imgtype = 'Receipts';
                        }else if($upload->type=='attach_photo_before'){
                            $imgtype = 'Photo Before';
                        }else if($upload->type=='attach_photo_after'){
                            $imgtype = 'Photo After';
                        }else if($upload->type=='all_paint_can'){
                            $imgtype = 'Attach Front of Pack of All Paint can';
                        }
                        $lists .= '<li>'.$imgtype.' - '.$req->image_summary[$key].'</li>';
                    }

                }
                $lists .= '</ol>';
            }
            
            if($req->status == 'review') {
                $emailq = EmailQueue::where([['purchase_id',$id],['status','P']])->count();
                if($emailq == 0){
                    $date_reminder = now()->addDays(7)->format('Y-m-d');
                }

                if($emailq == 1){
                    $date_reminder = now()->addDays(14)->format('Y-m-d');
                } 
                $dataemail = ['name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'list_of_resubmit' => $lists];    
                $subject = ['code' => $postPurchase->warranty_code];
                $input = [
                    'code' => 'Y9VCH3',
                    'user_id' => $postPurchase->customer_info->id,
                    'purchase_id' => $id,
                    'name'  => $postPurchase->customer_info->name,
                    'email'  => $postPurchase->customer_info->email,
                    'cc'  => $cc,
                    'bcc'  => $emailbcc,
                    'data' => $dataemail,
                    'created_date'  => $date_reminder,
                ];
                $item = EmailQueue::create($input);
                Helper::sendEmailAdmin2($postPurchase->customer_info->email, $subject, 'Y9VCH3', $dataemail, $cc, $emailbcc);
            }
            
            /*if($req->status == 'decline') {
                $dataemail = ['code' => $postPurchase->warranty_code, 'submission_date' => $postPurchase->created_at->format('M d D, Y'), 'reason_for_decline' => $postPurchase->status_remark, 'name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'list_of_resubmit' => $lists];    
                $subject = ['code' => $postPurchase->warranty_code];
                Helper::sendEmailAdmin2($postPurchase->customer_info->email, $subject, 'VIE1M6', $dataemail, $cc, $bcc);
            }*/
            
            /*if($req->status == 'approved') {
                if(!empty($postPurchase->product_year)) {
                    $pdf = $this->emailattach($id);
                }else {
                    $pdf = "";
                }
                
                if(!empty($postPurchase->product_year_power)) {
                    $pdf2 = $this->emailattachPowerflexx($id);
                }else {
                    $pdf2 = "";
                }
                
                $dataemail = ['expiry_date' => date('d-m-Y', strtotime($postPurchase->expiry_date)),'code' => $postPurchase->warranty_code, 'name' => $postPurchase->customer_info->name, 'email' => $postPurchase->customer_info->email, 'product_name' => $postPurchase->product_name($postPurchase->product_year)];    
                $subject = ['code' => $postPurchase->warranty_code];
                Helper::sendEmailAdminwithAttachment($postPurchase->customer_info->email, $subject, 'VIE1M5', $dataemail, $cc, $bcc, $pdf, $pdf2);
            }*/
            $msg = ['success' => 'Submissions Successfully update'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('warranty.index')->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseDetails $purchaseDetails)
    {
        //
    }
    
    public function autocomplete(Request $request)
    {
        //dd($request->all());
        if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }
        $query = $request->get('query','');
        $state = $request->get('state','');
        $datas = Shops::select("company_name")
                ->where('company_name','LIKE','%'.$query.'%')
                ->where('state','=',$state)
                ->get();
        
        $dataModified = array();
        foreach ($datas as $data)
        {
          $dataModified[] = $data->company_name;
        }

        return response()->json($dataModified);
    }
    
    public function reuploadImages(Request $request) {
        //dd($request->all()); 
        /*if (is_null($this->user) || !$this->user->can('warranty.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any role !');
        }*/
        $preview = $config = $errors = [];
        
        $_key = $request->_key;
        $_id = $request->_id;        
        $_type = $request->_type;
        
        $model = Uploads::find($_id);
        $_ref = $model->ref;
       
        $destination_path = 'public/purchase_detail/' . $model->ref;
        /*if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        }*/           

        if ($request->hasFile('uploadFile')) {
            
            if ($model !== NULL) {
                $filename = 'public/purchase_detail/' . $model->ref . '/' . $model->real_filename;
                $filename = storage_path('app/public/purchase_detail/' . $model->ref . '/' . $model->real_filename);
                if (File::exists($filename)) {
                    File::delete($filename);
                }
                //$model->delete();
            }

            $allowedfileExtension = ['jpg', 'png', 'gif'];
            if(!empty($request->file('uploadFile'))) {
                $file = $request->file('uploadFile');
            }else {
                $file = '';
            }
            
            if(!empty($file)) {
                    $filename = $file->getClientOriginalName();
                    $fileSize = $file->getSize();
                    $extension = $file->getClientOriginalExtension();
                    
                    $baseName = $filename;
                    $realFileName   = md5($baseName.time()) . '.' . $extension;
                    
                    $path = $file->storeAs($destination_path,$realFileName);                    
                    $newFileUrl = asset('/storage/purchase_detail/'.$_ref.'/'.$realFileName);
                    
                    if($path) {                        
                        $uploads = Uploads::where('id', $_id)
                        ->update([
                            'file_name' => $baseName, 'real_filename' => $realFileName, 'status' => 'pending', 'summary' => ''
                         ]);
                        $lastId = $_id;
                        $preview[] = $newFileUrl;
                        //array_push($preview, $newFileUrl);
                        $config[] = [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/reedit', // server api to delete the file based on key
                        ];
                    }else {
                        $errors[] = $fileName;
                    }
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                if (!empty($errors)) {
                    $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
                    $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
                }
                return $out;
            }else {
                return [];
            }
        }else {
            return [];
        }
    }
    
    // Generate PDF
    public function createPDF($id) {
        $model = PurchaseDetails::find($id);
        
        if($model->status == "approved")
        {
            $product_name = str_replace('_', ' ', env('PRODUCT_NAME7'));

            if(!empty($model->shop_info->address1)) {
                $address = $model->shop_info->address1.$model->shop_info->address2;
            }else {
                $address = 'N/A';
            }
            
            if (!empty($model->apply_date)) {
                $apply_date = $model->apply_date->format('d-m-Y');
            } else {
                $apply_date = 'N/A';
            }
            
            if (!empty($model->a_name)) {
                $a_name = $model->a_name;
            } else {
                $a_name = 'N/A';
            }
            
            if (!empty($model->a_address)) {
                $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
            } else {
                $a_address = 'N/A';
            }
            
            $model = [
                'warranty_code' => $model->warranty_code,
                'product_name' => $product_name,
                'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
                'apply_date' => $apply_date,
                'store_name' => $model->shop_info->company_name,
                'address' => $address,
                'post_code' => $model->shop_info->post_code,
                'city' => $model->shop_info->city,
                'state' => $model->shop_info->state,
                'name' => $model->customer_name,
                'email' => $model->customer_info->email,
                'mobile' => $model->customer_mobile,
                'expiry_date' => $model->expiry_date->format('d-m-Y'),
                'customer_address' => $model->address1.' '.$model->address2,
                'customer_city' => $model->city,
                'customer_post_code' => $model->post_code,
                'customer_state' => $model->state,
                'painter_name' => $a_name,
                'painter_address' => $a_address,
                
                
            ];
            $pdf = PDF::loadView('admin.warranties.pdf_view', $model);
            //$pdf = PDF::loadView('pdf', compact('show'));
            return $pdf->stream(date('d-m-Y').".pdf");
        }
        return response(view('errors.404'), 404);
    }
    
    public function createPDFPowerflexx($id) {
        $model = PurchaseDetails::find($id);
        
        if($model->status == "approved")
        {
            $product_name = str_replace('_', ' ', env('PRODUCT_NAME12'));

            if(!empty($model->shop_info->address1)) {
                $address = $model->shop_info->address1.$model->shop_info->address2;
            }else {
                $address = 'N/A';
            }
            
            if (!empty($model->apply_date)) {
                $apply_date = $model->apply_date->format('d-m-Y');
            } else {
                $apply_date = 'N/A';
            }
            
            if (!empty($model->a_name)) {
                $a_name = $model->a_name;
            } else {
                $a_name = 'N/A';
            }
            
            if (!empty($model->a_address)) {
                $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
            } else {
                $a_address = 'N/A';
            }
            
            if(!empty($model->expiry_date_power)){
                $expiry_date_power = $model->expiry_date_power->format('d-m-Y');
            }else{
                $expiry_date_power = 'N/A';
            }
            
            $model = [
                'warranty_code' => $model->warranty_code,
                'product_name' => $product_name,
                'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
                'apply_date' => $apply_date,
                'store_name' => $model->shop_info->company_name,
                'address' => $address,
                'post_code' => $model->shop_info->post_code,
                'city' => $model->shop_info->city,
                'state' => $model->shop_info->state,
                'name' => $model->customer_name,
                'email' => $model->customer_info->email,
                'mobile' => $model->customer_mobile,
                'expiry_date' => $expiry_date_power,
                'customer_address' => $model->address1.' '.$model->address2,
                'customer_city' => $model->city,
                'customer_post_code' => $model->post_code,
                'customer_state' => $model->state,
                'painter_name' => $a_name,
                'painter_address' => $a_address,
                
            ];
            $pdf = PDF::loadView('admin.warranties.pdf_view', $model);
            //$pdf = PDF::loadView('pdf', compact('show'));
            return $pdf->stream(date('d-m-Y').".pdf");
        }
        return response(view('errors.404'), 404);
    }
    
    public function emailattach($id)
    {
        $model = PurchaseDetails::find($id);

        if (!empty($model->shop_info->address1)) {
            $address = $model->shop_info->address1 . $model->shop_info->address2;
        } else {
            $address = 'N/A';
        }
        
        if (!empty($model->apply_date)) {
            $apply_date = $model->apply_date->format('d-m-Y');
        } else {
            $apply_date = 'N/A';
        }
        
        if (!empty($model->a_name)) {
            $a_name = $model->a_name;
        } else {
            $a_name = 'N/A';
        }

        if (!empty($model->a_address)) {
            $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
        } else {
            $a_address = 'N/A';
        }

        $model = [
            'warranty_code' => $model->warranty_code,
            'product_name' => $model->product_name($model->product_year),
            'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
            'apply_date' => $apply_date,
            'store_name' => $model->shop_info->company_name,
            'address' => $address,
            'post_code' => $model->shop_info->post_code,
            'city' => $model->shop_info->city,
            'state' => $model->shop_info->state,
            'name' => $model->customer_info->name,
            'email' => $model->customer_info->email,
            'mobile' => $model->customer_info->mobile,
            'expiry_date' => $model->expiry_date->format('d-m-Y'),
            'customer_address' => $model->address1 . ' ' . $model->address2,
            'customer_city' => $model->city,
            'customer_post_code' => $model->post_code,
            'customer_state' => $model->state,
            'painter_name' => $a_name,
            'painter_address' => $a_address,
        ];
        $pdf = PDF::loadView('admin.warranties.pdf_view', $model);
        return $pdf;

    }
    
    public function emailattachPowerflexx($id)
    {
        $model = PurchaseDetails::find($id);

        if (!empty($model->shop_info->address1)) {
            $address = $model->shop_info->address1 . $model->shop_info->address2;
        } else {
            $address = 'N/A';
        }
        
        if (!empty($model->apply_date)) {
            $apply_date = $model->apply_date->format('d-m-Y');
        } else {
            $apply_date = 'N/A';
        }
        
        if (!empty($model->expiry_date_power)) {
            $expiry_date_power = $model->expiry_date_power->format('d-m-Y');
        } else {
            $expiry_date_power = 'N/A';
        }
        
        if (!empty($model->a_name)) {
            $a_name = $model->a_name;
        } else {
            $a_name = 'N/A';
        }

        if (!empty($model->a_address)) {
            $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
        } else {
            $a_address = 'N/A';
        }

        $model = [
            'warranty_code' => $model->warranty_code,
            'product_name' => str_replace('_', ' ', env('PRODUCT_NAME12')),
            'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
            'apply_date' => $apply_date,
            'store_name' => $model->shop_info->company_name,
            'address' => $address,
            'post_code' => $model->shop_info->post_code,
            'city' => $model->shop_info->city,
            'state' => $model->shop_info->state,
            'name' => $model->customer_info->name,
            'email' => $model->customer_info->email,
            'mobile' => $model->customer_info->mobile,
            'expiry_date' => $expiry_date_power,
            'customer_address' => $model->address1 . ' ' . $model->address2,
            'customer_city' => $model->city,
            'customer_post_code' => $model->post_code,
            'customer_state' => $model->state,
            'painter_name' => $a_name,
            'painter_address' => $a_address,
        ];
        $pdf = PDF::loadView('admin.warranties.pdf_powerflexx_view', $model);
        //return $pdf->stream(date('d-m-Y').".pdf");
        return $pdf;
    }
    
    public function get_by_states(Request $request)
    {
        return Helper::myStates($request->state_id);
    }
    
    public function get_by_cities(Request $request)
    {
        return Helper::myCities($request->city_id);
    }
    
    public function get_by_shops(Request $request)
    {
        $query = $request->get('query','');
        $state = $request->get('state','');
        return Helper::myShops($query,$state);
    }
    
    public function storeMedia(Request $request) {
        $_ref = $request->ref;
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        } 
        //$allowedfileExtension = ['jpg', 'png', 'gif', 'pdf'];
        
        $image = $request->file('file');
        $img_type = $request->img_type;
        $imageName = $image->getClientOriginalName();
        $extension = $image->getClientOriginalExtension();
        $rename = str_replace(' ','_',$imageName);
        
        $realFileName = md5(time()).$rename;
        
        $path = $image->storeAs($destination_path, $realFileName);
        $newFileUrl = asset('/storage/purchase_detail/' . $_ref . '/' . $realFileName);
        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $imageName, 'real_filename' => $realFileName, 'type' => $img_type]);
        echo $realFileName;
    }
    
    public function deleteMedia(Request $request) {
        $_ref = $request->ref;
        $image = $request->url;
        $img_type = $request->img_type;
        
        $filename  = storage_path('app/public/purchase_detail/'.$_ref.'/'.$image);
        if(File::exists($filename)) {
            $uploaded_image = Uploads::where('ref', '=', $_ref)->where('real_filename', '=', $image)->where('type', '=', $img_type)->first();
            $id = $uploaded_image->id;
            $upload = Uploads::find($id);
            if (!is_null($upload)) {
                $upload->delete();
            }
            File::delete($filename);
        }
        
        return $filename; 
    }
    
    public function checkemail(Request $request)
    {
        
        $email = $request->get('email','');
        
        $useremail = User::all()->where('email', $email)->first();
        if ($useremail) {
            $data = ['name' => $useremail->name,'mobile' => $useremail->mobile];
            return response()->json($data);
        } else {
            return response()->json('no');
        }

        //return response()->json($dataModified);
    }
    
    public function sendEmail($id,Request $request)
    {
        //$dd = substr("Hello world",0,100);
        $token = str_random(60);
        $purchase = PurchaseDetails::find($id);

        $shop = Shops::find($purchase->shop_id);
        $shop->token = $token;
        $shop->save();
        $reset_token = $request->getSchemeAndHttpHost().'/admin/shop/'.$token;
        $data = ['shopname' => $purchase->shop_info->company_name, 'token' => $reset_token];    
        $subject = '';
        $return = Helper::sendEmailAdmin2(env('APP_SHOP_NOTIFY'), $subject, 'M50T7C', $data);
        return redirect()->route('warranty.accepted')->with(['success' => 'Shop info email Successfully send']);
    }
}
