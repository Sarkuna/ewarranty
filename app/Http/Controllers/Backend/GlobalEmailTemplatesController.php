<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\GlobalEmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Admin;

class GlobalEmailTemplatesController extends Controller
{
    public $user;


    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('email.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Email Template"]];
        $emailtemplates = GlobalEmailTemplate::latest()->get();
        return view('admin.global-email-templates.index', ['breadcrumbs' => $breadcrumbs], compact('emailtemplates'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        if (is_null($this->user) || !$this->user->can('email.create')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('global-email-templates.index'), 'name' => "All Email Template"], ['name' => "Create"]];
        return view('admin.global-email-templates.create', ['breadcrumbs' => $breadcrumbs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'name'=>'string|required',
            'code'=>'string|nullable',
            'subject'=>'string|required',
            'template'=>'string|nullable',
            'status'=>'required|in:yes,no'
        ]);

        $data=$request->all();


        // return $data;

        $status=GlobalEmailTemplate::create($data);
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Email Template Successfully added';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('global-email-templates.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GlobalEmailTemplate  $globalEmailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(GlobalEmailTemplate $globalEmailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GlobalEmailTemplate  $globalEmailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('email.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $emailtemplate=GlobalEmailTemplate::findOrFail($id);
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('global-email-templates.index'), 'name' => "All Email Template"], ['name' => "Edit"]];
        return view('admin.global-email-templates.edit', ['breadcrumbs' => $breadcrumbs])->with('emailtemplate',$emailtemplate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GlobalEmailTemplate  $globalEmailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post=GlobalEmailTemplate::findOrFail($id);
        $this->validate($request,[
            'name'=>'string|required',
            'subject'=>'string|required',
            'template'=>'string|nullable',
            'status'=>'required|in:yes,no'
        ]);

        $data=$request->all();


        // return $data;

        $status=$post->fill($data)->save();
        if($status){
            //request()->session()->flash('success','Post Successfully added');
            $msg = 'Email Template Successfully update';
        }
        else{
            //request()->session()->flash('error','Please try again!!');
            $msg = 'Please try again!!';
        }
        return redirect()->route('global-email-templates.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GlobalEmailTemplate  $globalEmailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('email.delete')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        GlobalEmailTemplate::destroy($id);
        return redirect()->route('global-email-templates.index')->with('success', 'Delete successfully!');
    }
}
