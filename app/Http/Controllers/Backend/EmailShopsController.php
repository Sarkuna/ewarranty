<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\Shops;
use App\Models\Admin;
use App\Models\AssignManagerShops;
use App\Models\AssignUserShops;
use App\Models\States;

class EmailShopsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($token)
    {
        $shop = Shops::where(['token' => $token])->first();
        if(!empty($shop)) {
            $id = $shop->id;
            $states = States::select('id','state')->get();
            $managers = Admin::select('id','name','region')->where('type', 'manager')->get();
            $sales = Admin::select('id','name')->where('type', 'sales')->get();
            $model = Shops::findOrFail($id);
            if(!empty($model->assign_manager->id)) {
                $manage_id = $model->assign_manager->manager_id;
            }else {
                $manage_id = '';
            }

            if(!empty($model->assign_sale_shop->id)) {
                $sale_id = $model->assign_sale_shop->sales_id;
            }else {
                $sale_id = '';
            }

            $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('shops.index'), 'name' => "All Shops"], ['name' => "Edit"]];
            return view('admin.shops.email_edit', compact('breadcrumbs','model','managers','sales', 'manage_id', 'sale_id', 'token', 'states'));

        }else {
            return view('admin.shops.email_fail');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $shop = Shops::where(['token' => $request->token])->first();
        if(!empty($shop)) {
            $id = $shop->id;
            //$model->assign_sale_shop->id;
            $post = Shops::findOrFail($id);
            if(!empty($post->assign_manager->id)) {
                AssignManagerShops::destroy($post->assign_manager->id);
            }

            if(!empty($post->assign_sale_shop->id)) {
                AssignUserShops::destroy($post->assign_sale_shop->id);
            }

            $request->validate([
                'company_name' => 'required|max:250',
                //'contact_num' => 'required|max:12',
                'city' => 'required',
                'post_code' => 'required',
                'state' => 'required',
                'status' => 'required',
                'manager_id' => 'required',
            ]);

            $data=$request->all();
            $data['token'] = null;
            $status=$post->fill($data)->save();

            if($status){
                if(!empty($request->manager_id)) {
                    $manager_assign = new AssignManagerShops();
                    $manager_assign->shop_id = $id;
                    $manager_assign->manager_id = $request->manager_id;
                    $manager_assign->save();
                }
                if(!empty($request->sales_id)) {
                    $sales_assign = new AssignUserShops();
                    $sales_assign->shop_id = $id;
                    $sales_assign->manager_id = $request->manager_id;
                    $sales_assign->sales_id = $request->sales_id;
                    $sales_assign->save();
                }
                //request()->session()->flash('success','Shop Successfully updated');
                return view('admin.shops.email_thank_you');
            }
            else{
                $msg = ['error' => 'Please try again!!'];
                return redirect()->route('guest.shop.token',$request->token)->with($msg);
            }
        }else {
            return view('admin.shops.email_fail');
        }
    }
}
