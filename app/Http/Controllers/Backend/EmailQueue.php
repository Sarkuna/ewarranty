<?php

namespace App\Http\Controllers\Backend;

use App\Models\EmailQueue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailQueue extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailQueue  $emailQueue
     * @return \Illuminate\Http\Response
     */
    public function show(EmailQueue $emailQueue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailQueue  $emailQueue
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailQueue $emailQueue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailQueue  $emailQueue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailQueue $emailQueue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailQueue  $emailQueue
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailQueue $emailQueue)
    {
        //
    }
}
