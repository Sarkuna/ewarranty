<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\GlobalEmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
use PDF;

//Models
use App\Models\PurchaseDetails;
use App\Models\Shops;
use App\Models\Uploads;
use App\Models\UploadReviews;

use App\Models\States;
use App\Models\Cities;
use App\Models\Postcodes;

use App\User;

use Mail;
use Helper;

class TeamPurchaseController extends Controller
{
    public $user;


    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseDetails $purchaseDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warranty = PurchaseDetails::find($id);
        
        /*$query = PurchaseDetails::query();
        if($this->user->type == 'sales'){
            $lists = Helper::assign_shops('sales',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }else if($this->user->type == 'manager'){
            $lists = Helper::assign_shops('manager',$this->user->id);
            $query = $query->whereIn('shop_id', $lists);
        }
        
        $models = $query->where('status', '=', 'in progress')->latest()->get();*/
        if($warranty->status == 'approved'){
            return redirect()->route('warranty.show',$warranty->id);    
        }
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => "/", 'name' => "Warranties"], ['name' => $warranty->warranty_code]];        
        return view('admin.teams.edit',compact('breadcrumbs','warranty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $this->validate($req,[
            'shop_state' => 'required',
            'shop_name' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
        ]);

        $postPurchase=PurchaseDetails::findOrFail($id);
        $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();

        if($shops){
            $shopid = $shops->id;
        }else {
            $shop = Shops::create([
                'company_name' => $req->shop_name,
                'state' => $req->shop_state,
                'status' => 'active',
            ]);
            $shopid = $shop->id;
        }
        


        $purchase = array(
            'shop_id' => $shopid,
            'invoice_num' => $req->invoice_num,
            'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
            'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
            'address1' => $req->address1,
            'address2' => $req->address2,
            'city' => $req->city,
            'post_code' => $req->postcode,
            'state' => $req->state,
        );

        $status = $postPurchase->fill($purchase)->save();
        if($status){
            $msg = ['success' => 'Submissions Successfully update'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('warranty.reviews')->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseDetails $purchaseDetails)
    {
        //
    }
}
