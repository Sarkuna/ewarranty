<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;

use App\Models\Admin;

use Mail;
use Helper;

class RegionalSalesManagerController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('managers.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any role !');
        }
        $models = Admin::where('type', '=', 'manager')->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Regional Manager"]];
        return view('admin.managers.index', compact('breadcrumbs','models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('managers.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }

        $roles  = Role::all();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('managers.index'), 'name' => "All Regional Manager"], ['name' => "Create"]];
        return view('admin.managers.create', compact('roles','breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username = strstr($request->email, '@', true);
        $pass = Str::random(8);

        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|max:100|email|unique:admins,email,NULL,id,type,manager',
            'region' => 'required',
            //'password' => 'required|min:6',
            //'confirm_password' => ['same:password'],
        ]);
        $username = date('hms').'_'.$username;
        $username1 = mb_strimwidth($username, 0, 20);
        
        // Create New User
        $user = new Admin();
        $user->name = $request->name;
        $user->username = $username1;
        $user->email = $request->email;
        $user->type = 'manager';
        $user->status = $request->status;
        $user->password = Hash::make($pass);
        $user->region = $request->region;
        $user->save();

        if($user){
            $user->assignRole($request->roles);
            $dataemail = ['name' => $request->name, 'email' => $request->email, 'password' => $pass];    
            $subject = ['name' => $request->name]; 
            $return = Helper::sendEmailAdmin2($request->email, $subject, 'PUGM68', $dataemail);
            
            //request()->session()->flash('success','Post Successfully added');
            $msg = ['success' => 'Regional Manager has been created !!'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('managers.index')->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admins  $admins
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->user) || !$this->user->can('managers.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
        $model = Admin::where([['type', '=', 'manager']])->findOrFail($id);
        $roles  = Role::all();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('managers.index'), 'name' => "All Reginoal Managers"], ['name' => "Create"]];
        return view('admin.managers.edit', compact('breadcrumbs','model','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admins  $admins
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Admin::where([['type', '=', 'manager']])->findOrFail($id);

        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email|unique:admins,email,'. $id.',id,type,manager',
            //'mobile' => 'required|max:15',
            //'password' => 'required|min:6',
            //'confirm_password' => ['same:password'],
        ]);
        
        $data=$request->all();

        $user=$post->fill($data)->save();
        
        if($user){
            $post->roles()->detach();
            if ($request->roles) {
                $post->assignRole($request->roles);
            }
            $msg = ['success' => 'Regional Manager has been updated !!'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('managers.index')->with($msg);
    }

}
