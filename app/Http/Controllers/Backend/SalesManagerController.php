<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;

use App\Models\Admin;

use Mail;
use Helper;

class SalesManagerController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Admin::where('type', '=', 'sales')->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Sales PIC"]];
        return view('admin.sales.index', compact('breadcrumbs','models'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mysales()
    {
        $models = Admin::where('manager_id', '=', $this->user->id)->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Sales PIC"]];
        return view('admin.sales.my_sales_team_index', compact('breadcrumbs','models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('sales.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }

        $roles  = Role::all();
        $managers = Admin::select('id','name','region')->where('type', 'manager')->get();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('sales.index'), 'name' => "All Sales PIC"], ['name' => "Create"]];
        return view('admin.sales.create', compact('breadcrumbs','managers','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            //'email' => 'required|max:100|email|unique:admins,type,sales',
            'email' => 'required|max:100|email|unique:admins,email,NULL,id,type,sales',
            //'mobile' => 'required|max:15',
            //'manager_id' => 'required'
            //'password' => 'required|min:6',
            //'confirm_password' => ['same:password'],
        ]);
        
        //$username = substr($request->email, strpos($request->email, '@') + 1);
        $username = strstr($request->email, '@', true);
        $pass = Str::random(8);
        
        $username = date('hms').'_'.$username;
        $username1 = mb_strimwidth($username, 0, 20);

        // Create New User
        $user = new Admin();
        $user->name = $request->name;
        $user->username = $username1;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->type = 'sales';
        $user->status = $request->status;
        $user->password = Hash::make($pass);
        $user->manager_id = $request->manager_id;
        $user->save();
        
        if($user){
            $user->assignRole($request->roles);
            $dataemail = ['name' => $request->name, 'email' => $request->email, 'password' => $pass];    
            $subject = ['name' => $request->name];
            $return = Helper::sendEmailAdmin2($request->email, $subject, 'PUGM68', $dataemail);
            
            //request()->session()->flash('success','Post Successfully added');
            $msg = ['success' => 'Sales PIC has been created !!'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('sales.index')->with($msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $managers = Admin::select('id','name','region')->where('type', 'manager')->get();
        $model = Admin::where([['type', '=', 'sales']])->findOrFail($id);
        
        $admin = Admin::find($id);
        $roles  = Role::all();
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['link' => route('sales.index'), 'name' => "All Sales PIC"], ['name' => "Create"]];
        return view('admin.sales.edit', compact('breadcrumbs','model','roles','managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Admin::where([['type', '=', 'sales']])->findOrFail($id);

        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email|unique:admins,email,'. $id.',id,type,sales',
            //'mobile' => 'required|max:15',
            //'manager_id' => 'required'
            //'password' => 'required|min:6',
            //'confirm_password' => ['same:password'],
        ]);
        
        $data=$request->all();

        $user=$post->fill($data)->save();
        
        
        
        if($user){
            $post->roles()->detach();
            if ($request->roles) {
                $post->assignRole($request->roles);
            }
            //$dataemail = ['name' => $request->name, 'email' => $request->email, 'password' => $request->confirm_password];    
            //$subject = '';
            //$return = Helper::sendEmailAdmin2($request->email, $subject, 'PUGM68', $dataemail);
            
            //request()->session()->flash('success','Post Successfully added');
            $msg = ['success' => 'Sales PIC has been updated !!'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
            //$msg = 'Please try again!!';
        }
        return redirect()->route('sales.index')->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
