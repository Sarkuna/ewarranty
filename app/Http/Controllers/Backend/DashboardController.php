<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

use App\Rules\MatchOldPasswordAdmin;
use App\Rules\IsValidPassword;

use App\Models\PurchaseDetails;

class DashboardController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }


    public function index()
    {   //dd($this->user->id);
        //dd(Auth::guard('admin')->user()->getAllPermissions()->toArray());
        //Role::create(['name' => 'writer1']);
        //Permission::create(['name' => 'warranties.view']);
        //dd(Auth::guard('admin')->user()->can('warranties.view'));
        if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }

        $total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        $total_roles = count(Role::select('id')->get());
        $accepted = PurchaseDetails::where('status', '=', 'in progress')->count();
        $review = PurchaseDetails::where('status', '=', 'review')->count();
        $approved = PurchaseDetails::where('status', '=', 'approved')->count();
        
        $purchase_all = PurchaseDetails::count();
        $product_year7 = PurchaseDetails::where('product_year', '=', '7')->count();
        $product_year12 = PurchaseDetails::where('product_year', '=', '12')->count();
        
        if($product_year7 > 0) {
            $product_year7P = $product_year7/$purchase_all * 100;
            $product_year7P = round($product_year7P);
            
        }else {
            $product_year7P = 0;
        }
        
        if($product_year12 > 0){
            $product_year12P = $product_year12/$purchase_all * 100;
            $product_year12P = round($product_year12P);
        }else {
            $product_year12P = 0;
        }
        
        $transaction_history = PurchaseDetails::take(5)->get();
        
        
        
        return view('admin.pages.dashboard.index', compact('accepted','review','approved','purchase_all','product_year7','product_year12','product_year7P','product_year12P','total_admins', 'total_roles', 'total_permissions', 'transaction_history'));
    }
    
    public function manager()
    {       
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        $total_roles = count(Role::select('id')->get());
        $accepted = PurchaseDetails::where('status', '=', 'in progress')->count();
        $review = PurchaseDetails::where('status', '=', 'review')->count();
        $approved = PurchaseDetails::where('status', '=', 'approved')->count();
        
        $purchase_all = PurchaseDetails::count();
        $product_year7 = PurchaseDetails::where('product_year', '=', '7')->count();
        $product_year12 = PurchaseDetails::where('product_year', '=', '12')->count();
        
        if($product_year7 > 0) {
            $product_year7P = $product_year7/$purchase_all * 100;
            $product_year7P = round($product_year7P);
            
        }else {
            $product_year7P = 0;
        }
        
        if($product_year12 > 0){
            $product_year12P = $product_year12/$purchase_all * 100;
            $product_year12P = round($product_year12P);
        }else {
            $product_year12P = 0;
        }
        
        $transaction_history = PurchaseDetails::take(5)->get();
        
        
        
        return view('admin.pages.dashboard.index', compact('accepted','review','approved','purchase_all','product_year7','product_year12','product_year7P','product_year12P','total_admins', 'total_roles', 'total_permissions', 'transaction_history'));
    }
    
    public function sale()
    {       
        /*if (is_null($this->user) || !$this->user->can('dashboard.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view dashboard !');
        }*/

        $total_roles = count(Role::select('id')->get());
        $total_admins = count(Admin::select('id')->get());
        $total_permissions = count(Permission::select('id')->get());
        $total_roles = count(Role::select('id')->get());
        $accepted = PurchaseDetails::where('status', '=', 'in progress')->count();
        $review = PurchaseDetails::where('status', '=', 'review')->count();
        $approved = PurchaseDetails::where('status', '=', 'approved')->count();
        
        $purchase_all = PurchaseDetails::count();
        $product_year7 = PurchaseDetails::where('product_year', '=', '7')->count();
        $product_year12 = PurchaseDetails::where('product_year', '=', '12')->count();
        
        if($product_year7 > 0) {
            $product_year7P = $product_year7/$purchase_all * 100;
            $product_year7P = round($product_year7P);
            
        }else {
            $product_year7P = 0;
        }
        
        if($product_year12 > 0){
            $product_year12P = $product_year12/$purchase_all * 100;
            $product_year12P = round($product_year12P);
        }else {
            $product_year12P = 0;
        }
        
        $transaction_history = PurchaseDetails::take(5)->get();
        
        
        
        return view('admin.pages.dashboard.index', compact('accepted','review','approved','purchase_all','product_year7','product_year12','product_year7P','product_year12P','total_admins', 'total_roles', 'total_permissions', 'transaction_history'));
    }
    
    public function changePassword(){
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        return view('admin.pages.dashboard.changePassword');
    }
    public function changPasswordStore(Request $request)
    {
        
        //dd(auth('admin')->user());
        $request->validate([
            'current_password' => ['required', new MatchOldPasswordAdmin],
            //'new_password' => ['required'],
            'new_password' => ['required', 'string', new isValidPassword()],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        Admin::find(auth('admin')->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        return redirect()->route('admin.dashboard')->with('success','Password successfully changed');
        //return redirect()->route('dashboard');
    }
}
