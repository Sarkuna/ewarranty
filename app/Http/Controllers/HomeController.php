<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;
use App\Rules\MatchOldPassword;
use App\Rules\IsValidPassword;

use App\Models\Uploads;
use App\User;
use App\Models\Shops;
use App\Models\PurchaseDetails;
use App\Models\PurchaseDetailReviews;
use Auth;
use App\Models\States;
use App\Models\Cities;
use App\Models\Postcodes;

class HomeController extends Controller
{
    private $photos_path;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->photos_path = public_path('/upload/');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "All Email Template"]];
        $warranties = PurchaseDetails::where('user_id',Auth::user()->id)->latest()->paginate(5);
        $states = States::select('id','state')->get();
        return view('home', ['breadcrumbs' => $breadcrumbs], compact('warranties','states'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        //return view('home');
    }
    
    public function termsConditions(){
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        return view('termsConditions');
    }
    
    public function home2()
    {
        $msg = ['success' => 'Thank you your form was successfully submitted'];
        //dd($msg);
        return redirect()->route('user.home')->with($msg);
       // return view('home2');
    }
    
   
    public function store(Request $req)
    {
        //dd($req->all());die;
        $agree = 0;
        $req->validate([
            'name' => 'required|max:200',
            'email' => 'required|email|max:255|unique:users',
            'mobile' => 'required|max:15|unique:users',            
            'shop_state' => 'required',
            'shop_name' => 'required',
            'date_of_purchase' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'terms' => 'required'            
        ]);
        
        if($req->has('terms')){
            $agree = 1;
        }
        

        DB::beginTransaction();
        try {// all good
            $user = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'mobile' => $req->mobile,
                'password' => Hash::make('Password'),
                'status' => 'active',
            ]);
            $shop = Shops::create([
                'company_name' => $req->shop_name,
                'state' => $req->shop_state,
                'status' => 'inactive',
            ]);
            
            
            $purchase = PurchaseDetails::create([
                'user_id' => $user->id,
                'customer_name' => $req->name,
                'customer_mobile' => $req->mobile,
                'shop_id' => $shop->id,
                'ref' => $req->ref,
                'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
                'address1' => $req->address1,
                'address2' => $req->address2,
                'city' => $req->city,
                'post_code' => $req->postcode,
                'state' => $req->state,
                'visitor' => $req->ip(),
                //'agree' => $req->terms('agree') ? true : false,
                'agree' => $agree,
            ]);
            
            if($user && $shop && $purchase) {
                DB::commit();
                $msg = 'Client Successfully added';
                echo $msg;die;
            }else {                
                DB::rollback();                
                $msg = 'Please try again!!';
                echo $msg;die;
            }
            die;
            return redirect()->back()->with('success', $msg);
            
        } catch (\Exception $e) {// something went wrong
            dd($e);
            DB::rollback();
            return redirect()->back()->with('success', 'Please try again!!');
        }
    }

    public function uploadImages(Request $request) {
        //dd($request->all());        
        $preview = $config = $errors = [];
        $_ref = $request->_ref;
        $_type = $request->_type;
        //dd($request->_type);
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        }           

        if ($request->hasFile('uploadFile') || $request->hasFile('uploadFile706') || $request->hasFile('uploadFile707') || $request->hasFile('uploadFile708')) {

            $allowedfileExtension = ['jpg', 'png', 'gif'];
            if(!empty($request->file('uploadFile'))) {
                $files = $request->file('uploadFile');
            }else if(!empty($request->file('uploadFile706'))) {
                $files = $request->file('uploadFile706');
            }else if(!empty($request->file('uploadFile707'))) {
                $files = $request->file('uploadFile707');
            }else if(!empty($request->file('uploadFile708'))) {
                $files = $request->file('uploadFile708');
            }else {
                $files = '';
            }
            
            if(!empty($files)) {
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $fileSize = $file->getSize();
                    $extension = $file->getClientOriginalExtension();
                    
                    $baseName = $filename . '.' . $extension;
                    $realFileName   = md5($baseName.time()) . '.' . $extension;
                    
                    $path = $file->storeAs($destination_path,$realFileName);                    
                    $newFileUrl = asset('/storage/purchase_detail/'.$_ref.'/'.$realFileName);
                    
                    if($path) {
                        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $baseName,'real_filename' => $realFileName,'type' => $_type]);
                        $lastId = $uploads->id;
                        $preview[] = $newFileUrl;
                        //array_push($preview, $newFileUrl);
                        $config[] = [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ];
                        
                        /*array_push($config, [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ]);*/
                    }else {
                        $errors[] = $fileName;
                    }
                }
                
                //$totalimage =  Uploads::where(['ref' => $_ref])->count();
                $totalimage =  Uploads::where('ref', '=', $request->_ref)->get()->count();
                
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                if (!empty($errors)) {
                    $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
                    $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
                }
                return $out;
            }else {
                return [];
            }
        }else {
            return [];
        }
    }
    
    public function edit(Request $request)
    {
        $id = $request->key;
        $model = Uploads::find($id);
        if($model!==NULL){
            $filename  = 'public/purchase_detail/'.$model->ref.'/'.$model->real_filename;
            $filename  = storage_path('app/public/purchase_detail/'.$model->ref.'/'.$model->real_filename);
            if(File::exists($filename)) {
                File::delete($filename);
            }
            if($model->delete()){
                $out = ['minFileCount' => 1,'maxFileCount' => 1];
                return $out;
                //echo json_encode(['success'=>true]);
            }else{
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
    
    public function show($id)
    {
        $warranty = PurchaseDetails::where('user_id',Auth::user()->id)->find($id);
        return view('warranty_show',compact('warranty'));
    }
    
    public function changePassword(){
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        return view('changePassword');
    }
    public function changPasswordStore(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            //'new_password' => ['required'],
            'new_password' => ['required', 'string', new isValidPassword()],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        return redirect()->route('user.home')->with('success','Password successfully changed');
        //return redirect()->route('dashboard');
    }
    
    public function get_by_states(Request $request)
    {
        if (!$request->state_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $cities = Cities::orderby("city_name","asc")->where('state_id', $request->state_id)->get();
            foreach ($cities as $city) {
                $html .= '<option data-id="'.$city->id.'" value="'.$city->city_name.'">'.$city->city_name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
    
    public function get_by_cities(Request $request)
    {
        if (!$request->city_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $postcodes = Postcodes::orderby("postcode","asc")->where('city_id', $request->city_id)->get();
            foreach ($postcodes as $pcode) {
                $html .= '<option value="'.$pcode->postcode.'">'.$pcode->postcode.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
}
