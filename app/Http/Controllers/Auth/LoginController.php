<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\User;
use Auth;
use Hash;
use Helper;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/';
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $guard = 'user';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    protected function guard()
    {
        return Auth::guard('web');
    }
    
    public function index()
    {
        
        //return view('home');
      return view('auth.login'); 
    }
    // Login
    public function showLoginForm(){
        return view('auth.login');
    }
    
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|max:150',
            'password' => 'required',
        ], [
            'email.required' => 'Name is required',
            //'name.min' => 'Name must be at least 2 characters.',
            'email.max' => 'Email should not be greater than 150 characters.',
        ]);
	$credentials = $request->only('email', 'password');
        
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            
            Auth::user()->date_last_login = Carbon::now()->format('Y-m-d H:i:s');
            Auth::user()->save();
            //\AdminLogActivity::addToLog('Successully Logged in!');*/
            //dd(Auth::user());
            //session()->flash('success', 'Successfully Logged in !');
            return redirect()->route('user.home');
        }
        // error
        session()->flash('error', 'Invalid email or password');
        return back();
        //return Redirect::to('/#login') ->with('error', 'Invalid email and password');
        
       
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
        //return $this->loggedOut($request) ?: redirect('/');
    }
}
