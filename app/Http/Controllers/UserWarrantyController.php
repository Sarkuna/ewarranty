<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use File;
use DB;

use App\Models\Uploads;
use App\User;
use App\Models\Shops;
use App\Models\PurchaseDetails;
use App\Models\PurchaseDetailReviews;
use Auth;
use PDF;
use Helper;

class UserWarrantyController extends Controller
{
    private $photos_path;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->photos_path = public_path('/upload/');
        //$this->middleware('guest')->except('logout');
        $this->middleware('auth');
    }
    
    public function index()
    { 
       return view('new_submission');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        
        //dd($req->all());die;
        $msg = array();
        $agree = 0;
        $req->validate([
            'name' => 'required|max:200',
            'mobile' => 'required|max:15', 
            'shop_state' => 'required',
            'shop_name' => 'required',
            'invoice_num' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'terms' => 'required'            
        ]);
        
        if($req->has('terms')){
            $agree = 1;
        }

        DB::beginTransaction();
        try {// all good
            $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();
            $manage_id = null; $sale_id = null; $emailbcc = array();
            if($shops){
                $shopid = $shops->id;
                if(!empty($shops->assign_manager->manager_id)) {
                    $manage_id = $shops->assign_manager->manager_id;
                    if(!empty($shops->assign_manager->manager->email)){
                        $emailbcc[] = $shops->assign_manager->manager->email;
                    }
                }
                
                if(!empty($shops->assign_sale->id)) {
                    $sale_id = $shops->assign_sale->sales_id;
                    if(!empty($shops->assign_sale->sale->email)){
                        $emailbcc[] = $shops->assign_sale->sale->email;
                    }
                }
            }else{
                $shop = Shops::create([
                    'company_name' => $req->shop_name,
                    'state' => $req->shop_state,
                    'status' => 'new',
                ]);
                $shopid = $shop->id;
            }
            
            if(!empty($req->surface_opt)){
                $surface_opt = implode(', ', $req->surface_opt);
            }else {
                $surface_opt = null;
            }
            
            
            $purchase = PurchaseDetails::create([
                'user_id' => Auth::user()->id,
                'customer_name' => $req->name,
                'customer_mobile' => $req->mobile,
                'shop_id' => $shopid,
                'ref' => $req->ref,
                'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
                'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
                'address1' => $req->address1,
                'city' => $req->city,
                'post_code' => $req->postcode,
                'state' => $req->state,
                'visitor' => $req->ip(),
                //'agree' => $req->terms('agree') ? true : false,
                'agree' => $agree,
                'invoice_num' => $req->invoice_num,
                'surface' => implode(', ', $req->surface),
                'surface_opt' => $surface_opt,
                'topcoat' => implode(', ', $req->topcoat),
                'a_name' => $req->a_name,
                'a_address' => $req->a_address,
                'a_state' => $req->a_state,
                'a_city' => $req->a_city,
                'a_postcode' => $req->a_postcode,
                'sales_id' => null,
                'manager_id' => null,
            ]);
            
            if($purchase) {                
                DB::commit();
                //$msg = 'Client Successfully added';
                $msg = ['success' => 'Thank you. Your form was successfully submitted'];
                $data = ['name' => Auth::user()->name,'email' => Auth::user()->email];    
                $subject = '';
                $emailbcc[] = env('APP_RS_SUPPORT');
                $return = Helper::sendEmailAdmin2(Auth::user()->email, $subject, 'M50T7B', $data,$emailbcc);
                //Auth::login($user);
                //echo $msg;die;
            }else {                
                DB::rollback();                
                //$msg = 'Please try again!!';
                $msg = ['error' => 'Please try again!!'];
            }
            return redirect()->route('user.home')->with($msg);
            //return redirect()->back()->with($msg);
            
        } catch (\Exception $e) {// something went wrong
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error', 'Oops! Please try again!!');
        }
    }

    public function uploadImages(Request $request) {
        //dd($request->all());        
        $preview = $config = $errors = [];
        $_ref = $request->_ref;
        $_type = $request->_type;
        //dd($request->_type);
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        }           

        if ($request->hasFile('uploadFile') || $request->hasFile('uploadFile706') || $request->hasFile('uploadFile707') || $request->hasFile('uploadFile708')) {

            $allowedfileExtension = ['jpg', 'png', 'gif'];
            if(!empty($request->file('uploadFile'))) {
                $files = $request->file('uploadFile');
            }else if(!empty($request->file('uploadFile706'))) {
                $files = $request->file('uploadFile706');
            }else if(!empty($request->file('uploadFile707'))) {
                $files = $request->file('uploadFile707');
            }else if(!empty($request->file('uploadFile708'))) {
                $files = $request->file('uploadFile708');
            }else {
                $files = '';
            }
            
            if(!empty($files)) {
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $fileSize = $file->getSize();
                    $extension = $file->getClientOriginalExtension();
                    
                    $baseName = $filename;
                    $realFileName   = md5($baseName.time()) . '.' . $extension;
                    
                    $path = $file->storeAs($destination_path,$realFileName);                    
                    $newFileUrl = asset('/storage/purchase_detail/'.$_ref.'/'.$realFileName);
                    
                    if($path) {
                        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $baseName,'real_filename' => $realFileName,'type' => $_type]);
                        $lastId = $uploads->id;
                        $preview[] = $newFileUrl;
                        //array_push($preview, $newFileUrl);
                        $config[] = [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ];
                        
                        /*array_push($config, [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ]);*/
                    }else {
                        $errors[] = $fileName;
                    }
                }
                
                //$totalimage =  Uploads::where(['ref' => $_ref])->count();
                $totalimage =  Uploads::where('ref', '=', $request->_ref)->get()->count();
                
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                if (!empty($errors)) {
                    $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
                    $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
                }
                return $out;
            }else {
                return [];
            }
        }else {
            return [];
        }
    }
    
    public function reuploadImages(Request $request) {
        //dd($request->all());        
        $preview = $config = $errors = [];
        
        $_key = $request->_key;
        $_id = $request->_id;        
        $_type = $request->_type;
        
        $model = Uploads::find($_id);
        $_ref = $model->ref;
       
        $destination_path = 'public/purchase_detail/' . $model->ref;
        /*if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        }*/           

        if ($request->hasFile('uploadFile')) {
            
            if ($model !== NULL) {
                $filename = 'public/purchase_detail/' . $model->ref . '/' . $model->real_filename;
                $filename = storage_path('app/public/purchase_detail/' . $model->ref . '/' . $model->real_filename);
                if (File::exists($filename)) {
                    File::delete($filename);
                }
                //$model->delete();
            }

            $allowedfileExtension = ['jpg', 'png', 'gif'];
            if(!empty($request->file('uploadFile'))) {
                $file = $request->file('uploadFile');
            }else {
                $file = '';
            }
            
            if(!empty($file)) {
                    $filename = $file->getClientOriginalName();
                    $fileSize = $file->getSize();
                    $extension = $file->getClientOriginalExtension();
                    
                    $baseName = $filename;
                    $realFileName   = md5($baseName.time()) . '.' . $extension;
                    
                    $path = $file->storeAs($destination_path,$realFileName);                    
                    $newFileUrl = asset('/storage/purchase_detail/'.$_ref.'/'.$realFileName);
                    
                    if($path) {                        
                        $uploads = Uploads::where('id', $_id)
                        ->update([
                            'file_name' => $baseName, 'real_filename' => $realFileName, 'status' => 'pending', 'summary' => ''
                         ]);
                        $lastId = $_id;
                        $preview[] = $newFileUrl;
                        //array_push($preview, $newFileUrl);
                        $config[] = [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/reedit', // server api to delete the file based on key
                        ];
                    }else {
                        $errors[] = $fileName;
                    }
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                if (!empty($errors)) {
                    $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
                    $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
                }
                return $out;
            }else {
                return [];
            }
        }else {
            return [];
        }
    }
    public function edit(Request $request)
    {
        $id = $request->key;
        $model = Uploads::find($id);
        if($model!==NULL){
            $filename  = 'public/purchase_detail/'.$model->ref.'/'.$model->real_filename;
            $filename  = storage_path('app/public/purchase_detail/'.$model->ref.'/'.$model->real_filename);
            if(File::exists($filename)) {
                File::delete($filename);
            }
            if($model->delete()){
                $out = ['minFileCount' => 1,'maxFileCount' => 1];
                return $out;
                //echo json_encode(['success'=>true]);
            }else{
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
    
    public function updateResubmit(Request $req)
    {
        $itemTypes = $req->image_id;
        $status = Uploads::whereIn('id', $itemTypes)
        ->update([
            'summary' => '',
            'status' => 'pending',
        ]);        
        
        if($status){
            \App\Models\EmailQueue::where([['purchase_id',$req->id]])->delete();
            $msg = ['success' => 'Successfully re-submitted'];
        }
        else{
            $msg = ['error' => 'Please try again!!'];
        }
        
        return redirect()->route('user.home')->with($msg);
    }
    
    public function autocomplete(Request $request)
    {
        $query = $request->get('query','');
        $state = $request->get('state','');
        $datas = Shops::select("company_name")
                ->where('company_name','LIKE','%'.$query.'%')
                ->where('state','=',$state)
                ->where('status','=','active')
                ->get();
        
        $dataModified = array();
        foreach ($datas as $data)
        {
          $dataModified[] = $data->company_name;
        }

        return response()->json($dataModified);
    }
    
    public function update($id)
    {
        $warranty = PurchaseDetails::where('user_id',Auth::user()->id)->where('status','review')->find($id);
        if (empty($warranty)) {
            return redirect()->route('user.home')->with('error', 'Page not Found');
        }
        return view('re_submission',compact('warranty'));
    }
    
    public function downloadPDF($id)
      {
        //$devloper = TopDevloperInfo::find($id);
        $model = PurchaseDetails::where('user_id',Auth::user()->id)->find($id);
        
        if($model->status == "approved")
        {
            $product_name = 'Dulux Weathershield';

            if(!empty($model->shop_info->address1)) {
                $address = $model->shop_info->address1.$model->shop_info->address2;
            }else {
                $address = 'N/A';
            }
            
            if(!empty($model->apply_date)) {
                $apply_date = $model->apply_date->format('d-m-Y');
            }else {
                $apply_date = 'N/A';
            }
            
            if (!empty($model->a_name)) {
                $a_name = $model->a_name;
            } else {
                $a_name = 'N/A';
            }
            
            if (!empty($model->a_address)) {
                $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
            } else {
                $a_address = 'N/A';
            }
            $model = [
                'warranty_code' => $model->warranty_code,
                'product_name' => $product_name,
                'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
                'apply_date' => $apply_date,
                'store_name' => $model->shop_info->company_name,
                'address' => $address,
                'post_code' => $model->shop_info->post_code,
                'city' => $model->shop_info->city,
                'state' => $model->shop_info->state,
                'name' => $model->customer_info->name,
                'email' => $model->customer_info->email,
                'mobile' => $model->customer_info->mobile,
                'expiry_date' => $model->expiry_date->format('d-m-Y'),
                'customer_address' => $model->address1.' '.$model->address2,
                'customer_city' => $model->city,
                'customer_post_code' => $model->post_code,
                'customer_state' => $model->state,
                'painter_name' => $a_name,
                'painter_address' => $a_address,
                
            ];
            $pdf = PDF::loadView('admin.warranties.pdf_view', $model);
            //$pdf = PDF::loadView('pdf', compact('show'));
            return $pdf->stream(date('d-m-Y').".pdf");
        }
        return response(view('errors.404'), 404);
        
        $pdf = PDF::loadView('pdf', compact('warranty'));
        return $pdf->stream('topdevloperinfo.pdf');
      }
      
      public function downloadPDFPowerflexx($id)
      {
        //$devloper = TopDevloperInfo::find($id);
        $model = PurchaseDetails::where('user_id',Auth::user()->id)->find($id);
        
        if($model->status == "approved")
        {
            $product_name = 'Dulux Weathershield Powerflexx';

            
            if(!empty($model->shop_info->address1)) {
                $address = $model->shop_info->address1.$model->shop_info->address2;
            }else {
                $address = 'N/A';
            }
            
            if(!empty($model->apply_date)) {
                $apply_date = $model->apply_date->format('d-m-Y');
            }else {
                $apply_date = 'N/A';
            }
            
            if (!empty($model->a_name)) {
                $a_name = $model->a_name;
            } else {
                $a_name = 'N/A';
            }
            
            if (!empty($model->a_address)) {
                $a_address = $model->a_address.', '.$model->a_city.', '.$model->a_postcode.', '.$model->a_state;
            } else {
                $a_address = 'N/A';
            }
            
            $model = [
                'warranty_code' => $model->warranty_code,
                'product_name' => $product_name,
                'date_of_purchase' => $model->date_of_purchase->format('d-m-Y'),
                'apply_date' => $apply_date,
                'store_name' => $model->shop_info->company_name,
                'address' => $address,
                'post_code' => $model->shop_info->post_code,
                'city' => $model->shop_info->city,
                'state' => $model->shop_info->state,
                'name' => $model->customer_info->name,
                'email' => $model->customer_info->email,
                'mobile' => $model->customer_info->mobile,
                'expiry_date' => $model->expiry_date_power->format('d-m-Y'),
                'customer_address' => $model->address1.' '.$model->address2,
                'customer_city' => $model->city,
                'customer_post_code' => $model->post_code,
                'customer_state' => $model->state,
                'painter_name' => $a_name,
                'painter_address' => $a_address,
                
            ];
            $pdf = PDF::loadView('admin.warranties.pdf_view', $model);
            //$pdf = PDF::loadView('pdf', compact('show'));
            return $pdf->stream(date('d-m-Y').".pdf");
        }
        return response(view('errors.404'), 404);
        
        $pdf = PDF::loadView('pdf', compact('warranty'));
        return $pdf->stream('topdevloperinfo.pdf');
      }
      
      
    public function storeMedia(Request $request) {
        $_ref = $request->ref;
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        } 
        //$allowedfileExtension = ['jpg', 'png', 'gif', 'pdf'];
        
        $image = $request->file('file');
        $img_type = $request->img_type;
        $imageName = $image->getClientOriginalName();
        $extension = $image->getClientOriginalExtension();
        $rename = str_replace(' ','_',$imageName);
        
        $realFileName = md5(time()).$rename;
        
        $path = $image->storeAs($destination_path, $realFileName);
        $newFileUrl = asset('/storage/purchase_detail/' . $_ref . '/' . $realFileName);
        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $imageName, 'real_filename' => $realFileName, 'type' => $img_type]);
        echo $realFileName;
    }
    
    public function deleteMedia(Request $request) {
        $_ref = $request->ref;
        $image = $request->url;
        $img_type = $request->img_type;
        
        $filename  = storage_path('app/public/purchase_detail/'.$_ref.'/'.$image);
        if(File::exists($filename)) {
            $uploaded_image = Uploads::where('ref', '=', $_ref)->where('real_filename', '=', $image)->where('type', '=', $img_type)->first();
            $id = $uploaded_image->id;
            $upload = Uploads::find($id);
            if (!is_null($upload)) {
                $upload->delete();
            }
            File::delete($filename);
        }
        
        return $filename; 
    }
}
