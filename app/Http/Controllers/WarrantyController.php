<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use File;
use DB;

use App\Models\Uploads;
use App\User;
use App\Models\Shops;
use App\Models\PurchaseDetails;
use App\Models\PurchaseDetailReviews;
use App\Models\States;
use App\Models\Cities;
use App\Models\Postcodes;


use Auth;
use Mail;
use Helper;

class WarrantyController extends Controller
{
    private $photos_path;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->photos_path = public_path('/upload/');
        $this->middleware('guest')->except('logout');
        //$this->middleware('auth');
    }
    
    public function index()
    {
        $states = States::select('id','state')->get();
        return view('index', compact('states'));
    }
    
    public function indexTest()
    {
        return view('index_test');
    }
    
    
    public function termsConditions(){
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        return view('termsConditions');
    }
    
    public function usageGuide(){
        
        return redirect('/')->with('error','Oops! Please try again!!'); 
        //return redirect('/#guide&success');
        //$breadcrumbs = [['link' => "/", 'name' => "Home"], ['name' => "Change Password"]];
        //return view('productUsageGuide');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $msg = array();
        $agree = 0;
        $req->validate([
            'name' => 'required|max:200',
            'email' => 'required|email|max:255|unique:users',
            'mobile' => 'required|max:15',            
            'shop_state' => 'required',
            'shop_name' => 'required',
            'invoice_num' => 'required',
            'date_of_purchase' => 'required',
            'apply_date' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'terms' => 'required'            
        ]);
        
        if($req->has('terms')){
            $agree = 1;
        }
        

        DB::beginTransaction();
        try {// all good
            
            $password = Str::random(8);
            
            $user = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'mobile' => $req->mobile,
                'password' => Hash::make($password),
                'status' => 'active',
            ]);
            
            
            /*$shop = Shops::create([
                'company_name' => $req->shop_name,
                'state' => $req->shop_state,
                'status' => 'inactive',
            ]);*/
            
            $shops = Shops::where([['company_name',$req->shop_name],['state',$req->shop_state]])->first();
            $manage_id = null; $sale_id = null; $emailbcc = array();
            
            if($shops) {
                $shopid = $shops->id;
                if(!empty($shops->assign_manager->manager_id)) {
                    $manage_id = $shops->assign_manager->manager_id;
                    /*if(!empty($shops->assign_manager->manager->email)){
                        $emailbcc[] = $shops->assign_manager->manager->email;
                    }*/
                }
                
                if(!empty($shops->assign_sale->id)) {
                    $sale_id = $shops->assign_sale->sales_id;
                    /*if(!empty($shops->assign_sale->sale->email)){
                        $emailbcc[] = $shops->assign_sale->sale->email;
                    }*/
                }
                
            } else {
                $shop = Shops::create([
                    'company_name' => $req->shop_name,
                    'state' => $req->shop_state,
                    'status' => 'new',
                ]);
                $shopid = $shop->id;
            }
            
            if(!empty($req->surface_opt)){
                $surface_opt = implode(', ', $req->surface_opt);
            }else {
                $surface_opt = null;
            }
            
            
            $purchase = PurchaseDetails::create([
                'user_id' => $user->id,
                'customer_name' => $req->name,
                'customer_mobile' => $req->mobile,
                'shop_id' => $shopid,
                'ref' => $req->ref,
                'date_of_purchase' => date('Y-m-d', strtotime($req->date_of_purchase)),
                'apply_date' => date('Y-m-d', strtotime($req->apply_date)),
                'address1' => $req->address1,
                'city' => $req->city,
                'post_code' => $req->postcode,
                'state' => $req->state,
                'visitor' => $req->ip(),
                //'agree' => $req->terms('agree') ? true : false,
                'agree' => $agree,
                'invoice_num' => $req->invoice_num,
                'surface' => implode(', ', $req->surface),
                'surface_opt' => $surface_opt,
                'topcoat' => implode(', ', $req->topcoat),
                'a_name' => $req->a_name,
                'a_address' => $req->a_address,
                'a_state' => $req->a_state,
                'a_city' => $req->a_city,
                'a_postcode' => $req->a_postcode,
                'sales_id' => $sale_id,
                'manager_id' => $manage_id,
            ]);
            
            if($user && $purchase) {
                DB::commit();
                //$msg = 'Client Successfully added';
                //$msg = ['success' => 'Thank you your form was successfully submitted '];
                $data = ['name' => $user->name,'email' => $user->email, 'password' => $password];    
                $subject = '';
                $emailbcc[] = env('APP_RS_SUPPORT');
                $return = Helper::sendEmailAdmin2($user->email, $subject, 'M50T7A', $data,$emailbcc);
                //Auth::login($user);
                ///#guide
                //return redirect()->route('user.home')->with($msg);
                //return redirect('/#guide');
                return redirect('/#guide&success');
                //echo $msg;die;
            }else {                
                DB::rollback();                
                //$msg = 'Please try again!!';
                $msg = ['error' => 'Please try again!!'];
            }
            return redirect()->back()->with($msg);
            
        } catch (\Exception $e) {// something went wrong
            //dd($e);
            DB::rollback();
            return redirect()->back()->with('error','Oops! Please try again!!');
        }
    }
    
    public function storeMedia(Request $request) {
        $_ref = $request->ref;
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        } 
        //$allowedfileExtension = ['jpg', 'png', 'gif', 'pdf'];
        
        $image = $request->file('file');
        $img_type = $request->img_type;
        $imageName = $image->getClientOriginalName();
        $extension = $image->getClientOriginalExtension();
        $rename = str_replace(' ','_',$imageName);
        
        $realFileName = md5(time()).$rename;
        
        $path = $image->storeAs($destination_path, $realFileName);
        $newFileUrl = asset('/storage/purchase_detail/' . $_ref . '/' . $realFileName);
        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $imageName, 'real_filename' => $realFileName, 'type' => $img_type]);
        echo $realFileName;
    }
    
    public function deleteMedia(Request $request) {
        $_ref = $request->ref;
        $image = $request->url;
        $img_type = $request->img_type;
        
        $filename  = storage_path('app/public/purchase_detail/'.$_ref.'/'.$image);
        if(File::exists($filename)) {
            $uploaded_image = Uploads::where('ref', '=', $_ref)->where('real_filename', '=', $image)->where('type', '=', $img_type)->first();
            $id = $uploaded_image->id;
            $upload = Uploads::find($id);
            if (!is_null($upload)) {
                $upload->delete();
            }
            File::delete($filename);
        }
        
        return $filename; 
    }

    public function uploadImages(Request $request) {
        //dd($request->all());        
        $preview = $config = $errors = [];
        $_ref = $request->_ref;
        $_type = $request->_type;
        //dd($request->_type);
        $destination_path = 'public/purchase_detail/' . $_ref;
        if (! File::exists($destination_path)) {
            File::makeDirectory($destination_path, 0775, true, true);
        }           

        if ($request->hasFile('uploadFile') || $request->hasFile('uploadFile706') || $request->hasFile('uploadFile707') || $request->hasFile('uploadFile708')) {

            $allowedfileExtension = ['jpg', 'png', 'gif'];
            if(!empty($request->file('uploadFile'))) {
                $files = $request->file('uploadFile');
            }else if(!empty($request->file('uploadFile706'))) {
                $files = $request->file('uploadFile706');
            }else if(!empty($request->file('uploadFile707'))) {
                $files = $request->file('uploadFile707');
            }else if(!empty($request->file('uploadFile708'))) {
                $files = $request->file('uploadFile708');
            }else {
                $files = '';
            }
            
            if(!empty($files)) {
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $fileSize = $file->getSize();
                    $extension = $file->getClientOriginalExtension();
                    
                    $baseName = $filename;
                    $realFileName   = md5($baseName.time()) . '.' . $extension;
                    
                    $path = $file->storeAs($destination_path,$realFileName);                    
                    $newFileUrl = asset('/storage/purchase_detail/'.$_ref.'/'.$realFileName);
                    
                    if($path) {
                        $uploads = Uploads::create(['ref' => $_ref, 'file_name' => $baseName,'real_filename' => $realFileName,'type' => $_type]);
                        $lastId = $uploads->id;
                        $preview[] = $newFileUrl;
                        //array_push($preview, $newFileUrl);
                        $config[] = [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ];
                        
                        /*array_push($config, [
                            'key' => $lastId,
                            'caption' => $filename,
                            'size' => $fileSize,
                            //'downloadUrl' => $newFileUrl, // the url to download the file
                            'url' => 'image/'.$lastId.'/edit', // server api to delete the file based on key
                        ]);*/
                    }else {
                        $errors[] = $fileName;
                    }
                }
                
                //$totalimage =  Uploads::where(['ref' => $_ref])->count();
                $totalimage =  Uploads::where('ref', '=', $request->_ref)->get()->count();
                
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                if (!empty($errors)) {
                    $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
                    $out['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
                }
                return $out;
            }else {
                return [];
            }
        }else {
            return [];
        }
    }
    
    public function edit(Request $request)
    {
        $id = $request->key;
        $model = Uploads::find($id);
        if($model!==NULL){
            $filename  = 'public/purchase_detail/'.$model->ref.'/'.$model->real_filename;
            $filename  = storage_path('app/public/purchase_detail/'.$model->ref.'/'.$model->real_filename);
            if(File::exists($filename)) {
                File::delete($filename);
            }
            if($model->delete()){
                $out = ['minFileCount' => 1,'maxFileCount' => 1];
                return $out;
                //echo json_encode(['success'=>true]);
            }else{
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
    
    public function autocomplete(Request $request)
    {
        //dd($request->all());
        $query = $request->get('query','');
        $state = $request->get('state','');
        $datas = Shops::select("company_name")
                ->where('company_name','LIKE','%'.$query.'%')
                ->where('state','=',$state)
                ->where('status','=','active')
                ->get();
        
        $dataModified = array();
        foreach ($datas as $data)
        {
          $dataModified[] = $data->company_name;
        }

        return response()->json($dataModified);
    }
    
    public function get_by_states(Request $request)
    {
        if (!$request->state_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $cities = Cities::orderby("city_name","asc")->where('state_id', $request->state_id)->get();
            foreach ($cities as $city) {
                $html .= '<option data-id="'.$city->id.'" value="'.$city->city_name.'">'.$city->city_name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
    
    public function get_by_cities(Request $request)
    {
        if (!$request->city_id) {
            $html = '<option value="">Select</option>';
        } else {
            $html = '<option value="">Select</option>';
            $postcodes = Postcodes::orderby("postcode","asc")->where('city_id', $request->city_id)->get();
            foreach ($postcodes as $pcode) {
                $html .= '<option value="'.$pcode->postcode.'">'.$pcode->postcode.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }
    
    
    public function checkemail(Request $request)
    {
        
        $email = $request->get('email','');
        
        $useremail = User::all()->where('email', $email)->first();
        if ($useremail) {
            return response()->json('yes');
        } else {
            return response()->json('no');
        }

        //return response()->json($dataModified);
    }
    
    public function checkinvoice(Request $request)
    {
        //dd($request->all());
        $invoice_num = $request->get('invoice_num','');
        $shopname = $request->get('shopname','');
        
        $shop = Shops::where('company_name','=',$shopname)->first();
        
        $invoicenum = PurchaseDetails::all()->where('shop_id', $shop->id)->where('invoice_num', $invoice_num)->first();
        if ($invoicenum) {
            return response()->json('yes');
        } else {
            return response()->json('no');
        }

        //return response()->json($dataModified);
    }
}
